function lsbm_shutdown()

    root = getenv('LSBM_ROOT');
    if dk.fs.isdir(root)
        setenv( 'LSBM_ROOT', '' );
        dk.print( '[LSBM] Shutting down from folder "%s".', root );
    else
        warning( 'LSBM does not appear to have started.' );
    end

end
