
## Stimuli

All functions expect a 1x3 vector `[tstart,tend,dt]` as first input.
Set `dt=0` if the step is not fixed.

# Pre/post-processing

Example pre-processing: inter-hemispheric scaling, normalisation
Example post-processing: delay rounding (can be done on edges)

## Design

Keep it simple: main object is `lsbm.Config`, the parameterisation are just structures, and can use some of the helper functions in `lsbm.build.*`.

## To-do

 - Model objects are just a front to gather system parameters
 - Units are almost bare Models, but Networks define parameters+methods for coupling/delay, and have a list of units
 - Models can build() a Simulation, given problem params
 - Simulations can run(), given stepping/control params, and return a Result
 - Result is an adapter on the Simulation, which allows easy access to model logic
 - There are functions to build Network objects (uniform, variable, etc) from Parcellation + Unit config
