function p = stim_bump(tf,loc,width,amp)
    assert( tf(1)<=loc && loc<=tf(2), 'Location is outside time-frame.' );
    p.name = 'impulse';
    p.loc = loc;
    p.amp = amp;
    p.width = width;
end