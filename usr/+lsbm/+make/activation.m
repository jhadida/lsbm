function p = activation(name,thresh,width,gain,varargin)
%
% p = lsbm.resp.activation(name,thresh,width,gain,varargin)
%
% Available activations are:
%   step, rectifier, linear, softplus, smoothstep, rational, mixed
%
% Additional parameters:
%   + Rational: alpha(1.0)
%
% JH

    p = dk.c2s(varargin);
    p.name = name;
    p.thresh = thresh;
    p.width = width;
    p.gain = gain;
    
end