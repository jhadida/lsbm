function p = stim_wave(tf,freq,amp,phi,base)
    if nargin < 5, base=0; end
    if nargin < 4, phi=0; end
    
    p.name = 'harmonic';
    p.freq = freq;
    p.amp = amp;
    p.phi = phi;
    p.base = base;
end