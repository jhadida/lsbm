function p = inducer(name,wait,length)
%
% p = lsbm.resp.inducer(name,wait,length)
%
% Available inducers are:
%   step, linear, smoothstep
%
% JH

    p.name = name;
    p.wait = wait;
    p.length = length;
    
end