function p = stim_stairs(tf,steps,init)
    if nargin < 3, init=0; end
    assert( ismatrix(steps) && size(steps,2)==2 && all(diff(steps(:,1)) > 0), 'Bad steps.' );
    
    p.name = 'piecewise';
    p.stepmat = steps;
    p.init = init;
end