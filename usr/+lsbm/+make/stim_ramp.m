function p = stim_ramp(tf,slope,init)
    if nargin < 3, init=0; end
    p.name = 'timecourse';
    p.time = tf(1:2);
    p.vals = init + slope*[ 0, tf(2)-tf(1) ];
end