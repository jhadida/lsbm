function p = sigmoid(name,mu,sigma,varargin)
%
% p = lsbm.resp.sigmoid(name,mu,sigma,varargin)
%
% Available sigmoids are:
%   logistic, gaussian, hyperbolic, gumbel, weibull
%
% Additional parameters:
%   + Weibull: shape(1.0)
%
% JH

    p = dk.c2s(varargin);
    p.name = name;
    p.mu = mu;
    p.sigma = sigma;
    
end