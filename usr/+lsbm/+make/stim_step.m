function p = stim_step(tf,onset,value)
    p.name = 'heaviside';
    p.onset = onset;
    p.value = value;
end