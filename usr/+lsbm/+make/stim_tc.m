function p = stim_tc(tf,time,vals)
    assert( isvector(time) && all(diff(time) > 0), 'Bad timepoints.' );
    assert( isvector(vals) && numel(vals)==numel(time), 'Time/value mismatch.' );
    assert( time(1)<=tf(1) && time(end)>=tf(2), 'Bounds mismatch.' );

    p.name = 'timecourse';
    p.time = time;
    p.vals = vals;
end