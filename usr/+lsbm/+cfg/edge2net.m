function [coupling,delay] = edge2net( edge, n )
%
% [coupling,delay] = lsbm.cfg.edge2net( edge, n )
%
% Rebuild netmats from edges:
%   - edge is a struct-array with fields: src, dst, delay, coupling;
%   - n is the size of the output matrices (should be >= largest index).
%
% See also: lsbm.cfg.net2edge
%
% JH

    % allocate netmats
    delay    = zeros(n);
    coupling = zeros(n);
    
    % iterate on edges
    nedges = numel(edge);
    
    for i = 1:nedges
        r = edge(i).src;
        c = edge(i).dst;
        
        delay(r,c)    = edge(i).delay;
        coupling(r,c) = edge(i).coupling;
    end

end
