function edges = delround( edges, thresh )
%
% edges = lsbm.cfg.delround( edges, thresh )
%
% Round delays in struct-array of edges.
%
% See also: lsbm.cfg.network, lsbm.cfg.net2edge
%
% JH

    assert( thresh > eps, 'Rounding threshold should be positive.' );
    [ edges.delay ] = thresh * round( [ edges.delay ] / thresh );
    
end
