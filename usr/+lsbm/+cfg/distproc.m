function dmat = distproc( dmat, proc, val, rnd )
%
% dmat = lsbm.cfg.distproc( dmat, proc, value, round=0 )
%
% Generate delay matrix from distance matrix using specified method.
% 
% INPUTS
%
%   proc    One of:
%               velocity    divided by value (scalar or matrix)
%               average     divide by nz average, and scale by value 
%               median      divide by nz median, and scale by value
%
%   value   Scalar or matrix to be used for processing.
%   round   If positive, round delays to be multiple of specified value.
%
% JH

    if nargin < 4, rnd = 0; end
    
    % apply desired processing
    switch lower(proc)
        case {'v','vel','velocity'}
            dmat = dmat ./ val;
        case {'a','avg','average'}
            dmat = val * dmat / mean(nonzeros(dmat(:)));
        case {'m','med','median'}
            dmat = val * dmat / median(nonzeros(dmat(:)));
        otherwise
            error( 'Unknown processing: %s', proc );
    end
    
    % round output if required
    if rnd > eps
        dmat = rnd * round( delay / rnd );
    end

end