function [C,D] = network( Nunit, Nnode, Nstate, local, nonloc )
%
% [C,D] = lsbm.cfg.network( Nunit, Nnode, Nstate, local, nonloc )
%
% Compute network matrices with coupling and delay.
%
% Nunit     number of regions
% Nnode     number of populations per region
% Nstate    number of equations per population
%
%   (define M=Nnode*Nstate, N=M*Nunit)
%
%
% Local coupling (struct):
%
%   local.con     M-by-M              (to be replicated for all units)
%              or M-by-M-by-Nunit     (one slice per unit)
%
%   local.del     idem
%
%
% Network coupling (struct):
%
%   nonloc.con    N-by-N
%              or struct-array with fields
%                   .mat    Nunit-by-Nunit
%                   .mask   M-by-M Kronecker mask
%
%   nonloc.del    N-by-N
%              or Nunit-by-Nunit (replicated for all nodes)
%              or struct-array with fields
%                   .mat    Nunit-by-Nunit
%                   .mask   M-by-M Kronecker mask
%
%
% See also: lsbm.cfg.net2edge
%
% JH

    M = Nnode*Nstate;
    N = M*Nunit;
    
    assert( dk.is.struct(local,{'con','del'}) && dk.is.struct(nonloc,{'con','del'}), ...
        'Inputs should be structs with fields: .con, .del' );
    
    
    % check local coupling
    if numel(local.con) == M*M
        local.con = repmat(local.con,1,1,Nunit); 
    else
        local.con = reshape(local.con,[M,M,Nunit]);
    end
    if isempty(local.del)
        local.del = zeros(M,M,Nunit);
    elseif numel(local.del) == M*M
        local.del = repmat(local.del,1,1,Nunit); 
    else
        local.del = reshape(local.del,[M,M,Nunit]);
    end
    
    assert( all(size(local.con) == [M,M,Nunit]), 'Bad local weights.' );
    assert( all(size(local.del) == [M,M,Nunit]), 'Bad local delays.' );
    
    % weigths
    if isnumeric(nonloc.con)
        assert( dk.is.matrix(nonloc.con,[N,N]), 'Bad nonlocal weights shape.' );
        C = nonloc.con;
    else
        assert( dk.is.struct(nonloc.con,{'mat','mask'},0), 'Bad nonlocal weights struct.' );
        C = zeros(N);
        n = numel(nonloc.con);
        
        for i = 1:n
            C = C + kron( nonloc.con(i).mat, nonloc.con(i).mask );
        end
    end
    
    % delays
    if isnumeric(nonloc.del)
        
        if dk.is.matrix(nonloc.del,[Nunit,Nunit])
            D = kron( nonloc.del, ones(M) );
        else
            assert( dk.is.matrix(nonloc.del,[N,N]), 'Bad nonlocal weights shape.' );
            D = nonloc.del;
        end
        
    else
        assert( dk.is.struct(nonloc.del,{'mat','mask'},0), 'Bad nonlocal weights struct.' );
        D = zeros(N);
        n = numel(nonloc.del);
        
        for i = 1:n
            D = D + kron( nonloc.del(i).mat, nonloc.del(i).mask );
        end
    end
    
    % apply local coupling
    for i = 1:Nunit
        k = (1:M) + (i-1)*M;
        C(k,k) = C(k,k) + local.con(:,:,i);
        D(k,k) = D(k,k) + local.del(:,:,i);
    end
    
end
