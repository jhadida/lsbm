function edge = net2edge( coupling, delay, dround, cthresh )
%
% edge = lsbm.cfg.net2edge( coupling, delay, dround=0, cthresh=eps )
%
% Convert network matrices to struct-array of edges for simulation:
%   - coupling and delay should be square matrices of size Nnode;
%   - dround is a scalar used to force commensurate delays, i.e.
%     if positive all delay values are rounded to multiples of dround;
%   - cthresh is a scalar used to threshold absolute coupling values, i.e.
%     if positive all edges with absolute coupling <= cthresh are ignored.
%
% See also: lsbm.cfg.network, lsbm.Config
%
% JH

    if nargin < 3, dround = 0; end
    if nargin < 4, cthresh = eps; end

    % extract non-negligible values
    mask     = abs(coupling) > cthresh;
    coupling = coupling(mask);
    delay    = delay(mask);
    nedges   = sum(mask(:));
    
    % round delays
    if dround > eps
        delay = dround * round( delay / dround );
    end
    
    % allocate output
    edge = struct( 'src', 0, 'dst', 0, 'delay', 0, 'coupling', 0 );
    edge = repmat( edge, [1,nedges] );
    
    [row,col] = find(mask);
    for i = 1:nedges
        edge(i).src       = row(i);
        edge(i).dst       = col(i);
        edge(i).delay     = delay(i);
        edge(i).coupling  = coupling(i);
    end
    
end
