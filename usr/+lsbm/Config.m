classdef Config < handle
%
% Configuration wrapper for LSBM_Helper (C++ simulation library).
% This should allow all possible simulations with the framework.
%
% JH

    properties
        default_block_size = 1000;
    end
    
    properties (SetAccess = private)
        problem;
        control;
        stepping;
        verbose;
        system;
        other;
    end
    
    methods
        
        function self = Config(in)
            self.clear();
            switch nargin
                case 0
                    self.set_verbose('default');
                case 1
                    if ischar(in)
                        self.set_verbose(in);
                    elseif isa(in,'lsbm.Config')
                        self.copy(in);
                    elseif isstruct(in)
                        self.parse(in);
                    else
                        error('Bad input.');
                    end
                otherwise
                    error('Unexpected number of inputs.');
            end
        end
        
        function [p,n] = get_props(self)
            p = { 'problem', 'control', 'stepping', 'system', 'other', 'verbose' };
            n = numel(p);
        end
        
        function self = clear(self)
            [p,n] = self.get_props();
            for i = 1:n
                self.(p{i}) = struct();
            end
            self.verbose = 'default';
        end
        
        function self = copy(self,other)
            [p,n] = self.get_props();
            for i = 1:n
                self.(p{i}) = other.(p{i});
            end
        end
        
        function other = clone(self)
            other = lsbm.Simulation();
            other.copy(self);
        end
        
        function cfg = build(self)
        %
        % Return configuration structure which can be passed to LSBM_Helper
        %
            cfg.problem    = self.problem; 
            cfg.properties = struct( 'step', self.stepping, 'error', self.control );
            cfg.system     = self.system;
            cfg.other      = self.other;
            cfg.version    = '0.1';
            cfg.verbose    = self.verbose;
            
            % now deal with specific issues
            cfg = deal_with_issues(cfg);
            
        end
        
        function self = parse(self,cfg,ver)
            
            if nargin < 3, ver = dk.struct.get(cfg,'version','latest'); end
            
            switch ver
                case {'0.1','latest'}
                    self.problem  = cfg.problem;
                    self.control  = cfg.properties.error;
                    self.stepping = cfg.properties.step;
                    self.system   = cfg.system;
                    self.other    = cfg.other;
                    self.verbose  = cfg.verbose;
            end
        end
        
        function info(self)
            
            to_prc = @(x) floor(100*x);
            
            % network or unit simulation?
            is_net = all(isfield( self.system, {'nodes','edges'} ));
            if is_net
                nnodes = numel(self.system.nodes);
                nedges = numel(self.system.edges);
                dk.print('[lsbm.Simulation] Network simulation with %d nodes and %d edges:',nnodes,nedges);
                
                if isfield(self.system.edges,'coupling')
                    dk.print('\t- Average coupling: %g', mean([self.system.edges.coupling]) );
                end
                if isfield(self.system.edges,'delay')
                    d = [self.system.edges.delay];
                    dk.print('\t- Average delay: %g', mean(nonzeros(d)) );
                    dk.print('\t- Delayed ratio: %d %%', to_prc(nnz(d)/numel(d)) );
                end
            else
                dk.print('[lsbm.Simulation] Single unit simulation:');
            end
            
            % problem type
            is_ivp = isfield( self.problem, 'xstart' );
            tspan  = self.problem.tend - self.problem.tstart;
            if is_ivp
                dk.print('\t- Initial value problem (tspan: %.3f sec)',tspan);
            else
                nhist = numel(self.problem.history.time);
                dk.print('\t- History problem (%d past timepoints, tspan: %.3f sec)',nhist,tspan);
            end
            
            % stepping info
            if isfield( self.stepping, 'dt' )
                dt = self.stepping.dt;
                dk.print('\t- Fixed time-step: %g sec', dt);
                
            elseif isfield( self.stepping, 'nsteps' )
                ns = self.stepping.nsteps;
                dk.print('\t- Fixed number of steps: %d steps', ns);
                
            else
                tmin = self.stepping.min;
                tmax = self.stepping.max;
                tini = self.stepping.init;
                dk.print('\t- Adaptive time-step between %g and %g sec (init: %g sec)',tmin,tmax,tini);
            end
            
            % tolerance info
            tol = self.control;
            nctrl = dk.struct.get( tol, 'norm_control', false );
            dk.print('\t- Control parameters: abs=%g, rel=%g, norm_control=%s',tol.abs,tol.rel,dk.util.bool2tf(nctrl));
            
        end
        
    end
    
    
    %-----------------------------
    % PROBLEM TO SOLVE
    %-----------------------------
    methods
        
        function self = set_ivp(self,tstart,tend,xstart,bsize)
            
            if nargin < 5, bsize = self.default_block_size; end
            assert( tend > tstart, 'tend should be larger than tstart.' );
            assert( bsize > 1, 'Block-size should be greater than 1.' );
            
            self.problem = struct( 'tstart', tstart, 'tend', tend, 'xstart', xstart, 'bsize', bsize );
            
        end
        
        function self = set_hp(self,tstart,tend,history,bsize)
            
            if nargin < 5, bsize = self.default_block_size; end
            assert( tend > tstart, 'tend should be larger than tstart.' );
            assert( bsize > 1, 'Block-size should be greater than 1.' );
            assert( all(isfield(history,{'time','vals'})), 'History should be a structure with fields time/vals.' );
            assert( numel(history.time) >= 2, 'There should be at least 2 timepoints in history.' );
            assert( history.time(end) == tstart, 'Last history point and tstart should match.' );
            
            self.problem = struct( 'tstart', tstart, 'tend', tend, 'history', history, 'bsize', bsize );
            
        end
        
        function self = set_timeframe(self,tstart,tend)
            
            if nargin == 2 && numel(tstart) == 2
                tend   = tstart(2);
                tstart = tstart(1);
            end
            assert( tend > tstart, 'tend should be larger than tstart.' );
            self.problem.tstart = tstart;
            self.problem.tend   = tend;
            
        end
        
        function set_verbose(self,val)
        %
        % Levels are: quiet, info, debug
        %
            assert( ismember(val,{'quiet','info','debug','default'}), 'Bad verbose.' );
            self.verbose = val;
        end
        
    end
    
    
    %-----------------------------
    % SYSTEM SPECIFICATION
    %-----------------------------
    methods
        
        function self = set_network(self,nodes,edges)
            
            assert( isstruct(nodes) && isstruct(edges), 'Invalid inputs.' );
            
            self.system.nodes = nodes;
            self.system.edges = edges;
        end
        
        function [C,D] = get_netmats(self)
            [C,D] = lsbm.cfg.edge2net( self.system.edges, numel(self.system.nodes) );
        end
        function D = get_delays(self)
            try
                D = [self.system.edges.delay];
            catch
                D = [];
            end
        end
        
        function self = set_sysopt(self,varargin)
        %
        % set_sysopt( <struct> )
        % set_sysopt( Name, Value ... )
        %
            
            if nargin == 2 && isstruct(varargin{1})
                self.system.options = varargin{1};
            else
                self.system.options = struct(varargin{:});
            end
            
        end
        
        function self = set_system(self,varargin)
        %
        % set_system( <struct> )
        % set_system( Name, Value ... )
        %
            
            if nargin == 2 && isstruct(varargin{1})
                self.system = varargin{1};
            else
                self.system = struct(varargin{:});
            end
            
        end
        
    end
    
    
    %-----------------------------
    % SAMPLING PROPERTIES
    %-----------------------------
    methods
        
        function a = is_fixed_step(self)
            a = any(isfield( self.stepping, {'dt','nsteps'} ));
        end
        
        function self = set_step(self,varargin)
        %
        % set_step( dt )
        % set_step( min_dt, max_dt, init_dt = (min_dt+max_dt)/2 )
        % set_step( struct('min','max','init') )
        % set_step( [min_dt, max_dt] )
        % set_step( [min_dt, max_dt, init_dt] )
        %
            
            % one input
            if nargin == 2
                
                dt = varargin{1};
                
                if isstruct(dt) % structure input
                    
                    mindt = dt.min;
                    maxdt = dt.max;
                    inidt = dk.struct.get( dt, 'init', (mindt+maxdt)/2 );
                    self.set_step( mindt, maxdt, inidt );
                    
                elseif ~isscalar(dt) % array input
                    
                    mindt = dt(1);
                    maxdt = dt(2);
                    if numel(dt) > 2
                        inidt = dt(3);
                    else
                        inidt = (mindt+maxdt)/2;
                    end
                    self.set_step( mindt, maxdt, inidt );
                    
                else % scalar input
                    
                    assert( dt > eps, 'dt should be positive.' );
                    self.stepping = struct( 'dt', dt );
                    
                end
                
            end
            
            % two or three inputs
            if nargin >= 3
                
                mindt = varargin{1};
                maxdt = varargin{2};
                
                if nargin == 4
                    initdt = varargin{3};
                else
                    initdt = (mindt+maxdt)/2;
                end
                
                assert( mindt > eps, 'Min dt should be positive.' );
                assert( (initdt >= mindt) && (maxdt >= initdt), 'Bad ranking of min/max/init dt.' );
                
                self.stepping = struct( 'min', mindt, 'max', maxdt, 'init', initdt );
                
            end
            
        end
        
        function self = set_numsteps(self,nsteps)
            
            nsteps = ceil(nsteps);
            assert( nsteps > 0, 'Number of steps should be positive.' );
            
            self.stepping = struct( 'nsteps', nsteps );
            
        end
        
        function self = set_tolerance(self,absolute,relative)
        %
        % set_tolerance( abs, rel )
        % set_tolerance( [abs,rel] ) 
        % set_tolerance( struct('abs','rel') )
        %
            
            % accept different inputs
            if nargin == 2
                if isstruct(absolute)
                    relative = absolute.rel;
                    absolute = absolute.abs;
                elseif numel(absolute)==2
                    relative = absolute(2);
                    absolute = absolute(1);
                end
            end
            
            number = @(x) isnumeric(x) && isscalar(x);
            assert( number(absolute) && number(relative), 'Two scalar inputs requried.' );
            assert( all([absolute,relative] > eps), 'Inputs should be positive.' );
            self.control.abs = absolute;
            self.control.rel = relative;
            
        end
        
        function self = set_norm_control(self)
            self.control.norm_control = true;
        end
        function self = unset_norm_control(self)
            self.control = dk.struct.rem( self.control, 'norm_control' );
        end
        
        function self = unset_control(self)
            self.control = struct();
        end
        
    end
    
    
    %-----------------------------
    % OTHER OPTIONS
    %-----------------------------
    methods
        
        function self = set_other(self,other)
            
            % obsolete
            if isfield(other,'downsample')
                warning( 'Option "downsample" is now obsolete, use "resample" instead.' );
            end
            if isfield(other,'tolerance')
                warning( 'Option "tolerance" is now obsolete.' );
            end
            
            
            % random seed
            if isfield(other,'random_seed')
                self.set_random_seed( other.random_seed );
            else
                self.unset_random_seed();
            end
            
            % induction
            if isfield(other,'induction')
                self.set_induction( other.induction );
            else
                self.unset_induction();
            end
            
            % resampling 
            if isfield(other,'resample')
                self.set_resampling( other.resample );
            else
                self.unset_resampling();
            end
            
        end
        
        % Random-seed
        function self = set_random_seed(self,r)
            self.other.random_seed = r;
        end
        function self = unset_random_seed(self)
            self.other = dk.struct.rem( self.other, 'random_seed' );
        end
        
        % Induction
        function self = set_induction(self,varargin)
            self.other.induction = dk.c2s(varargin);
        end
        function self = unset_induction(self)
            self.other = dk.struct.rem( self.other, 'induction' );
        end
        
        % Resampling (in Hz)
        function self = set_resampling(self,fs)
            assert( fs > 0, 'Sampling rate should be positive.' );
            self.other.resample = fs;
        end
        function self = unset_resampling(self)
            self.other = dk.struct.rem( self.other, 'resample' );
        end
        
    end
    
    methods (Hidden)
        
        % unused?
        function self = make_fast(self,fs)
            
            assert( isfield(self.stepping,'dt'), 'Fast mode only works with fixed time-step integration.' );
            assert( isfield(self.system,'edges'), 'System is not set or is not a network.' );
            assert( isfield(self.system.edges,'delay'), 'Fast mode only works with delay systems.' );
            
            % delays must be multiples of the time-step
            dt = self.stepping.dt;
            delays = [self.system.edges.delay];
            check = delays/dt;
            check = abs(check - round(check));
            assert( all(check < eps), 'Fast mode only works when delays are multiples of the time-step.' );
            
            % transform delays to number of time-steps
            bsize = self.problem.bsize;
            ne = numel(self.system.edges);
            
            for i = 1:ne
                nsteps = round(delays(i)/dt);
                self.system.edges(i).delay = nsteps;
                bsize = max( bsize, nsteps+1 );
            end
            
            % set block-size such that all delayed-terms are contained within one block
            self.problem.bsize = bsize;
            
            % enable resampling
            if nargin > 1, self.set_resampling(fs); end
            
        end
        
    end
    
end

%-----------------------------
% DEALING WITH ISSUES
%-----------------------------
function cfg = deal_with_issues(cfg)
    cfg = issue_history_sampling(cfg);
end

% history problem and fixed_step option interact with each other
function cfg = issue_history_sampling(cfg)

    if isfield( cfg.system, 'options' ) ...
    && isfield( cfg.problem, 'history' ) ...
    && any(isfield( cfg.properties.step, {'dt','nsteps'} ))

        H  = cfg.problem.history;
        dt = cfg.properties.step.dt;

        if ~all( abs(diff(H.time) - dt) < eps )

            % Going forwards from the first timepoint in history with fixed time-steps could miss the initial
            % time-point, which is the last time-point in history. We can't correct for the last step only,
            % because that would make time-steps irregular. So we go backwards from the initial time-point
            % instead, and if needed go one time-point past the first history point. If that happens, then
            % the value at that point needs to be extrapolated. 
            k = ceil( (H.time(end)-H.time(1)) / dt );
            t = fliplr( H.time(end) - (0:k)*dt )'; 
            v = interp1( H.time, H.vals, t, 'pchip' );

            dk.warn([ ...
                'Configuration with fixed-step option requires the history to be sampled at the same time-step as that specified for integration.\n' ...
                'The history values have been resampled to this effect, and the first (earliest) history value might have been extrapolated.'
            ]);

            cfg.problem.history = struct( 'time', t, 'vals', v );

        end

    end

end
