function [s,sinv] = sigm2handle(in,norm)
%
% [s,sinv] = lsbm.util.sigm2handle( in, norm=false )
%
% Convert input sigmoid structure into function handle.
% Optionally normalise to ensure values between 0 and 1 for positive inputs.
%
% See also: lsbm.make.sigmoid
%
% JH

    if nargin < 2, norm=false; end
    
    switch lower(in.name)
        
        case 'logistic'
            
            mu   = in.mu;
            sig  = in.sigma;
            val  = @(x) 1./(1+exp( -(x-mu)/sig ));
            ival = @(y) mu + sig*log( y./(1-y) );
            
        case {'gaussian','normal','normcdf'}
            
            mu   = in.mu;
            sig  = in.sigma;
            val  = @(x) normcdf(x,mu,sig);
            ival = @(y) norminv(x,mu,sig);
            
        otherwise
            error('Not implemented: %s',in.name);
            
    end
    
    if norm
        v0   = val(0);
        s    = @(x) max( 0, (val(x)-v0)/(1-v0) );
        sinv = @(y) ival( v0 + y*(1-v0) );
    else
        s    = val;
        sinv = ival;
    end
    
end