function [best,step] = dichotomy( gt, range, ndigits, maxshift )
%
% best = lsbm.util.dichotomy( is_greater_than, range, ndigits=3, maxshift=[0,2] )
%
% Dichotomic search with at least ndigits significant digits compared to range-width.
% 
%    
% See also: ant.math.dichotomy
%
% JH

    if nargin < 4, maxshift=[0,2]; end
    if nargin < 3, ndigits=3; end
    
    if isscalar(range), range = [0,range]; end
    [~,maxerr]  = dk.num.magnitude( abs(range(2)-range(1)), ndigits );
    [best,step] = ant.math.dichotomy( gt, range, maxerr, maxshift );
    
end