function out = run( cfg, simfun, rescls, samp, init, dtbounds )
%
% out = lsbm.run( cfg, simfun, rescls, samp, init, dtbounds=[1e-9,1e-3] )
%
% Assign configuration, run simulation, and return result.
%
%
% INPUTS
% ------
%
%        cfg    Instance of lsbm.Config class
%     simfun    Handle to simulation function expecting a config struct built by lsbm.Config::build(),
%               and returning a single struct in output.
%     rescls    Result class used to wrap output results, deriving from lsbm.model.Result.
%       samp    Sampling config, struct with fields (in SECONDS):
%                   .span*  Time-span (scalar: length, 1x2 vector: start/end)
%                   .step*  Time-step (scalar: fixed, 1x3 vector: min/max/ini, string: auto)
%                   .tol    Tolerance (1x2 vector: abs,rel, default=[])
%                   .nc     Norm-control (logical, default=false)
%       init    Initialisation for the integration problem: 
%                       IVP: vector with initial state
%                   History: struct{time,vals}
%   dtbounds    Vector [dtmin,dtmax]
%   
%
% OUTPUT
% ------
%
% The struct returned by simfun.
% 
% JH

    if nargin < 6, dtbounds = [1e-9, 1e-3]; end
    if isstruct(cfg), cfg = lsbm.Config(cfg); end

    assert( isa(cfg,'lsbm.Config'), 'Bad input config.' );
    assert( dk.is.fhandle(simfun), 'simfun should be a function handle.' );
    assert( dk.is.struct(samp,{'span','step'}), 'Bad sampling params.' );
    
    
    % sampling properties
    tspan = samp.span;
    tstep = samp.step;
    
    if isscalar(tspan)
        tstart = 0;
        tend   = tspan;
    else
        tstart = tspan(1);
        tend   = tspan(2);
    end
    
    % recommend dt using positive delays
    delays = cfg.get_delays();
    if any(delays > 0)
        recom = recommend_dt( delays, dtbounds );
    else
        recom = [];
    end
    
    % set automatic timestep
    if strcmp(tstep,'auto')
        dk.reject( isempty(recom), 'Cannot recommend timestep without positive delays.' );
        tstep = recom;
        dk.info('Automatically set timestep to %g.',tstep);
    end
    
    % assign configuration
    if dk.is.struct( init, {'time','vals'} )
        cfg.set_hp( tstart, tend, init ); % history problem
    else
        cfg.set_ivp( tstart, tend, init ); % initial value problem
    end
    cfg.set_step(tstep);
    step = cfg.stepping; % parse and format
    
    % adjust timestep
    if isfield(step,'dt')
        step = process_scalar( step.dt, recom, dtbounds );
    else
        step = process_struct( step, recom, dtbounds(1) );
    end
    cfg.set_step(step); % final timestep
    
    % additional options
    tol = dk.struct.get( samp, 'tol', [] );
    if ~isempty(tol)
        cfg.set_tolerance( tol );
    end
    if dk.struct.get( samp, 'nc', false )
        cfg.set_norm_control();
    end
    
    % run simulation and collect outputs
    in = cfg.build();
    out = simfun(in);
    
    % wrap output
    out.solution = ant.TimeSeries(out.solution);
    out = rescls( in, out );

end

function dt = recommend_dt( delays, bounds )

    MFAC = 5;

    % check for small positive delays
    D_pos   = delays > 0;
    D_small = D_pos & (delays < bounds(1)*MFAC);
    n_small = nnz(D_small);
    
    dk.assert( any(D_pos), 'Cannot recommend timestep without positive delays.' );
    dk.reject( 'w', n_small > 0, [ ...
        'Found %d (/ %d) small delays; this might cause precision issues during integration.\n' ...
        'You should consider rounding the delays to a more coarse precision.' ...
    ], n_small, numel(delays) );
    
    % recommend a time-step using the smallest positive delay
    dt = min(delays(D_pos)) / MFAC;
    
    % warn if it falls outside the bounds
    dk.assert( 'w', dk.num.between(dt,bounds(1),bounds(2)), ...
        'Recommended timestep is outside allowed bounds.' );
    
    dt = dk.num.clamp(dt,bounds);
    
end

function dt = process_scalar(desired,recom,bounds)

    assert( dk.is.number(desired), 'Bad input type.' );
    
    mindt = bounds(1);
    maxdt = bounds(2);
    dt = desired;
    
    dk.assert( 'i', isempty(recom) || desired <= recom, ...
        'Desired timestep %g might be too large (recommended: %g).', desired, recom );
        
    if desired < mindt % warn and force the timestep to the minimum value
        dk.warn( 'Desired timestep %g is too small, forcing to %g instead.', desired, mindt );
        dt = mindt;
        
    elseif desired > maxdt % warn and force the timestep to the maximum value
        dk.warn( 'Desired timestep %g is too large, forcing to %g instead.', desired, maxdt );
        dt = maxdt;
        
    end

end

function dt = process_struct(desired,recom,mindt)

    dt = desired;
    if isempty(recom), return; end
    
    dk.reject( desired.min > recom, ...
        'Minimum desired timestep %g should not be larger than %g.', desired.min, recom );
    dk.reject( desired.max < mindt, ...
        'Maximum desired timestep %g should not be smaller than %g.', desired.max, mindt );
    
    dk.reject( 'i', desired.max > recom, ...
        'Maximum desired timestep %g is larger than recommended timestep %g.', desired.max, recom );
    dk.reject( 'i', desired.min < mindt, ...
        'Minimum desired timestep %g is smaller than recommended minimum %g.', desired.min, mindt );

end
