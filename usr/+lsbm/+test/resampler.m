function resampler()

    [t,x] = lsbm.test.data_meg();
    [~,nc] = size(x);
    
    %n = 20;
    %ts.time = t(1:n);
    %ts.vals = x(1:n,:);
    
    ts.time = t;
    ts.vals = zscore(x);
    
    % resample with LSBM
    dt = 1/100; % original is 250Hz
    
    opt.offset = 0;
    opt.consume = true;
    opt.lag = 0;
    opt.bsize = 100;
    
    ts_r = lsbm.mex.test_resampler(ts,dt,opt); 
    
    % resample with Matlab
    tq = colon( ts.time(1) + opt.offset, dt, ts.time(end) )';
    ts_m.vals = interp1( ts.time, ts.vals, tq, 'pchip' );
    ts_m.time = tq;
    
    % compare both interpolations
    max(abs( ts_r.vals(:) - ts_m.vals(:) ))
    
    % show results
    if nargout == 0
        dk.ui.image( {ts.time,1:nc,ts.vals}, 'subplot', {2,1,1}, ...
            'title', 'Reference', 'xlabel', "Time (sec)", 'ylabel', 'Channel' );
        
        dk.ui.image( {ts_r.time,1:nc,ts_r.vals}, 'subplot', {2,1,2}, ...
            'title', 'Resampled', 'xlabel', "Time (sec)", 'ylabel', 'Channel' );
    end

end