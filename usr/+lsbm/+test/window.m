function w = window( name, nw )

    if nargin < 2, nw=31; end
    w = lsbm.mex.test_window( name, nw );
    
    if nargout == 0
        plot(w,'k-','LineWidth',1);
        title(sprintf( 'Window: %s', name ));
    end

end