function out = sigmoid_all( varargin )
    
    % list of sigmoid functions
    name = { 'logistic', 'gaussian', 'hyperbolic', 'gumbel', 'weibull', 'smoothstep' };
    n = numel(name);
    
    % parameters
    p = dk.getopt( varargin, 'mu', 0.0, 'sigma', 1.0 );
    m = p.mu;
    s = p.sigma;
    w = 6;
    x = linspace( m-w*s, m+w*s, 500 );
    
    % compute sigmoid values/derivatives
    out = dk.struct.repeat( {'name','x','v','r','d'}, 1, n );
    for i = 1:n
        [v,d,r] = test_sigmoid( name{i}, p, x );
        out(i).name = name{i};
        out(i).x = x;
        out(i).v = v;
        out(i).r = r;
        out(i).d = d;
    end
    
    % draw results
    figure; hold on;
    for i = 1:n
        plot( x, out(i).v, 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Sigmoid (value)' );
    
    figure; hold on;
    for i = 1:n
        plot( out(i).v, out(i).r, 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Sigmoid (inverse)' );
    
    figure; hold on;
    for i = 1:n
        plot( out(i).v, log(eps+abs(out(i).r-x)), 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Sigmoid (log-error)' );
    
    figure; hold on;
    for i = 1:n
        plot( x, out(i).d, 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Sigmoid (derivative)' );

end

function [v,d,r] = test_sigmoid( name, p, x )
    p.name = name;
    
    p.type = 'val';
    v = lsbm.mex.test_sigmoid(p,x);
    
    p.type = 'inv'; 
    r = lsbm.mex.test_sigmoid(p,v);
	dk.print( '%s round-trip error: %g', name, max(abs(x-r)) );
    
    p.type = 'der';
    d = lsbm.mex.test_sigmoid(p,x);
end

function [v,d,r] = test_sigmoid_norm( name, p, x )
    p.name = name;
    
    p.type = 'normval';
    v = lsbm.mex.test_sigmoid(p,x);
    
    p.type = 'norminv'; 
    r = lsbm.mex.test_sigmoid(p,v);
    dk.print( '%s round-trip error: %g', name, max(abs(x-r)) );
    
    p.type = 'normder';
    d = lsbm.mex.test_sigmoid(p,x);
end
