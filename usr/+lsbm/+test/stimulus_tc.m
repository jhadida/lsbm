function [t,x] = stimulus_tc(name)
%
% [t,x] = lsbm.test.stimulus_example(name)
%
% Collection of interpolation problems:
%
%   sin         up-sample (3:1) sinusoid from irregular sample
%   sigmoid     up-sample (6:1) sigmoid from irregular sample
%   noise       up-sample (4:1) white noise from irregular sample
%   meg1        interpolate from tc with 25% of timepoints loss
%   meg2        randomly sample and compare with interp1
%
% JH

    switch name
        
        case 'const'
            r = [0,1];
            s = 2*[1,1];
            t = linspace(0,1,100);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
            
        case 'ramp'
            r = [0,1];
            s = [-1,1];
            t = linspace(0,1,100);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
        
        case 'sin'
            f = @(u) sin( 2*pi*13*u + pi/3 );
            r = [0;sort(5*rand(150,1));5];
            s = f(r);
            t = linspace(0,5,450);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
            
        case 'sigmoid'
            f = @(u) (1+tanh(u))/2;
            r = [-5;sort(-5 + 10*rand(50,1));5];
            s = f(r);
            t = linspace(-5,5,300);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
            
        case 'noise'
            s = randn(52,1);
            r = [0;sort(rand(50,1));1];
            t = linspace(0,1,200);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
            
        case 'meg1'
            [r,s] = get_meg_data(1);
            m = rand(size(r)) > 0.25; % remove 25%
            t = r;
            r = r(m);
            s = s(m);
            x = lsbm.test.stimulus( 'tc', t, 'time', r, 'vals', s );
            
        case 'meg2'
            [t,x] = get_meg_data(2);
            r = sort(t(1) + (t(end)-t(1))*rand(100,1));
            s = lsbm.test.stimulus( 'tc', r, 'time', t, 'vals', x );
            y = interp1( t, x, r, 'pchip' );
            dk.print( 'Comparison with interp1: %g', max(abs(s(:)-y(:))) );
            
        otherwise
            error( 'Unknown tc: %s', name );
    end
    
    if nargout == 0
        plot(r,s,'r--o','LineWidth',1); hold on;
        plot(t,x,'k-','LineWidth',1); hold off;
        title(sprintf( 'Timecourse: %s', name ));
        legend('Reference','Interpolated');
    end

end

function [t,x] = get_meg_data(chan)
    [t,x] = lsbm.test.data_meg(chan);
    t = t(1:501);
    x = x(1:501);
end
