function [x,y] = sigmoid( name, varargin )
%
% [x,y] = lsbm.test.sigmoid( name )
%   Test a sigmoid function by name.
%       logistic, gaussian, hyperbolic, gumbel, weibull
%
% [x,y] = lsbm.test.sigmoid( name, 'mu', 0.0, 'sigma', 1.0 );
%   Specify parameters.
%
% JH

    p = dk.getopt( varargin, 'mu', 0.0, 'sigma', 1.0 );
    p.name = name;
    
    m = p.mu;
    s = p.sigma;
    w = 6;
    x = linspace( m-w*s, m+w*s, 300 );
    y = lsbm.mex.test_sigmoid(p,x);
    
    if nargout == 0
        plot(x,y,'k-','LineWidth',1); hold on;
        plot([m,m],[0,1],'r--','LineWidth',2); hold off;
        title(sprintf( 'Sigmoid: %s', name ));
    end

end