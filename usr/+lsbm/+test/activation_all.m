function out = activation_all( varargin )
    
    % parameters
    p = dk.getopt( varargin, 'thresh', 0.0, 'width', 1.0, 'gain', 1.0 );
    m = p.thresh;
    s = p.width;
    x = linspace( m-2*s, m+5*s, 300 );
    
    % compute bounded and unbounded separately
    out.bounded = run_group( { 'step', 'linear', 'smoothstep', 'smoothstep4' }, p, x );
    out.unbounded = run_group( { 'rectifier', 'softplus', 'rational', 'mixed' }, p, x );

end

function out = run_group( name, p, x )

    n = numel(name);

    % compute activation values/derivatives
    out = dk.struct.repeat( {'name','x','v','d'}, 1, n );
    for i = 1:n
        p.name = name{i};
        v = lsbm.mex.test_activation(p,x);
        d = gradient(v,x(2)-x(1));
        out(i).name = name{i};
        out(i).x = x;
        out(i).v = v;
        out(i).d = d;
        
        assert( all(d >= 0), 'Activation function should be increasing.' );
    end
    
    % draw results
    figure; 
    
    subplot(2,1,1); hold on;
    for i = 1:n
        plot( x, out(i).v, 'LineWidth', 1 );
    end
    hold off; title( 'Activation (value)' );
    
    subplot(2,1,2); hold on;
    for i = 1:n
        plot( x, out(i).d, 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Activation (derivative)' );

end
