function out = inducer_all( varargin )

    name = { 'step', 'linear', 'smoothstep', 'smoothstep4' };
    n = numel(name);
    
    % parameters
    p = dk.getopt( varargin, 'onset', 0.0, 'length', 1.0 );
    m = p.onset;
    s = p.length;
    x = linspace( m-s, m+2*s, 300 );
    
    % compute inducer values
    out = dk.struct.repeat( {'name','x','v','d'}, 1, n );
    for i = 1:n
        p.name = name{i};
        v = lsbm.mex.test_inducer(p,x);
        out(i).name = name{i};
        out(i).x = x;
        out(i).v = v;
    end
    
    % draw results
    figure; hold on;
    for i = 1:n
        plot( x, out(i).v, 'LineWidth', 1 );
    end
    hold off; legend(name); title( 'Inducer' );

end
