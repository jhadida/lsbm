function [t,x] = data_meg(chan)
%
% Load and return surrogate MEG data.
%

    D = load(lsbm.path('+test/data-meg.mat'));
    t = D.time;
    x = D.vals;
    if nargin > 0
        x = x(:,chan);
    end

end