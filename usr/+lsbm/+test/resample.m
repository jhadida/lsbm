function [t,x] = resample()

    omega = 10;
    fs = 50*omega;
    
    % create data
    [t,x] = lsbm.test.data_wave( omega, fs, pi/3, 0.2 );
    
    % resample using LSBM
    ts = struct( 'time', t, 'vals', x );
    ts_r = lsbm.mex.test_resample( ts, 10/fs );
    ts_d = lsbm.mex.test_downsample( ts, 10/fs );
    
%     % resample using matlab
%     ts_m.time = ts_r.time;
%     ts_m.vals = interp1( t, x, ts_r.time, 'pchip' );
    
    if nargout==0
        plot(t,x,'k-','LineWidth',1); hold on;
        plot(ts_r.time,ts_r.vals,'or--','LineWidth',1); 
        plot(ts_d.time,ts_d.vals,'xb--','LineWidth',1); hold off;
        legend('Reference','Resampled','Downsampled');
        
%         figure;
%         plot(ts_m.time,ts_m.vals-ts_r.vals);
    end

end