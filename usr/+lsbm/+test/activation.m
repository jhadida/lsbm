function [x,y] = activation( name, varargin )
%
% [x,y] = lsbm.test.activation( name )
%   Test an activation function by name.
%       step, linear, smoothstep, smoothstep(2,3,4)
%       rectifier, softplus, rational, mixed
%
% [x,y] = lsbm.test.activation( name, 'thresh', 0.0, 'width', 1.0, 'gain', 1.0 );
%   Specify parameters.
%
% JH

    p = dk.getopt( varargin, 'thresh', 0.0, 'width', 1.0, 'gain', 1.0 );
    p.name = name;
    
    m = p.thresh;
    s = p.width;
    g = p.gain;
    x = linspace( m-2*s, m+5*s, 300 );
    y = lsbm.mex.test_activation(p,x);
    
    if nargout == 0
        plot(x,y,'k-','LineWidth',1); hold on;
        plot([m,m],[0,g],'r--','LineWidth',2); hold off;
        title(sprintf( 'Activation: %s', name ));
    end

end