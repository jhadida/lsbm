function window_all( nw )

    if nargin < 1, nw=31; end
    name = { 'blackman', 'flattop', 'gauss', 'hamming', 'hann', 'tukey' };
    n = numel(name);
    
    for i = 1:n
        plot( lsbm.test.window(name{i},nw), 'LineWidth', 1 ); hold on;
    end
    legend(name);

end