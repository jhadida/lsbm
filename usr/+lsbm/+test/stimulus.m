function x = stimulus( name, t, varargin )
%
% x = lsbm.test.stimulus( name, t )
%   Test an stimulus by name.
%       constant, heaviside, impulse, harmonic, piecewise, timecourse
%
% x = lsbm.test.stimulus( name, t, ... );
%   Specify parameters (stimulus-specific).
%         constant: value
%        heaviside: onset, value
%          impulse: loc, width, amp
%         harmonic: *freq, phi, base, amp
%        piecewise: *stepmat(Nx2), init
%       timecourse: *time, *vals
%
% JH

    % parse options
    switch name
        case {'c','constant'}
            name = 'constant';
            p = dk.getopt( varargin, 'value', 1.0 );
        case {'s','step','heaviside'}
            name = 'heaviside';
            p = dk.getopt( varargin, 'value', 1.0, 'onset', 0.0 );
        case {'b','bump','impulse'}
            name = 'impulse';
            p = dk.getopt( varargin, 'loc', 0.0, 'width', 1.0, 'amp', 1.0 );
        case {'h','harmonic'}
            name = 'harmonic';
            p = dk.getopt( varargin, 'phi', 0.0, 'base', 0.0, 'amp', 1.0 );
        case {'p','piecewise'}
            name = 'piecewise';
            p = dk.getopt( varargin, 'init', 0.0 );
        case {'tc','timecourse'}
            name = 'timecourse';
            p = dk.c2s( varargin );
        otherwise
            error( 'Unknown stimulus: %s', name );
    end
    p.name = name;
    
    % compute and show stimulus
    x = lsbm.mex.test_stimulus(p,t);
    if nargout == 0
        plot(t,x,'k-','LineWidth',1); 
        title(sprintf( 'Stimulus: %s', name ));
    end

end