function [t,x] = stimulus_example(name)
%
% [t,x] = lsbm.test.stimulus_example(name)
%
% Collection of example stimuli:
%
%   const, const0, const1
%   step, step-late, step-neg
%   bump, bump-late, bump-neg
%   sin, cos, sin+
%   stairs, stairs-long, stairs-alt
%
% JH

    N = 300;
    t = linspace(-1,5,N);

    switch name
        
        case 'const'
            x = lsbm.test.stimulus( 'c', t );
        case 'const0'
            x = lsbm.test.stimulus( 'c', t, 'value', 0 );
        case 'const1'
            x = lsbm.test.stimulus( 'c', t, 'value', 1 );
            
        
        case 'step'
            x = lsbm.test.stimulus( 's', t );
        case 'step-late'
            t = linspace(22,28,N);
            x = lsbm.test.stimulus( 's', t, 'onset', 23 );
        case 'step-neg'
            x = lsbm.test.stimulus( 's', t, 'value', -1 );
        
            
        case 'bump'
            x = lsbm.test.stimulus( 'b', t );
        case 'bump-late'
            t = linspace(20,26,N);
            x = lsbm.test.stimulus( 'b', t, 'loc', 23, 'width', pi );
        case 'bump-neg'
            x = lsbm.test.stimulus( 'b', t, 'amp', -2 );
            
            
        case 'sin'
            x = lsbm.test.stimulus( 'h', t, 'freq', 3 );
        case 'cos'
            x = lsbm.test.stimulus( 'h', t, 'freq', 3, 'phi', pi/2 );
        case 'sin+'
            x = lsbm.test.stimulus( 'h', t, 'freq', 5, 'amp', 2, 'base', 1 );
        
            
        case 'stairs'
            x = lsbm.test.stimulus( 'p', t, 'stepmat', [0,1; 1,2; 2,3; 3,4] );
        case 'stairs-long'
            t = linspace(-1,7,N);
            x = lsbm.test.stimulus( 'p', t, 'stepmat', [0,1; 2,2; 5,3] );
        case 'stairs-alt'
            x = lsbm.test.stimulus( 'p', t, 'stepmat', [0,1; 1,-2; 2,3; 3,-4; 4,0] );
        
            
        otherwise
            error( 'Unknown example: %s', name );
    end
    
    if nargout == 0
        plot(t,x,'k-','LineWidth',1); 
        title(sprintf( 'Stimulus: %s', name ));
    end

end