function [t,x] = data_wave(omega,fs,phi,noise)
%
% Generate simple sinusoid with centred gaussian noise.
%

    if nargin < 4, noise=0; end
    if nargin < 3, phi=0; end
    
    t = colon(0,1/fs,1)';
    x = sin(2*pi*omega*t+phi) + noise*randn(numel(t),1);

end