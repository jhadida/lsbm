function [x,y] = inducer( name, varargin )
%
% [x,y] = lsbm.test.inducer( name )
%   Test an inducer by name.
%       step, linear, smoothstep, smoothstep(2,3,4)
%
% [x,y] = lsbm.test.inducer( name, 'onset', 0.0, 'length', 1.0 );
%   Specify parameters.
%
% JH

    p = dk.getopt( varargin, 'onset', 0.0, 'length', 1.0 );
    p.name = name;
    
    m = p.onset;
    s = p.length;
    x = linspace( m-5*s, m+5*s, 300 );
    y = lsbm.mex.test_inducer(p,x);
    
    if nargout == 0
        plot(x,y,'k-','LineWidth',1); hold on;
        plot([m,m],[0,1],'r--','LineWidth',2); hold off;
        title(sprintf( 'Inducer: %s', name ));
    end

end