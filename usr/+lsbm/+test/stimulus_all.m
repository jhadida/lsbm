function stimulus_all( type )
%
% lsbm.test.stimulus_all( type )
%
% Call stimulus_example and stimulus_tc for a specified type of stimulus:
%   constant, heaviside, impulse, harmonic, piecewise, timecourse
%
% Opens a figure with multiple subplots (one for each example).
%
% JH

    % select group
    switch type
        case {'c','constant'}
            name = 'constant';
            fun = @lsbm.test.stimulus_example;
            lst = {'const','const0','const1'};
        case {'s','step','heaviside'}
            name = 'heaviside';
            fun = @lsbm.test.stimulus_example;
            lst = {'step','step-late','step-neg'};
        case {'b','bump','impulse'}
            name = 'impulse';
            fun = @lsbm.test.stimulus_example;
            lst = {'bump','bump-late','bump-neg'};
        case {'h','harmonic'}
            name = 'harmonic';
            fun = @lsbm.test.stimulus_example;
            lst = {'sin','cos','sin+'};
        case {'p','piecewise'}
            name = 'piecewise';
            fun = @lsbm.test.stimulus_example;
            lst = {'stairs','stairs-long','stairs-alt'};
        case {'tc','timecourse'}
            name = 'timecourse';
            fun = @lsbm.test.stimulus_tc;
            lst = {'sin','sigmoid','noise','meg1','meg2'};
        otherwise
            error( 'Unknown type: %s', type );
    end
    
    % open figure and draw examples
    n = numel(lst);
    [r,c] = dk.gridfit(n);
    dk.fig.new(sprintf('Stimuli group: %s',name),0.8);
    for i = 1:n
        subplot(r,c,i); fun(lst{i});
    end

end