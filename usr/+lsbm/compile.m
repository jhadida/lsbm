function compile( type, varargin )
%
% lsbm.compile( type, <filename(s)> )
%
% TYPES:
%
%   lib
%       Compiles the three core libraries (drayn,disol,lsbm). Does not expect any filename. 
%       Erases all existing compiled files (change in libraries implies changes in derivative work).
%
%   fun
%       If called without filename, compiles all .cpp files found in +mex/fun subfolder.
%       Otherwise compiles each specified function.
%
%   model
%       If called without filename, compiles all .cpp files found in +mex/model subfolder.
%       Otherwise compiles each specified model.
%
%   test
%       If called without filename, compiles all .cpp files found in +mex/test subfolder.
%       Otherwise compiles each specified test.
%
% Compile all Mex and C++ sources.
%
% JH

switch nargin

    case 0
        compile_libraries();
        compile_functions();
        compile_models();

    case 1

        switch lower(type)
            case {'libraries','libs','lib'}
                compile_libraries();

            case {'functions','function','func','fun'}
                compile_functions();

            case {'models','model','mod'}
                compile_models();

            case {'tests','test'}
                compile_tests();

            otherwise
                error('Unknown type "%s".',type);
        end

    otherwise

        nfiles = numel(varargin);
        for i = 1:nfiles

            file = varargin{i};
            switch lower(type)
                case {'functions','function','func','fun'}
                    compile_function(file);

                case {'models','model','mod'}
                    compile_model(file);

                case {'tests','test'}
                    compile_test(file);

                otherwise
                    error('Unknown type "%s".',type);
            end
        end

end
    
end

%----------------------------------------------------------------------
%----------------------------------------------------------------------

function p = get_paths()

    pmex = lsbm.path('+mex');
    
    p.mex = pmex;
    p.lib = fullfile( pmex, 'lib' );
    p.inc = fullfile( pmex, 'inc' );
    p.bin = fullfile( pmex, 'bin' );
    
    p.test = fullfile( pmex, 'test' );
    p.func = fullfile( pmex, 'func' );
    p.model = fullfile( pmex, 'model' );

end

function [opt,stg] = get_jmx_config()

    opt = struct();
    path = get_paths();
    
    libpath = @(x) fullfile(path.lib,x);
    
    % options
    opt.cpp11    = true;
    opt.jmx      = true;
    opt.arma     = true;
    opt.optimise = true;
    opt.verbose  = false;
    opt.debug    = false;
    
    % settings
    %
    %   Other possible flags:
    %       DRAYN_SHOW_DEBUG
    %       DRAYN_SHOW_INFO
    %       DRAYN_SAFE_INDEXING
    %
    %   Deterministic random seed requires manually seeding the random generator 
    %   (cf osd_helper), if you want runs to be reproducible:
    %       DRAYN_DETERMINISTIC_RANDOM_SEED
    %   
    stg.def = { 'DRAYN_USE_JMX', 'DRAYN_USE_ARMADILLO', 'DRAYN_DETERMINISTIC_RANDOM_SEED' };
    stg.lib = { 'ut', 'mwlapack', 'mwblas' };
    stg.ipath = { libpath('drayn/src'), libpath('disol/src'), libpath('lsbm/src'), path.inc };
    
    if dk.env.is32bits()
        stg.def{end+1} = 'DRAYN_USE_32BITS_NUMBER';
        stg.def{end+1} = 'DRAYN_USE_32BITS_INDEX';
    end
    
    % comment / uncomment as needed
    % stg.def{end+1} = 'DRAYN_SHOW_DEBUG';
    stg.def{end+1} = 'DRAYN_SHOW_INFO';
    
end

function obj = get_libobjects()
    path = get_paths();
    binpath = @(x) fullfile(path.bin,x);
    obj = { binpath('lsbm.o'), binpath('disol.o'), binpath('drayn.o') };
end

function cleanbin()
    path = get_paths();
    jmx_cleanup( path.mex, 'all' );
    jmx_cleanup( path.bin, 'obj' );
end

%----------------------------------------------------------------------
%----------------------------------------------------------------------

function compile_libraries()

    path = get_paths();
    libpath = @(x) fullfile(path.lib,x);
    
    [option,setting] = get_jmx_config();
    option.outdir = path.bin;
    option.mex = false;
    
    cleanbin();
    jmx_build();
    jmx( libpath('drayn/src/drayn.cpp'), option, setting );
    jmx( libpath('disol/src/disol.cpp'), option, setting );
    jmx( libpath('lsbm/src/lsbm.cpp'), option, setting );

end

function compile_models()

    path = get_paths();
    modpath = @(x) fullfile(path.model,x);
    
    files = dk.fs.lsext( path.model, 'cpp' );
    files = dk.mapfun( modpath, files, false );
    libobj = get_libobjects();
    
    [option,setting] = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;
    
    dk.mapfun( @(f) jmx( [{f}, libobj], option, setting ), files );

end

function compile_functions()

    path = get_paths();
    funpath = @(x) fullfile(path.func,x);
    
    files = dk.fs.lsext( path.func, 'cpp' );
    files = dk.mapfun( funpath, files, false );
    libobj = get_libobjects();
    
    [option,setting] = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;
    
    %dk.mapfun( @(f) jmx( f, option, 'ipath', path.inc ), files );
    dk.mapfun( @(f) jmx( [{f}, libobj], option, setting ), files );
    
end

function compile_tests()

    path = get_paths();
    testpath = @(x) fullfile(path.test,x);
    
    files = dk.fs.lsext( path.test, 'cpp' );
    libobj = get_libobjects();
    
    [option,setting] = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;
    
    n = numel(files);
    for i = 1:n
        f = files{i};
        option.outfile = ['test_' dk.str.xrem(f,'cpp')]; % prefix output name
        jmx( [{testpath(f)}, libobj], option, setting ); 
    end

end

%----------------------------------------------------------------------
%----------------------------------------------------------------------

function compile_model(name)

    path = get_paths();
    file = fullfile( path.model, dk.str.xset(name,'cpp') );
    libobj = get_libobjects();
    
    [option,setting] = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;

    jmx( [{file}, libobj], option, setting );

end

function compile_function(name)

    path = get_paths();
    file = fullfile( path.func, dk.str.xset(name,'cpp') );
    
    option = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;
    
    jmx( file, option, 'ipath', path.inc );

end

function compile_test(name)

    path = get_paths();
    testpath = @(x) fullfile(path.test,x);
    
    name = dk.str.xset(name,'cpp');
    libobj = get_libobjects();
    
    [option,setting] = get_jmx_config();
    option.outdir = path.mex;
    option.mex = true;
    option.outfile = ['test_' dk.str.xrem(name,'cpp')];

    jmx( [{testpath(name)}, libobj], option, setting );
    
end
