
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: stimulus( <Config>, <Timevec> );");
    jmx::println("   Config: struct");
    jmx::println("  Timevec: vector\n");
    jmx::println("Available names: constant, heaviside, impulse, harmonic, piecewise, timecourse");
    jmx::println("  Common params: name");
    jmx::println("     + Constant: value(1.0)");
    jmx::println("    + Heaviside: onset(0.0), value(1.0)");
    jmx::println("      + Impulse: loc(0.0), width(1.0), amp(1.0)");
    jmx::println("     + Harmonic: freq(0.0), phi(0.0), base(0.0), amp(0.0)");
    jmx::println("    + Piecewise: stepmat(Nx2[onset, value]), init(0.0)");
    jmx::println("   + Timecourse: time, vals\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // build sigmoid
    auto S = lsbm::stimulus_factory(args.getstruct(0));
    auto x = args.getvec<double>(1);
    auto y = args.mkvec<double>(0,x.length());

    for ( index_t k=0; k < x.length(); k++ ) y[k] = (*S)(x[k]);
}
