
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: inducer( <Config>, <Timevec> );");
    jmx::println("   Config: struct");
    jmx::println("  Timevec: vector\n");
    jmx::println("Available names: step, linear, smoothstep, smoothstep2, smoothstep3, smoothstep4");
    jmx::println("  Common params: name, onset(0.0), length(1.0)\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // build sigmoid
    auto I = lsbm::inducer_factory(args.getstruct(0));
    auto x = args.getvec<double>(1);
    auto y = args.mkvec<double>(0,x.length());

    for ( index_t k=0; k < x.length(); k++ ) y[k] = (*I)(x[k]);
}
