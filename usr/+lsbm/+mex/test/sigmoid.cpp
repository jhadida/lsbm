
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: sigmoid( <Config>, <Timevec> );");
    jmx::println("   Config: struct");
    jmx::println("  Timevec: vector\n");
    jmx::println("Available names: logistic, gaussian, hyperbolic, gumbel, weibull, smoothstep");
    jmx::println("  Common params: name, type('val'), mu(0.0), sigma(1.0)");
    jmx::println("      + Weibull: shape(1.0)\n");
}

struct Type_mapping: public lsbm::nmap<int>
{
    Type_mapping()
    {
        this->define( "val", 0 );
        this->define( "der", 1 );
        this->define( "inv", 2 );
    }
};

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // get parameters
    auto p = args.getstruct(0);
    auto type = p.getstr("type","val");
    const Type_mapping tmap;

    // build sigmoid
    auto S = lsbm::sigmoid_factory(p);
    auto x = args.getvec<double>(1);
    auto y = args.mkvec<double>(0,x.length());

    // compute output
    for ( index_t k=0; k < x.length(); k++ ) 
    switch ( tmap.get_id(type) ) {
        case 0: y[k] = S->val(x[k]); break;
        case 1: y[k] = S->der(x[k]); break;
        case 2: y[k] = S->inv(x[k]); break;
    }
}
