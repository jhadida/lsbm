
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: resample( <TimeSeries>, <Step>, <Burn=0> );");
    jmx::println(" TimeSeries: struct");
    jmx::println("       Step: scalar");
    jmx::println("       Burn: scalar\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // input time-series
    disol::ts_ptr<const double, const double> ts_in;
    disol::jmx_import( args.getstruct(0), ts_in );

    const index_t nt = ts_in.ntime();
    const index_t nc = ts_in.nchan();

    // parse inputs and allocate output
    auto step = args.getnum(1);
    auto burn = args.getnum(2,0.0);
    
    // create new resampled time-series
    auto ts_out = disol::jmx_make_ts<double,double>( args.mkstruct(0), nc, ts_in.tstart()+burn, ts_in.tend(), step );

    for ( auto it = ts_out.iter(); it; it.next() )
        drayn::interp_pchip_state( ts_in, it.time(), it, ts_in.is_fixed_step() );
}
