
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: resample( <TimeSeries>, <Step>, <Options> );");
    jmx::println(" TimeSeries: struct");
    jmx::println("       Step: scalar");
    jmx::println("     Option: struct\n");
    jmx::println("Available options are:");
    jmx::println("  offset(0), consume(true), lag(0), bsize(DISOL_BLOCK_SIZE)\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 3, 1, usage );

    // input time-series
    disol::ts_ptr<const double, const double> ts_in;
    disol::jmx_import( args.getstruct(0), ts_in );

    const index_t nt = ts_in.ntime();
    const index_t nc = ts_in.nchan();

    // parse inputs and allocate output
    auto step = args.getnum(1);
    auto opt = args.getstruct(2);

    const bool consume = opt.getbool("consume",true);
    const real_t lag = opt.getnum("lag",0.0);
    const real_t offset = opt.getnum("offset",0.0);
    const index_t bsize = opt.getnum<index_t>("bsize",DISOL_BLOCK_SIZE);
    const real_t tini = ts_in[0] + offset;

    // create source pool
    lsbm::traits::pool pool_src( nc, bsize, ts_in.is_fixed_step() );
    pool_src.lag(true);

    // create resampler and keep copying timepoints into pool
    drayn::resampler<double> resampler( pool_src, tini, step, consume, lag, bsize );

    for ( index_t k=0; k < nt; k++ ) {
        pool_src.time() = ts_in[k];
        pool_src.state().copy( ts_in.state_unsafe(k) );
        pool_src.increment();
    }
    
    // finalise and export resampled time-series
    disol::jmx_export( resampler.finalise(pool_src), args.mkstruct(0) );
}
