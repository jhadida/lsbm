
#include "lsbm.h"
#include "jmx.h"
using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: activation( <Config>, <Timevec> );");
    jmx::println("   Config: struct");
    jmx::println("  Timevec: vector\n");
    jmx::println("Available names: step, rectifier, linear, softplus, smoothstep, smoothstep2, smoothstep3, smoothstep4, rational, mixed");
    jmx::println("  Common params: name, thresh(0.0), width(1.0), gain(1.0)");
    jmx::println("     + Rational: alpha(1.0)\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // build sigmoid
    auto A = lsbm::activation_factory(args.getstruct(0));
    auto x = args.getvec<double>(1);
    auto y = args.mkvec<double>(0,x.length());

    for ( index_t k=0; k < x.length(); k++ ) y[k] = (*A)(x[k]);
}
