
#include "lsbm.h"
#include "jmx.h"
using namespace drayn::integer;

void usage() {
    jmx::println("\nUsage: downsample( <TimeSeries>, <Step>, <Burn=0>, <Nwin=15> );");
    jmx::println(" TimeSeries: struct");
    jmx::println("       Step: scalar");
    jmx::println("       Burn: scalar");
    jmx::println("       Nwin: scalar\n");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // input time-series
    disol::ts_ptr<const double, const double> ts_in;
    disol::jmx_import( args.getstruct(0), ts_in );
    const bool fixed = ts_in.is_fixed_step();

    const uidx_t nt = ts_in.ntime();
    const uidx_t nc = ts_in.nchan();

    // parse inputs and allocate output
    auto step = args.getnum(1);
    auto burn = args.getnum(2,0.0);
    
    // allocate output time-series
    auto ts_out = disol::jmx_make_ts<double,double>( args.mkstruct(0), nc, ts_in.tstart()+burn, ts_in.tend(), step );
    const uidx_t nafter = ts_out.ntime();
    DRAYN_ASSERT_ERR( nafter > 1, "Step is too large." )

    // create timecourse for downsampling
    const uidx_t nw = args.getnum<uidx_t>(3,15);
    const uidx_t ws = nw / 2;
    const auto w = drayn::window_norm<double>( drayn::win::Hamming, nw );
    const auto t = drayn::range<double>( ts_in.tstart(), ts_in.tend(), 2*step/(nw-1) );

    // get first/last point by copy/interpolation
    drayn::interp_pchip_state( ts_in, ts_out.tstart(), ts_out.state_unsafe(0), fixed );
    drayn::interp_pchip_state( ts_in, ts_out.tend(), ts_out.state_unsafe(nafter-1), fixed );

    // initialise buffer
    auto buf = drayn::buf_weighted<double>( w, ts_in.nchan() );

    uidx_t tn = 0;
    for (; tn < (nw+1)/2; tn++ )
        drayn::interp_pchip_state( ts_in, t[tn], buf.next(), fixed );

    // use buffer to estimate internal states
    for ( uidx_t i=1; (i < nafter-1) && (tn+ws < t.size()); i++ ) {
        for ( uidx_t j=0; j < ws; j++, tn++ )
            drayn::interp_pchip_state( ts_in, t[tn], buf.next(), fixed );

        buf.statesum( ts_out.state_unsafe(i) );
    }
}
