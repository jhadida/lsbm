
#include <iostream>
#include "lsbm.h"
#include "jmx.h"
using namespace drayn::integer;

void usage() {
    jmx::println("\nUsage: window( <Name>, <N=31> );");
    jmx::println("   Name: string");
    jmx::println("      N: scalar\n");
    jmx::println("Available names: blackman, flattop, gauss, hamming, hann, tukey\n");
}

struct Window_mapping: public lsbm::nmap<drayn::win>
{
    Window_mapping()
    {
        this->define( "blackman",   drayn::win::Blackman );
        this->define( "flattop",    drayn::win::Flattop );
        this->define( "gauss",      drayn::win::Gauss );
        this->define( "hamming",    drayn::win::Hamming );
        this->define( "hann",       drayn::win::Hann );
        this->define( "tukey",      drayn::win::Tukey );
    }
};

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 2, 1, usage );

    // get parameters
    auto name = args.getstr(0);
    const uidx_t nw = args.getnum<uidx_t>(1,31);
    const Window_mapping wmap;

    // compute window
    auto w = drayn::window<double>( wmap.get_id(name), nw );
    auto w_out = args.mkvec<double>( 0, nw );

    for ( uidx_t k=0; k < nw; k++ ) w_out[k] = w[k];
}
