
#include "lsbm.h"
#include "jmx.h"

using namespace jmx_types; // index_t, integ_t, real_t

void usage() {
    jmx::println("\nUsage: accuminterp( <TimeSeries>, <Src>, <Dst>, <W>, <T> );");
    jmx::println(" TimeSeries: struct");
    jmx::println("    Src,Dst: integer vectors");
    jmx::println("        W,T: double vectors");
}

void mexFunction( int nargout, mxArray *out[],
                  int nargin, const mxArray *in[] ) 
{
    // redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 5, 1, usage );

    // input time-series
    disol::ts_ptr<const double, const double> ts_in;
    disol::jmx_import( args.getstruct(0), ts_in );

    const index_t nt = ts_in.ntime();
    const index_t nc = ts_in.nchan();

    // parse inputs and allocate output
    auto src = args.getvec(1);
    auto dst = args.getvec(2);
    auto w = args.getvec(3);
    auto t = args.getvec(4);

    auto ne = w.length();
    DRAYN_ASSERT_ERR( src.length() == ne, "Bad length: src." )
    DRAYN_ASSERT_ERR( dst.length() == ne, "Bad length: dst." )
    DRAYN_ASSERT_ERR( t.length() == ne, "Bad length: t." )
    
    // create new resampled time-series
    auto wsum = args.mkvec<double>(0,nc);
    index_t ks, kd;

    for ( index_t k=0; k < nc; k++ ) wsum[k] = 0.0;
    for ( index_t k=0; k < ne; k++ ) {
        ks = static_cast<index_t>(src[k])-1;
        kd = static_cast<index_t>(dst[k])-1;
        DRAYN_ASSERT_ERR( ks < nc && kd < nc, "Channel index out of bounds." )

        // JH: too slow
        // wsum[kd] += w[ks]*ts_in.pinterp(t[ks],ks);
        // wsum[ks] += w[kd]*ts_in.pinterp(t[kd],kd);

        wsum[kd] += w[ks]*ts_in.linterp(t[ks],ks);
        wsum[ks] += w[kd]*ts_in.linterp(t[kd],kd);
    }
}
