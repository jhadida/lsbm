
//==================================================
// @title        icb_unit.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "helper.h"



		/********************     **********     ********************/
		/********************     **********     ********************/



void usage()
{
	drayn_print("[USAGE: icb_unit]:");
	drayn_print("Simulation of an instantaneous Conductance-based E/I unit.");
	drayn_print("See inc/helper.h for more details about the required input structure.");
	drayn_print("See lsbm/system/library/icb for more details about the model.");
}

// ------------------------------------------------------------------------

void mexFunction(	int nargout, mxArray *out[],
					int nargin, const mxArray *in[] )
{
	// redirect stdout and stderr to the Matlab console
    jmx::cout_redirect();
    jmx::cerr_redirect();

    // wrap input and output arguments
    auto args = jmx::Arguments( nargout, out, nargin, in );
    args.verify( 1, 1, usage ); 

	// select the desired helper
	using system_type   = lsbm::System_InstConductanceBased_unit;
	using problem_type  = lsbm::traits::problem_ivp;
	using euler_helper  = LSBM_Helper<system_type,problem_type,disol::stepper_Euler>;
	using rk2_helper    = LSBM_Helper<system_type,problem_type,disol::stepper_RK2>;
	using rk4_helper    = LSBM_Helper<system_type,problem_type,disol::stepper_RK4>;
	using rk6_helper    = LSBM_Helper<system_type,problem_type,disol::stepper_RK6>;
	using rk45_helper   = LSBM_Helper<system_type,problem_type,disol::stepper_RK45_Dopri>;
	using ross_helper   = LSBM_Helper<system_type,problem_type,disol::stepper_Ross>;
	using rk45a_helper  = LSBM_Helper<system_type,problem_type,disol::stepper_RK45_Dopri,disol::integrator_adaptive_step>;
	using rk853_helper  = LSBM_Helper<system_type,problem_type,disol::stepper_RK853_Dopri>;
	using rk853a_helper = LSBM_Helper<system_type,problem_type,disol::stepper_RK853_Dopri,disol::integrator_adaptive_step>;

	using helper_type   = rk4_helper;
	using handle_type   = helper_type::handle_type;
	using plugin_type   = disol::plugin_clamp<handle_type>;


	helper_type helper;
	DRAYN_ASSERT_R( helper.configure(args.getstruct(0)), "Could not extract configuration." )

	helper.run();
	helper.output(args.mkstruct(0));
}
