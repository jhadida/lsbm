#ifndef LSBM_HELPER_H_INCLUDED
#define LSBM_HELPER_H_INCLUDED

//==================================================
// @title        helper.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "jmx.h"
#include "drayn.h"
#include "disol.h"
#include "lsbm.h"

namespace dr = drayn;
using namespace dr::integer;

/**
 * Helper class to write Mex files for LSBMs:
 *
 * 	- The system class must derive from lsbm::NeuronalSystem.
 * 	- The stepper and integrator can be chosen arbitrarily (although 
 * 	  compatibility constraints may cause compilation errors).
 *
 * A typical Mex file looks like:
 *
 * void mexFunction( int nargout, mxArray *out[],
 *                   int nargin, const mxArray *in[] ) 
 * {
 *     jmx::cout_redirect();
 *     jmx::cerr_redirect();
 * 
 *     auto args = jmx::Arguments( nargout, out, nargin, in );
 * 	   args.verify(1,1);
 *
 * 	   using System = System_WilsonCowan_network;
 * 	   using Problem = lsbm::traits::problem_hist;
 * 	   using Helper = LSBM_Helper<System,Problem>;
 *
 *     Helper helper(args.getstruct(0));
 *     helper.run();
 *     helper.output(args.mkstruct(0,{"random_seed","solution"}));
 *     helper.timon.report(); // optional: see the breakdown of runtime
 * }
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



template <
	class System, class Problem,
	template <typename...> class Stepper = disol::stepper_RK4,
	template <typename...> class Integrator = disol::integrator_fixed_step
>
struct LSBM_Helper
{
	using system_type     = System;
	using problem_type    = Problem;
	using stepper_type    = Stepper<system_type>;
	using integrator_type = Integrator<stepper_type>;

	using handle_type     = typename stepper_type::handle_type;
	using events_type     = disol::prop_event<handle_type>;
    using signal_type     = typename events_type::signal_type;
    using slot_type       = typename events_type::slot_type;

	using property_type   = typename stepper_type::property_type;
	using time_type       = typename system_type::time_type;
	using value_type      = typename system_type::value_type;
	using resampler_type  = dr::resampler<value_type,time_type>;
    
    // ----------  =====  ----------
    
	// Inputs
	problem_type   in_problem;
	property_type  in_prop;

	// Internal components
	system_type      system;
	integrator_type  integrator;

	size_t random_seed;
	resampler_type resampler;
	time_type mindel, maxdel;
	dr::time_monitor timon;

	// Quick access to events
	inline events_type& events() { return in_prop.event; }

	// ----------  =====  ----------

	LSBM_Helper() { clear(); }
	LSBM_Helper( const jmx::Struct& cfg ) { configure(cfg); }

	void clear() 
    {
		system     	.clear();
		integrator 	.clear();
		resampler 	.clear();
		timon      	.clear();
	}

	// before running the simulation
	bool configure( const jmx::Struct& cfg )
	{
		clear(); 
        timon.start("config");

		DRAYN_ASSERT_RF( cfg.has_fields({"problem","properties","system","other"}),
			"[lsbm_helper.configure] Missing field(s)." )
		DRAYN_DEBUG("Checked fields.")

		// FIELD 'verbose': verbose level
		lsbm::verbosity(cfg.getstr("verbose","default"));

		// FIELD 'properties': integrator properties (cf disol.core.data)
		DRAYN_ASSERT_RF( disol::jmx_import(  cfg.getstruct("properties"), in_prop ),
			"[lsbm_helper.configure] Could not extract 'properties'." )
		DRAYN_DEBUG("Imported properties.")

		// FIELD 'problem': initial value problem (cf disol.core.data)
		DRAYN_ASSERT_RF( disol::jmx_import( cfg.getstruct("problem"), in_problem ),
			"[lsbm_helper.configure] Could not extract 'problem'." )
		DRAYN_DEBUG("Imported problem.")

		// FIELD 'system': configure neuronal system (cf lsbm.system)
		DRAYN_ASSERT_RF( system.configure(cfg.getstruct("system")),
			"[lsbm_helper.configure] Could not configure system." )
		DRAYN_DEBUG("Configured system.")

		// computing min/max delay can be costly; do it only once
		mindel = system.min_delay();
		maxdel = system.max_delay();

		// FIELD 'other': other parameters
		DRAYN_ASSERT_RF( other(cfg.getstruct("other")),
			"[lsbm_helper.configure] Could not complete optional config." )
		DRAYN_DEBUG("Applied other options.")

		// information
		lsbm::print_info( 
			"[lsbm_helper.configure] Integration problem: Tstart=%g, Tend=%g", 
			in_problem.tstart, in_problem.tend );

		timon.stop("config");
		return check();
	}

	bool check()
	{
		DRAYN_ASSERT_RF( maxdel <= in_problem.hspan(), 
			"[lsbm_helper.check] History should be longer than the largest delay." )

		DRAYN_ASSERT_RF( mindel >= in_prop.step.max_step, 
			"[lsbm_helper.check] Smallest delay should be larger than the largest time-step." )

		return true;
	}

	// run the simluation
	void run()
	{
		timon.start("run");
		system.records.lag(true); // exclude current timepoint from interpolation
		integrator.integrate( system, in_problem, in_prop );
		system.records.lag(false); // include last timepoint in final records
		timon.stop("run");
	}

	// Mex output
	void output( jmx::Struct out )
	{
		timon.start("output");

		// export random seed for reproducibility
		out.mknum<size_t>("random_seed",random_seed);

		// export solution (optionally resampled)
		if ( resampler )
			disol::jmx_export( resampler.finalise(system.records), out.mkstruct("solution") );
		else 
			disol::jmx_export( system.records, out.mkstruct("solution") );

		// additional output defined by system
		system.additional_outputs(out);

		timon.stop("output");
	}

protected:

	// used internally by configure
	bool other( const jmx::Struct& cfg )
	{
		const time_type tstart = in_problem.tstart;
		const time_type tspan = in_problem.tspan();

		// seed random generator
		// should be unsigned long int
		// 
		random_seed = cfg.getnum<size_t>( "random_seed", dr::random_seed_generator::get() );
		dr::deterministic_seed_generator::seed( random_seed );

		// setup resampler
		// specifies resampling FREQUENCY in Hz
		// 
		const time_type resample_freq = cfg.getnum( "resample", 0.0 );
		if ( resample_freq > 0.0 ) resampler.init( 
			system.records, tstart, 1/resample_freq,
			true, maxdel, system.records.bsize()
		);

		// setup induction
		// struct with fields: name, length, wait
		// 
		jmx::Struct induction;
		if ( cfg.has_field("induction") ) 
		{
			induction.wrap(cfg["induction"]);
			system.inducer = lsbm::inducer_factory(
				induction.getstr("name","step"),
				tstart + maxdel + induction.getnum("wait",0.0),
				induction.getnum("length",tspan/100)
			);
		}

		return true;
	}

};

#endif
