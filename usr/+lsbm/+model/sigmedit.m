function u = sigmedit( u, name, val )
%
% u = lsbm.model.sigmedit( u, name, val )
%
% Apply transformation to the sigmoid parameters.
% NOTE: this assumes a 2-populations model.
%
% Available transformations are:
%
%   s-scale    Scale all sigmoid parameters.
%   s-m/s      Set mu proportionally to sigma
%   s-s/m      Set sigma proportionally to mu
%   s-m        Set muE, keeping muI/muE constant
%   s-s        Set sigmaE, keeping sigmaI/sigmaE constant
%   s-mi/me    Set muI proportionally to muE
%   s-me/mi     .. idem with muE
%   s-si/se     .. idem with sigmaI
%   s-se/si     .. idem with sigmaE
%   s-i/me     Set muI proportionally to muE, keeping sigmaI/muI constant.
%   s-i/se      .. idem with sigmaI
%   s-e/mi      .. idem with muE
%   s-e/si      .. idem with sigmaE
%
% JH

    switch lower(name)
        
        % scale sigmoid params
        case 's-scale'
            u.E.S.mu = val*u.E.S.mu;
            u.E.S.sigma = val*u.E.S.sigma;

            u.I.S.mu = val*u.I.S.mu;
            u.I.S.sigma = val*u.I.S.sigma;
            
        % change mu/sigma within each subpopulation
        case 's-m/s'
            u.E.S.mu = val*u.E.S.sigma*sign(u.E.S.mu);
            u.I.S.mu = val*u.I.S.sigma*sign(u.I.S.mu);
        case 's-s/m'
            u.E.S.sigma = val*abs(u.E.S.mu);
            u.I.S.sigma = val*abs(u.I.S.mu);
            
        % set excitatory param, keeping inh/exc constant
        case 's-m'
            r = u.I.S.mu / u.E.S.mu;
            u.E.S.mu = val;
            u.I.S.mu = r*val;
        case 's-s'
            r = u.I.S.sigma / u.E.S.sigma;
            u.E.S.sigma = val;
            u.I.S.sigma = r*val;

        % change mu relative to the other subpop, keeping mu/sigma constant
        case 's-i/me'
            r = abs(u.I.S.sigma / u.I.S.mu);
            u.I.S.mu = val*u.E.S.mu;
            u.I.S.sigma = abs(r*u.I.S.mu);
        case 's-e/mi'
            r = abs(u.E.S.sigma / u.E.S.mu);
            u.E.S.mu = val*u.I.S.mu;
            u.E.S.sigma = abs(r*u.E.S.mu);

        % change sigma relative to the other subpop, keeping mu/sigma constant
        case 's-i/se'
            r = u.I.S.mu / u.I.S.sigma;
            u.I.S.sigma = val*u.E.S.sigma;
            u.I.S.mu = r*u.I.S.sigma;
        case 's-e/si'
            r = u.E.S.mu / u.E.S.sigma;
            u.E.S.sigma = val*u.I.S.sigma;
            u.E.S.mu = r*u.E.S.sigma;
            
        % change relative to the other subpop
        case 's-mi/me'
            u.I.S.mu = val*u.E.S.mu;
        case 's-me/mi'
            u.E.S.mu = val*u.I.S.mu;
        case 's-si/se'
            u.I.S.sigma = val*u.E.S.sigma;
        case 's-se/si'
            u.E.S.sigma = val*u.I.S.sigma;
            
        otherwise
            error('Unknown command: %s',name);
        
    end

end