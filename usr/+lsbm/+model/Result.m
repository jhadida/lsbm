classdef Result < handle
    
    properties (Abstract,Transient,Dependent)
        n_nodes, n_units
    end
    properties
        cfg     % config structure used for execution
        out     % output of Mex file
    end
    
    properties (Transient,Dependent)
        n_states, n_times
    end
    properties (Transient,Dependent,Hidden)
        ns, nt, ts
    end
    
    methods (Abstract)
        
        %
        % Constructor should take in input:
        %   1. the config structure used for execution
        %   2. the structure output by the Mex file
        %
        
        % throw error if input config / output are invalid
        self = check(self);
        
        % time-series of unit k (may have more than 1 signal)
        ts = unit(self,k);
        
        % time-series of node k (may have more than 1 signal)
        ts = node(self,k);
        
        % show solution
        fig = show(self,varargin);
        
        % show unit timecourse
        fig = show_unit(self,k,varargin);
        
    end
    
    methods
        
        function self = assign(self,cfg,out)
            assert( dk.is.struct(cfg) && dk.is.struct(out,{'solution'}), 'Inputs should be structs.' );
            
            self.cfg = cfg;
            self.out = out;
            self.check();
        end
        
        % resample the output solution
        function self = resample(self,varargin)
            self.ts.resample(varargin{:});
        end
        
        % ant.TimeSeries object wrapping the output solution
        function ts = get.ts(self), ts = self.out.solution; end
        
        % dimensions
        function n = get.n_states(self), n = self.ts.ns; end
        function n = get.n_times(self), n = self.ts.nt; end
        
        function n = get.nt(self), n = self.n_times; end
        function n = get.ns(self), n = self.n_states; end
        
        % check system type
        function y = is_network(self), y = self.n_unit > 1; end
        function y = is_unit(self), y = self.n_unit == 1; end
        
        % save to / load from file
        function s = save(self,filename)
            s.ver = '0.1';
            s.cfg = self.cfg;
            s.out = self.out;
            dk.save( filename, s );
        end
        
        function self = load(self,s)
            if ischar(s), s = load(s); end
            switch s.ver
                case '0.1'
                    self.cfg = s.cfg;
                    self.out = s.out;
                otherwise
                    error( 'Unknown version: %s', s.ver );
            end
            self.check();
        end
        
    end
    
end