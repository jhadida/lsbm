function fig = phase_plane( unit, input, varargin )
%
% fig = phase_plane( unit, input, varargin )
%
% INPUTS
% ------
%   
%     unit  Instance of lsbm.model.wc.Unit
%    input  Scalar Pe, or vector [Pe,Pi]
%
% OPTIONS
% -------
%
%    figure  Figure handle (default: figure())
%   npoints  Number of points for each nullcline (default: 200)
%   nquiver  Size of the square quiver grid (default: 30)
%     arrow  Scaling of quiver arrow size (default: 1)
%
% Draw the Wilson-Cowan phase-plane for a given unit configuration. 
%
% JH

    assert( isa(unit,'lsbm.model.wc.Unit'), 'Bad input unit.' );
    param = unit.param;

    % parse inputs
    opt = dk.getopt( varargin, 'figure', nan, 'npoints', 200, 'nquiver', 30, 'arrow', 1 );
    
    fig = opt.figure; 
    Npt = opt.npoints; % number of points for each nullcline
    Nqv = opt.nquiver; % size of the quiver grid
    arw = opt.arrow; % arrow size (normalised)
    
    if isscalar(Nqv), Nqv=Nqv*[1 1]; end
    if isscalar(input), input=[input,0]; end
    
    
    % select figure
    if ishandle(fig)
        fig = figure(fig); hold off;
    else
        fig = figure();
    end
    
    % extract input
    E_input = input(1);
    I_input = input(2);

    % extract parameters of interest
    Enode = param.E;
    Inode = param.I;
    
    E_am = Enode.am;
    I_am = Inode.am;
    
    E_tau = Enode.tau;
    I_tau = Inode.tau;
    
    [cee,cei,cie,cii] = dk.deal(dk.struct.values( param, {'cee','cei','cie','cii'} ));
    
    % sigmoid stuff
    [Se_val,Se_inv] = lsbm.util.sigm2handle( Enode.S, dk.struct.get(Enode,'norm',false) );
    [Si_val,Si_inv] = lsbm.util.sigm2handle( Inode.S, dk.struct.get(Inode,'norm',false) );

    
    % dE/dt = 0 and dI/dt = 0 (x=E, y=I)
    %Ne = @(x) (cee*x - Se_inv(x./(1-E_rp*x)) + E_input) / cie;
    %Ni = @(y) (cii*y + Si_inv(y./(1-I_rp*y)) - I_input) / cei;
    Ne = @(x) (-cee*x + Se_inv(x./(1-E_am*x)) - E_input) / cie;
    Ni = @(y) (-cii*y + Si_inv(y./(1-I_am*y)) - I_input) / cei;

    xe = linspace(0,1,Npt); xe = xe(2:end-1); ye = Ne(xe);
    yi = linspace(0,1,Npt); yi = yi(2:end-1); xi = Ni(yi);

    he = plot( xe, ye, 'b-', 'LineWidth', 1.5 ); hold on;
    hi = plot( xi, yi, 'r-', 'LineWidth', 1.0 ); 

    
    % draw vector field
    [Gx,Gy] = meshgrid( linspace(0,1,Nqv(1)), linspace(0,1,Nqv(2)) );
    %Vx = ( -Gx + (1-E_rp*Gx).*Se_val( cee*Gx - cie*Gy + E_input )) / E_tau;
    %Vy = ( -Gy + (1-I_rp*Gy).*Si_val( cei*Gx - cii*Gy + I_input )) / I_tau;
    Vx = ( -Gx + (1-E_am*Gx).*Se_val( cee*Gx + cie*Gy + E_input )) / E_tau;
    Vy = ( -Gy + (1-I_am*Gy).*Si_val( cei*Gx + cii*Gy + I_input )) / I_tau;
    
    quiver(Gx,Gy,Vx,Vy,arw);
    
    
    % finalise plot
    axis equal; grid off; hold off;
    xlim([0 1]); ylim([0 1]); 
    xlabel('E'); ylabel('I'); 
    legend([he,hi],'dE/dt=0','dI/dt=0');
    title('Phase-plane'); 
    
    
    % add selection callback
    fig.UserData.unit = unit;
    fig.UserData.tspan = 50*max(E_tau,I_tau);
    fig.UserData.tstep = min(E_tau,I_tau) / 10;
    
    function callback( Ge, Gi, varargin )
        dk.print('Selected point (E=%.2f,I=%.2f), running simulation.', Ge, Gi );

        tspan = fig.UserData.tspan;
        tstep = fig.UserData.tstep;

        res = unit.input(input).run( tspan, tstep, [Ge,Gi] );
        res.show_unit();
    end
    dk.widget.menu_ginput( @callback, fig );

end
