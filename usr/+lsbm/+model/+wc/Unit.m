classdef Unit < lsbm.model.System
%
% Parameters:
%
%   cee,cei,cie,cii             Local couplings (cxy = from x to y)
%   E/I                         Excitatory/Inhibitory subpop
%       am                          amortisation factor
%       tau                         time-constant
%       S{ *name, mu, sigma }       sigmoid (non-linearity)
%
% JH

    methods
        
        function self = Unit(u)
            self.assign(u);
        end
        
        function check(self)
            assert( dk.is.struct(self.param,{'cee','cei','cie','cii','E','I'}), 'Bad params' );
            assert( dk.is.struct(self.param.E,{'am','tau','S'}), 'Bad excitatory params' );
            assert( dk.is.struct(self.param.I,{'am','tau','S'}), 'Bad inhibitory params' );
            
            if ~isfield(self.param.E.S,'name')
                self.param.E.S.name = 'logistic';
            end
            if ~isfield(self.param.I.S,'name')
                self.param.I.S.name = 'logistic';
            end
        end
        
        function [sim,cfg] = run(self,tspan,tstep,init)
        %
        % [res,cfg] = run( tspan, tstep, init=[0,0] )
        %
        % Run simulation and return instance of lsbm.model.wc.Result
        %
            
            if nargin < 4, init=[0,0]; end
            
            % create config
            cfg = lsbm.Config('quiet');
            cfg.set_system(self.param);
            
            % sampling params
            samp.span = tspan;
            samp.step = tstep;
            
            % run simulation
            sim = lsbm.run( cfg, @lsbm.mex.wc_unit, @lsbm.model.wc.Result, samp, init );
            
        end
        
        function self = input(self,in)
        % 
        % input( Pe )           scalar (Pi=0)
        % input( [Pe, Pi] )     scalars (constant inputs)
        % input( {Pe, Pi} )     structures (cf lsbm.make.stim_*)
        %
        % Set external inputs to excitatory/inhibitory populations.
        %
        
            if iscell(in)
                Pe = in{1};
                Pi = in{2};
            elseif isnumeric(in)
                if isscalar(in), in=[in,0]; end
                const = @(v) lsbm.make.stim_const([],v);
                Pe = const(in(1));
                Pi = const(in(2));
            else
                error( 'Unknown input type.' );
            end
            
            self.param.E.P = Pe;
            self.param.I.P = Pi;
            
        end
        
        function self = edit(self,varargin)
        %
        % self = edit( Name1, Value1, Name2, ... )
        %
        % Apply transformation to unit parameters, with one (or a series of) operation(s).
        % All transformations take one parameter (numeric scalar).
        %
        % Available transformations are:
        %
        %   a-val      Set amE, keeping amI/amE constant.
        %
        %   c-scale    Scale all coupling coefficients.
        %   c-escale    .. only cee, cei
        %   c-iscale    .. only cii, cie
        %   c-i/e      Scale cii and cie, controlling abs(cii+cie)/(cee+cei)
        %   c-e/i      Scale cee and cei, controlling (cee+cei)/abs(cii+cie)
        %   c-ei/e     Set cei and cee, controlling the ratio cei/(cee+cei)
        %
        %   t-val      Set tauE, keeping tauI/tauE constant.
        %   t-i/e      Set tauI proportionally to tauE.
        %   t-e/i      Set tauE proportionally to tauI.
        %
        % See also: lsbm.model.sigmedit
        %
        
            const = @(v) lsbm.make.stim_const([],v); % alias
            
            n = (nargin-1)/2;
            u = self.param;
            for i = 1:n

                name = varargin{2*i-1};
                val  = varargin{2*i};

                switch lower(name)
                    
                    % set inputs
                    case 'pe'
                        u.E.P = const(val);
                    case 'pi'
                        u.I.P = const(val);

                    % set refractory periods (amortisation)
                    case 'a-val'
                        a = u.I.am / max(eps,u.E.am);
                        u.E.am = val;
                        u.I.am = a*val;

                    % scale coupling coefficients
                    case 'c-scale'
                        u.cee = val*u.cee;
                        u.cei = val*u.cei;
                        u.cie = val*u.cie;
                        u.cii = val*u.cii;

                    case 'c-escale'
                        u.cee = val*u.cee;
                        u.cei = val*u.cei;
                    case 'c-iscale'
                        u.cii = val*u.cii;
                        u.cie = val*u.cie;

                    % change the ratio (cii+cie)/(cee+cei) by scaling inhibitory couplings
                    case 'c-i/e'
                        es = u.cee+u.cei;
                        is = u.cii+u.cie;

                        fac = abs(val * es / is);
                        u.cii = fac*u.cii;
                        u.cie = fac*u.cie;

                    % change the ratio (cee+cei)/(cii+cie) by scaling excitatory couplings
                    case 'c-e/i'
                        es = u.cee+u.cei;
                        is = u.cii+u.cie;

                        fac = abs(val * is / es);
                        u.cee = fac*u.cee;
                        u.cei = fac*u.cei;

                    % change the ratio cei/(cei+cee)
                    case 'c-ei/e'
                        es = u.cee+u.cei;
                        assert( val > 0 && val < 1, 'Bad ratio.' );

                        u.cei = val*es;
                        u.cee = (1-val)*es;

                    % change tauE, keeping tauI/tauE constant
                    case 't-val'
                        a = u.I.tau / u.E.tau;
                        u.E.tau = val;
                        u.I.tau = a*val;

                    % change tauI wrt tauE, or tauE wrt tauI
                    case 't-i/e'
                        u.I.tau = val*u.E.tau;
                    case 't-e/i'
                        u.E.tau = val*u.I.tau;

                    otherwise
                        % sigmoid transformations (elsewhere to avoid duplication)
                        if strcmp( name(1:2), 's-' )
                            u = lsbm.model.sigmedit(u,name,val);
                        else
                            error('Unknown command: %s',name);
                        end

                end
            end
            self.param = u;
            self.check();
            
        end
        
        function info(self)
        
            u = self.param;
            print_subpop( 'E', u.E );
            print_subpop( 'I', u.I );

            dk.print( 'cee: %.2f', u.cee );
            dk.print( 'cei: %.2f', u.cei );
            dk.print( 'cie: %.2f', u.cie );
            dk.print( 'cii: %.2f', u.cii );

            fprintf('\n');
            dk.print( 'Ci/Ce: %.2f', -(u.cie + u.cii)/(u.cee + u.cei) );
            dk.print( 'Me/Ce: %.2f', u.E.S.mu / (u.cee + u.cei) );
            
        end
        
    end
    
    methods
        
        function fig = phase_plane(self,in,varargin)
        %
        % fig = phase_plane(input,varargin)
        %
        % Draw phase-plane, see below for additional options.
        %
        % See also: lsbm.model.wc.phase_plane
        
            fig = lsbm.model.wc.phase_plane(self,in,varargin{:});
        end
        
        function varargout = sweep(self,varargin)
        %
        % [sdev,freq] = sweep(T1,V1,T2,V2,varargin)
        %
        % Sweep WC unit for transformations T1 and T2 (can be vectors).
        % Outputs are m-by-n matrices, where n=#Pe and m=#Pi.
        %
        % See also: lsbm.model.wc.sweep
        
            varargout = cell(1,nargout);
            [varargout{:}] = lsbm.model.wc.sweep(self,varargin{:});
        end
        
        function y = has_normalised_sigm(self)
        %
        % y = has_normalised_sigm()
        %
        % Check if either sigmoid is normalised.
        %
            Se = self.param.E.S;
            Si = self.param.I.S;
        
            y = dk.struct.get(Se,'norm',false) || dk.struct.get(Si,'norm',false);
        end
        function self = normalise_sigm(self,val)
        %
        % normalise_sigm()
        % normalise_sigm(false)
        %
        % Enable/disable sigmoid normalisation for current unit.
        %
            if nargin < 2, val=true; end
            self.param.E.S.norm = val;
            self.param.I.S.norm = val;
        end
        
        function Pi = target_Pi(self,Ih)
        %
        % Pi = target_Pi( Ih )
        %
        % Compute value of Pi such that the value of I for E=1/2 equals the input Ih.
        % This is computed from the inhibitory nullcline (with un-normalised sigmoid).
        %

            assert( ~self.has_normalised_sigm(), 'Not implemented for normalised sigmoids.' );
            Pi = -self.param.cii*Ih - self.param.cei/2 + self.param.I.S.mu + ...
                self.param.I.S.sigma*log( Ih ./ (1 - (self.param.I.am+1)*Ih) );
            
        end
        
        function Pe = oscin(self,Pi,ndigit)
        %
        % Pe = oscin( Pi=0, ndigit=3 )
        %
        % Find oscillatory threshold of excitatory input using dichotomy.
        %
        % See also: lsbm.util.dichotomy, lsbm.model.wc.status
        
            if nargin < 2, Pi=0; end
            if nargin < 3, ndigit=3; end

            muE = self.param.E.S.mu;
            tau = self.param.E.tau;
            Pe = lsbm.util.dichotomy( @is_silent, abs(muE), ndigit );

            function s = is_silent(Pe)
                res = self.input([Pe,Pi]).run( 120*tau, 1e-3 );
                tse = res.excitatory(1).burn( 50*tau );
                s = strcmp( lsbm.model.wc.status(tse,true), 'silent' );
            end
            
        end
        
    end
    
end

function print_subpop( name, param )

    dk.print( '%s subpop', name );
    dk.print( '   tau: %.1f ms', 1000*param.tau );
    dk.print( '    am: %.2f', param.am );
    dk.print( '    mu: %.2f', param.S.mu );
    dk.print( ' sigma: %.2f', param.S.sigma );
    dk.print( '  norm: %s', dk.tostr(dk.struct.get( param, 'norm', false ),'tf') );
    fprintf('\n'); % skip line
    
end