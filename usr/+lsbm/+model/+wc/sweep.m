function [sdev,freq] = sweep( unit, T1, V1, T2, V2, varargin )
%
% [sdev,freq] = lsbm.model.wc.sweep( unit, Pe, Pi=0 )
%
% Sweep WC unit for to parameter transformations.
% Returns standard deviation and oscillation frequency peak for each combination,
% as m-by-n matrices where n=#V1 and m=#V2.
%
% Simulation length is 120*tauE, burning the first 50*tauE.
%
% JH

    assert( isa(unit,'lsbm.model.wc.Unit'), 'Bad input unit.' );
    assert( ~isscalar(V1), 'First transformation cannot be scalar.' );
    
    n1 = numel(V1);
    n2 = numel(V2);
    
    tauE = unit.param.E.tau;
    sdev = zeros(n2,n1);
    freq = zeros(n2,n1);
    
    for i = 1:n1
    for j = 1:n2
        res = unit.edit(T1,V1(i),T2,V2(j)).run( 120*tauE, 1e-3 );
        ts = res.excitatory(1).burn( 50*tauE );

        sdev(j,i) = ts.sdev();
        freq(j,i) = ant.dsp.freqmode(ts);
    end
    end
    
    if nargout == 0
        figure; colormap('jet');
        if isscalar(V2)
            dk.ui.line( [V1(:),sdev(:)], freq(:), varargin{:} );
            xlabel(T1); ylabel('St.Dev'); 
            h = colorbar; h.Label.String = 'Frequency (Hz)';
        else
            dk.ui.surface( ...
                V1, V2, sdev, 'color', freq, 'FaceAlpha', 0.95, ...
                'xlabel', T1, 'ylabel', T2, 'zlabel', 'St.Dev', ...
                'clabel', 'Frequency (Hz)', varargin{:} ...
            );
        end
    end
    
end

% % separate display of sdev / freq
% if nargout == 0
%     figure;
%     if isscalar(Pi)
%         subplot(2,1,1); plot( Pe, sdev );
%         xlabel('Exc.Input'); ylabel('St.Dev');
%         title('Standard-dev vs Pe');
% 
%         subplot(2,1,2); plot( Pe, freq );
%         xlabel('Exc.Input'); ylabel('Frequency (Hz)');
%         title('Frequency mode vs Pe');
%     else
%         subplot(2,1,1); dk.ui.surface( Pe, Pi, sdev );
%         xlabel('Exc.Input'); ylabel('Inh.Input'); zlabel('St.Dev');
%         title('Standard-dev vs Input');
% 
%         subplot(2,1,2); dk.ui.surface( Pe, Pi, freq );
%         xlabel('Exc.Input'); ylabel('Inh.Input'); zlabel('Frequency (Hz)');
%         title('Frequency mode vs Input');
%     end
% end