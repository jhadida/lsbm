classdef Result < lsbm.model.Result
    
    properties (Transient,Dependent)
        n_nodes, n_units;
    end
    
    methods
        
        function self = Result(varargin)
            switch nargin
                case 1
                    self.load(varargin{1});
                case 2
                    self.assign(varargin{:});
                otherwise
                    error( 'Bad input.' );
            end
        end
        
        function res = clone(self)
            res = lsbm.model.wc.Result(self.cfg,self.out);
        end
        
        function ts = excitatory(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.ts.select(2*k-1); 
        end
        function ts = inhibitory(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.ts.select(2*k); 
        end
        
        function pa = analyse(self,varargin)
            pa = nst.make.pipeline(varargin{:});
            pa.analyse( self.excitatory );
        end
        
    end
        
    methods
        
        function n = get.n_nodes(self), n = self.n_states; end
        function n = get.n_units(self), n = self.n_nodes / 2; end
        
        function ts = node(self,k), ts = self.ts.select( k ); end
        function ts = unit(self,k), ts = self.ts.select( 2*k + [-1,0] ); end
        
        function check(self)
            assert( dk.is.even(self.n_nodes), 'Number of nodes should be even.' );
        end
        
        function fig = show(self,freq,fs)
        %
        % fig = show( freq=1:60, fs=35 )
        %
        % Show results of network simulation:
        %   Excitatory envelope time-courses
        %   Envelope correlations
        %   Spectrogram
        %
        % See also: ant.ui.spectrogram, ant.dsp.ansig
        %
        
            assert( self.is_network(), 'Call show_unit() for single-unit simulations.' );
            
            if nargin < 3, fs=35; end
            if nargin < 2, freq=1:60; end
            
            tse = self.excitatory();
            env = ant.dsp.ansig(tse);
            aec = corrcoef(env.vals) - eye(env.ns);
            
            fig = dk.fig.new( '[lsbm.model.wc] Wilson-Cowan Network' );
            
            subplot(2,3,[1,2,3]);
            env.plot_image('ctype','pos'); title('Envelope time-courses');
            
            subplot(2,3,4);
            dk.ui.image(aec); title('Envelope correlations');
            
            subplot(2,3,[5,6]);
            ant.ui.spectrogram(tse,freq,fs);
            
            fig.UserData.E = tse;
            fig.UserData.aec = aec;
            fig.UserData.env = env;
            
        end
        
        function fig = show_unit(self,k,varargin)
        %
        % fig = show_unit( k, varargin )
        %
        % Show results of unit simulation:
        %   E/I time-courses
        %   Path in phase-space (orbit)
        %   Periodogram
        %
        % Additional inputs are forwarded to ant.ui.periodogram.
        %
        % See also: ant.ui.periodogram
        %
        
            if nargin < 2 && self.n_units==1, k=1; end
            
            tse = self.excitatory(k);
            tsi = self.inhibitory(k);
            
            fig = dk.fig.new( '[lsbm.model.wc] Wilson-Cowan Unit' );
            
            subplot(2,3,[1,2,3]);
            tse.plot_lines( 'b-', 'LineWidth', 1.5 ); hold on;
            tsi.plot_lines( 'r-', 'LineWidth', 1.0 ); hold off;
            legend('E','I'); xlabel('Time (sec)'); title('Time-courses');
            
            subplot(2,3,4);
            plot( tse.vals, tsi.vals );
            xlabel('E'); ylabel('I'); axis equal; axis([0,1,0,1]);
            title('Orbit');
            
            subplot(2,3,[5,6]);
            ant.ui.periodogram(tse,varargin{:});
            
            fig.UserData.E = tse;
            fig.UserData.I = tsi;
            
        end
        
    end
    
end
