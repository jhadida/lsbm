classdef Network < lsbm.model.System
    
    properties (Transient,Dependent)
        n_nodes
        n_units
    end 
    
    methods
        
        function self = Network()
        end
        
        function n = get.n_nodes(self), n = numel(self.param.nodes); end
        function n = get.n_units(self), n = self.n_nodes/2; end
        
        function info(self)
        end
        
        function res = run(self,tspan,tstep,init)
        end
        
        function self = input(self,varargin)
        end
        
    end
    
    methods
        
        function check(self)
            
            n = self.n_nodes;
            k = [ self.param.edges.src, self.param.edges.dst ];
            
            assert( dk.is.struct( self.param.nodes, {'am','tau','S','P'}, n ), 'Bad nodes.' );
            assert( all(dk.num.between( k, 1, n )), 'Bad edge indices.' );
            assert( all(self.param.edges.delay >= 0), 'Delays should be non-negative.' );
            
        end
        
        function setopt(self,varargin)
        %
        % Available options:   Default:
        %   enable_isp          false
        %   cache_delays        true
        %   num_threads         4
        %
            self.param.options = dk.c2s(varargin);
        end
        
        function self = build(self,unit,con,del,alpha)
        %
        % unit is a 1xN struct-array with fields
        %   cee, cei, cie, cii
        %   E/I
        %       am, tau, S, P
        %   
        % con/del are NxN matrices
        % alpha controls long-range E-to-E ratio (default 1)
        %
            if nargin < 5, alpha=1; end
            
            n = size(con,1);
            assert( dk.is.matrix(con,n) && dk.is.matrix(del,n), ...
                'Inputs con/del should be square matrices of the same size.' );
            assert( dk.num.between(alpha,0,1), 'alpha should be in [0,1].' );
            
            % setup nodes
            if isscalar(unit), unit = repmat(unit,1,n); end
            assert( dk.is.struct(unit,{'E','I','cee','cei','cie','cii'},n), 'Bad unit.' );
            
            nodes(2:2:2*n) = [unit.I];
            nodes(1:2:2*n) = [unit.E];
            
            % setup edges
            local.del = [];
            local.con = zeros(2,2,n);
            local.con(1,1,:) = reshape( [unit.cee], [1,1,n] );
            local.con(1,2,:) = reshape( [unit.cie], [1,1,n] );
            local.con(2,2,:) = reshape( [unit.cii], [1,1,n] );
            local.con(2,1,:) = reshape( [unit.cei], [1,1,n] );

            nonloc.con.mat  = con;
            nonloc.con.mask = [ alpha, 1-alpha; 0,0 ];
            nonloc.del = in.del;

            [con,del] = lsbm.cfg.network( n, 2, 1, local, nonloc );
            edges = lsbm.cfg.net2edge( con, del, dp );
            
            % save parameters
            self.param.nodes = nodes;
            self.param.edges = edges;
            
        end
        
    end
    
end