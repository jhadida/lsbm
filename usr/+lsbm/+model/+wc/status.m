function state = status(ts,strict)
%
% state = lsbm.model.wc.status(ts,strict=false)
%
% Characterise state of WC system given excitatory time-course(s).
% Use strict flag to bias towards oscillating state.
% State is one of (mutually exclusive):
%   
%   saturated
%   silent
%   oscillating
%
% JH

    assert( isa(ts,'ant.TimeSeries'), 'Bad time-series.' );

    if nargin < 2, strict=false; end

    if strict
        TAUM = 0.95; % average should be less with oscillations
        TAUS = 1e-3; % std should be more with oscillations
    else
        TAUM = 0.95;
        TAUS = 5e-3;
    end

    % check for saturated/silent states
    if max(ts.mean) > TAUM
        state = 'saturated';
    elseif mean(ts.std) < TAUS
        state = 'silent';
    else
        state = 'oscillating';
    end

end