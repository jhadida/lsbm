classdef Result < nst.model.AbstractResult
    
    properties
        raw, sim;
    end
    
    properties (Transient,Dependent)
        n_nodes, n_units;
    end
    
    methods
        
        function self = Result(varargin)
            self.clear();
            if nargin > 0, self.assign(varargin{:}); end
        end
        
        function self = assign(self,raw,sim)
            assert( mod(raw.ns,6) == 0, 'Number of signals should be a multiple of 6.' );
            if nargin < 3, sim=[]; end
            self.raw = raw; 
            self.sim = sim;
        end
        
        function other = clone(self)
            other = nst.model.cb.Result(self.raw.clone(),self.sim);
        end
        
        function n = get.n_nodes (self), n = self.n_states/3; end
        function n = get.n_units (self), n = self.n_nodes/2; end
        
        function m = model(self)
            if isempty(self.sim)
                error( 'Simulation data is not set.' );
            elseif isfield(self.sim.system,'model') % unit
                m = self.sim.system.model;
            else % network
                m = self.sim.system.options.model;
            end
        end
        
        function ts = eV(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-5 );
        end
        function ts = eGe(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-4 );
        end
        function ts = eGi(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-3 );
        end
        function ts = iV(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-2 );
        end
        function ts = iGe(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-1 );
        end
        function ts = iGi(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.raw.select( 6*k-0 );
        end
        
        function ts = excitatory(self,k)
            if nargin < 2, k = 1:self.n_units; end
            k  = bsxfun( @plus, [-5;-4;-3], dk.torow(6*k) );
            ts = self.raw.select(k(:));
        end
        
        function ts = inhibitory(self,k)
            if nargin < 2, k = 1:self.n_units; end
            k  = bsxfun( @plus, [-2;-1;0], dk.torow(6*k) );
            ts = self.raw.select(k(:));
        end
        
        function ts = privCurrentE(self,k)
            k = k (:);
            m = self.model();
            t = self.raw.time;
            V = self.raw.vals(:,k-2);
            g = self.raw.vals(:,k-1); % excitatory conductance
            
            ts = ant.TimeSeries( t, g.*(m.R_dep - V) );
        end
        function ts = privCurrentI(self,k)
            k = k (:);
            m = self.model();
            t = self.raw.time;
            V = self.raw.vals(:,k-2);
            g = self.raw.vals(:,k); % inhibitory conductance
            
            ts = ant.TimeSeries( t, g.*(m.R_dep - V) );
        end
        
        function ts = eCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentE(bsxfun( @plus, [-3;0], dk.torow(6*k) ));
        end
        function ts = eeCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentE(6*k-3);
        end
        function ts = eiCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentE(6*k);
        end
        function ts = iCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentI(bsxfun( @plus, [-3;0], dk.torow(6*k) ));
        end
        function ts = ieCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentI(6*k-3);
        end
        function ts = iiCurrent(self,k)
            if nargin < 2, k = 1:self.n_units; end
            ts = self.privCurrentI(6*k);
        end
        
        function ts = node(self,k), ts = self.raw.select( 3*k + (-2:0) ); end
        function ts = unit(self,k), ts = self.raw.select( 6*k + (-5:0) ); end
        
    end
    
    methods
        
        function fig = show_unit(self,k)
            if nargin < 2, k=1; end
            fig = nst.ui.sim.cb_unit( self.unit(k), 'Conductance-based unit' );
        end
        
        function pa = analyse(self,varargin)
            pa = nst.make.pipeline(varargin{:});
            pa.analyse( self.eGe() );
        end
        
    end
    
end
