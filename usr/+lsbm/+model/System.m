classdef System < handle
    
    properties
        param
    end
    
    methods (Abstract)
        
        % check validity of parameters
        check(self);
        
        % run simulation and return instance of model Result
        res = run(self,tspan,tstep,init);
        
        % set input before running simulation
        self = input(self,varargin);
        
        % print structured info to console
        info(self);
        
    end
    
    methods
        
        function self = assign(self,param)
            assert( dk.is.struct(param), 'Input should be a struct.' );
            self.param = param;
            self.check();
        end
        
    end
    
end