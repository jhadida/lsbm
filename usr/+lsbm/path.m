function p = path(varargin)
    h = fileparts(mfilename('fullpath'));
    p = fullfile(h,varargin{:});
end