classdef Result < handle
    
    properties (Abstract)
        raw; % time-series output by simulation
        sim; % Simulation object containing the configuration
    end
    
    properties (Transient,Dependent)
        n_states, n_times;
    end
    
    properties (Abstract,Transient,Dependent)
        n_nodes, n_units;
    end
    
    methods
        
        % dependent properties
        function n = get.n_states(self), n = self.raw.n_signals; end
        function n = get.n_times (self), n = self.raw.n_times; end
        
        % reset to empty state (can be overloaded of course)
        function clear(self)
            self.raw = ant.TimeSeries();
        end
        
        % check the state of the bound time-series
        function y = is_empty(self)
            y = ~isa(self.raw, 'ant.TimeSeries') || self.raw.is_empty(); 
        end
        
        % resample raw timecourses (see ant.TimeSeries)
        % not ideal to use .resample (instead of downsample), but no choice
        % for adaptive time-steps.
        %
        % resample(fs,method=pchip)
        function self = resample(self,varargin)
            self.raw.resample(varargin{:});
        end
        
    end
    
    methods (Abstract)
        
        % assign a new result
        %   first input is mexout.solution (ant.TimeSeries)
        %   second input is mexcfg (lsbm.Config)
        self = assign(self,ts,sim);
        
        % time-series of unit k (may have more than 1 signal)
        ts = unit(self,k);
        
        % time-series of node k (may have more than 1 signal)
        ts = node(self,k);
        
        % deep-copy of current instance
        res = clone(self);
        
        % analyse network results
        pa = analyse(self,varargin);
        
        % show unit timecourse
        fig = show_unit(self,k,varargin);
        
    end
    
end
