classdef Model < handle
    
    properties (Abstract,SetAccess=protected)
        id; % model identifier for serialisation
        result_class; % class handle to wrap Mex outputs as Matlab objects
        build_func; % function handle to build the configuration object
    end
    
    properties
        param; % structure of parameters for the given model
        other; % other options for NSL_Helper
    end
    
    properties (Transient)
        mexcfg; % configuration to be used calling the Mex wrappers 
        mexout; % structure containing Mex outputs
    end
    
    properties (Abstract,Transient,Dependent)
        n_states, n_nodes, n_units;
    end
    
    methods (Abstract)
        
        % run simulation with current parameters, optionally set 
        % sampling properties on the fly (see build_config), and
        % return instance of result_class.
        self = run(self,varargin);
        
    end
    
    methods
        
        % reset members
        function clear(self)
            self.param  = struct();
            self.other  = struct();
            self.mexcfg = nst.model.Simulation();
            self.mexout = struct();
        end
        
        % return simulation results
        %
        % get_solution()
        % get_solution( fs )
        %
        function sol = get_solution(self,varargin)
            sol = ant.TimeSeries( self.mexout.solution );
            sol = self.result_class( sol, self.mexcfg );
            if nargin > 1
                sol.resample(varargin{:});
            end
        end
        
        % build configuration object
        %
        % build_config()
        % build_config( tspan ) => sampling.span = tspan
        % build_config( varargin ) => set_sampling(varargin{:})
        %
        function cfg = build_config(self,varargin)
            
            % optionally set sampling properties on-the-fly
            if nargin == 2 
                self.param.sampling.span = varargin{1};
            elseif nargin > 2
                self.set_sampling( varargin{:} );
            end
            
            cfg = self.build_func( self.param );
            cfg.set_other( self.other );
            self.mexcfg = cfg;
        end
        
        % parameters
        function y = is_param(self,name)
            y = isfield(self.param,name);
        end
        function val = get_param(self,name,val)
            if self.is_param(name)
                val = self.param.(name);
            end
        end
        function self=set_param(self,name,val)
            self.param.(name) = val;
        end
        function self=rem_param(self,name)
            if self.is_param(name)
                self.param = rmfield( self.param, name );
            end
        end
        function self=merge_param(self,name,val)
            current = self.get_param( name, struct() );
            self.param.(name) = dk.struct.merge( current, val );
        end
        
        % set sampling properties
        function self=set_sampling(self,varargin)
            nst.make.util.set_sampling( self, varargin{:} );
        end
        
        % mex options are merged unless specified otherwise
        function self=set_options(self,opt,overwrite)
            
            if nargin < 3, overwrite=false; end
            if overwrite
                self.param.options = opt;
            else
                self.merge_param( 'options', opt );
            end
        end
        
    end
    
    methods
        
        % serialise/unserialise
        function data = serialise(self,filename)
            data.id    = self.id;
            data.ver   = '0.1';
            data.param = self.param;
            data.other = self.other;
            
            if nargin > 1
                dk.print( 'Saving model parameters to file: %s', filename );
                dk.save( filename, data );
            end
        end
        
        function self = unserialise(self,data)
            if ischar(data)
                dk.print( 'Loading model parameters from file: %s', data );
                data = load(data);
            end
            assert( strcmp(data.id,self.id), 'ID mismatch between this class and loaded config.' );
            switch data.ver
                case '0.1'
                    self.clear();
                    self.param = data.param;
                    self.other = data.other;
            end
        end
        
        % save the last simulation (configuration and output)
        function data=save_simulation(self,filename)
            
            cfg = self.build_config();
            data = struct( 'cfg', cfg.build(), 'out', self.mexout );
            dk.save( filename, data );
        end
        
    end
    
end
