function nonloc = network( conn, dist, param )
%
% nonloc = lsbm.cfg.network( param )
%
% Prepare nonlocal input to lsbm.cfg.netmats from anatomical netmats.
%
%   .con                Network connectivity, struct-array:
%       .weight             multiplication factor
%       .mask               Kronecker mask
%
%   .del                Network delay, struct-array:
%       .mask               Kronecker mask
%       .velocity           Propgation velocity (m/s)
%       .average            Average delay assuming uniform propagation velocity
%       .median             Median delay with same assumptions
%
% JH

    assert( isfield(param,'con') && dk.is.struct(param.con,{'weight','mask'},0), ...
        'Missing field "con", or bad field value.' );
    
    assert( isfield(param,'del') && dk.is.struct(param.del,{'mask'},0), ...
        'Missing field "del", or bad field value.' );

    hasfield = @(x,n) isfield(x,n) && ~isempty(x.n);
    
    % weight
    n = numel(param.con);
    for i = 1:n
        nonloc.con(i).mat  = conn;
        nonloc.con(i).mask = conn(i).weight * conn(i).mask;
    end
    
    % delay
    n = numel(param.del);
    dmean = mean(dist(:));
    dmedian = median(dist(:));
    
    for i = 1:n
        if hasfield(param.del(i),'average')
            v = dmean / param.del(i).average;
        elseif hasfield(param.del(i),'median')
            v = dmedian / param.del(i).median;
        else
            v = param.del(i).velocity;
        end
        assert( ~any(isnan(v)), 'NaN velocity.' );
        
        nonloc.del(i).mat  = dist;
        nonloc.del(i).mask = param.del(i).mask;
    end

end
