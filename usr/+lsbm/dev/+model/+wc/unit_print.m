function unit_print( u )
%
% Print unit config to console.
%
% JH
    
    print_subpop( 'E', u.E );
    print_subpop( 'I', u.I );
    
    dk.print( 'cee: %.2f', u.cee );
    dk.print( 'cei: %.2f', u.cei );
    dk.print( 'cie: %.2f', u.cie );
    dk.print( 'cii: %.2f', u.cii );
    
    fprintf('\n');
    dk.print( 'Ci/Ce: %.2f', -(u.cie + u.cii)/(u.cee + u.cei) );
    dk.print( 'Me/Ce: %.2f', u.E.S.mu / (u.cee + u.cei) );
    
end

function print_subpop( name, param )

    dk.print( '%s subpop', name );
    dk.print( '   tau: %.1f ms', 1000*param.tau );
    dk.print( '    am: %.2f', param.am );
    dk.print( '    mu: %.2f', param.S.mu );
    dk.print( ' sigma: %.2f', param.S.sigma );
    dk.print( '  norm: %s', dk.tostr(dk.struct.get( param, 'norm', false )) );
    fprintf('\n'); % skip line
    
end
