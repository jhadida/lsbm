function res = run(type,cfg)
%
% Simple wrapper to call Mex functions and return results in lsbm.model.wc.Result.
%

    switch lower(type)
        case {'u','unit'}
            sim = @lsbm.mex.wc_unit;
        case {'net','network'}
            sim = @lsbm.mex.wc_network;
        case {'snet','network_sto'}
            sim = lsbm.mex.wc_network_sto;
        otherwise
            error('Unknown type: %s',type);
    end
    res = lsbm.model.wc.Result(sim(cfg));

end