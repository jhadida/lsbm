function res = unit( u, input, tspan, tstep )
%
% lsbm.model.wc.unit( sys, input, tspan=, tstep=1e-3 )
%
% Run simulation of Wilson-Cowan unit.
%
% JH
    
    if isscalar(input), input=[input,0]; end
    if nargin < 3
        tspan = 100 * u.E.tau; 
    end
    
    % set constant inputs
    u.E.P = struct('name','const','value',input(1));
    u.I.P = struct('name','const','value',input(2));
    
    % initial state
    init = [0,0];
    
    % sampling parameters
    samp.span = tspan;
    samp.step = tstep;
    
    % build configuration
    cfg = lsbm.Config('quiet');
    cfg.set_system(u);
    
    % run simulation
    res = lsbm.run( cfg, @lsbm.mex.wc_unit, samp, init );
    
end