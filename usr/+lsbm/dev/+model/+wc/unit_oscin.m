function Pe = unit_oscin( u, Pi, ndigit )
%
% Find oscillatory input for given unit.
%
% JH

    if nargin < 2, Pi=0; end
    if nargin < 3, ndigit=3; end

    muE = u.E.S.mu;
    tau = u.E.tau;
    Pe = lsbm.util.dichotomy( @is_silent, abs(muE), ndigit );

    function s = is_silent(Pe)
        sim = lsbm.model.wc.unit( u, [Pe,Pi], 120*tau, 1e-3 );
        tse = sim.solution.select(1).burn( 50*tau );
        s = strcmp( lsbm.model.wc.state(tse,true), 'silent' );
    end

end