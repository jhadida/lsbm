function fig = unit( ts, title_str )
%
% fig = lsbm.ui.unit( ts, title_str )
%
% Plot timecourse and periodogram for single-channel input.
%
% JH

    if nargin < 2, title_str = 'Unit time-course'; end
    dk.assert( ts.ns == 1, 'Input timecourse should have only one channel.' );
    fig = figure( 'name', '[lsbm.ui] Unit UI' );

    % show raw time-courses
    subplot(2,1,1);
    ts.plot_lines( 'b-', 'LineWidth', 1.5 );
    xlabel('Time (sec)'); title(title_str);
    set(colorbar(gca),'Visible','off');

    % show spectrogram
    subplot(2,1,2);
    if ts.tspan > 5
        ant.ui.spectrogram( ts, 1:70, 50 );
    else
        ant.ui.periodogram( ts );
    end
    
    % stash the timecourses for easy retrieval
    fig.UserData.ts = ts;

end
