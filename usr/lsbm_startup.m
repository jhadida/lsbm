function lsbm_startup()

    here = fileparts(mfilename('fullpath'));
    assert( ~isempty(getenv('DECK_ROOT')), 'Required dependency "Deck" is not on the path.' );
    
    dk.print('[LSBM] Starting up from folder "%s".',here);
    dk.env.path_flag( 'LSBM_ROOT', here );
    cellfun( @checklib, { 'drayn', 'disol', 'lsbm' } );
    
end

function checklib(name)
    folder = fullfile( getenv('LSBM_ROOT'), '+lsbm/+mex/lib', name );
    dk.assert('w', dk.fs.isdir(folder), 'Library "%s" folder not found: %s', name, folder );
end
