#!/bin/bash

# source utilities
HERE=$(dirname "${BASH_SOURCE}")
source "$HERE/bashlib.sh" || { echo "Could not source utilities."; exit 1; }
qpushd "$HERE"

# update both repos
LIB_FOLDER=usr/+lsbm/+mex/lib
update_repo() {
    local name=$1
    chkdir "$LIB_FOLDER/$name"
    msg_bG "Repo ${name}: update..."
    qpushd "$LIB_FOLDER/$name"
    git pull
    qpopd
}
update_repo drayn
update_repo disol

# go back to calling folder
qpopd
