#!/usr/bin/env bash

# source utilities
HERE=$(dirname "${BASH_SOURCE}")
source "$HERE/bashlib.sh" || { echo "Could not source utilities."; exit 1; }
qpushd "$HERE"

# check that Mex folder is in order
MEX_FOLDER=usr/+lsbm/+mex
for f in bin func inc lib model test; do
    makedir "$MEX_FOLDER/$f"
done

# clone repositories
LIB_FOLDER=$MEX_FOLDER/lib
clone_repo() {
    local name=$1
    local url=$2

    if [ -d "$LIB_FOLDER/$name" ]; then
        msg_bG "Repo ${name}: ok"
    else
        msg_bB "Repo ${name}: cloning..."
        git clone "$url" "$LIB_FOLDER/$name" || echoerr "Could not clone '$name'"
        msg_bG "Repo ${name}: done!"
    fi
}

clone_repo drayn https://gitlab.com/jhadida/drayn.git
clone_repo disol https://gitlab.com/jhadida/disol.git

# link current folder in mexlib
if [ -d "$LIB_FOLDER/lsbm" ]; then
    msg_bG "Repo lsbm: ok"
else
    msg_bB "Repo lsbm: linking..."
    qpushd "$LIB_FOLDER"
    ln -s ../../../.. lsbm 
    qpopd
    msg_bG "Repo lsbm: done!"
fi

# go back to calling folder
qpopd
