
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

// alias for Drayn namespace
namespace dr = drayn;

// source integer types from Drayn
using namespace dr::integer;

// ------------------------------------------------------------------------

struct traits
{
    typedef double time;
    typedef double value;

    using system = disol::system<value,time>;
    using system_ito = disol::system_ito<value,time>;

    using pool = system::pool_type;
    using state = typename pool::state_type;

    using problem_ivp = disol::problem_ivp<value,time>;
    using problem_hist = disol::problem_hist<value,time>;
};

LSBM_NS_END
