
//==================================================
// @title        rescaling.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

bool rescaling::configure( double m, double s ) {
    mu = m; 
    sigma = s;
    return check();
}

#ifdef LSBM_USE_JMX

bool rescaling::configure( const jmx::Struct& in ) {
    mu = in.getnum("mu",0.0);
    sigma = in.getnum("sigma",1.0);
    return check();
}

#endif

LSBM_NS_END
