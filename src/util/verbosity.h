
//==================================================
// @title        verbosity.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

enum class verbose_level: int
{
    Quiet  =0,
    Info   =1,
    Debug  =2
};

struct verbose_mapping: public nmap<verbose_level>
{
    verbose_mapping()
    {
        this->define( "quiet",      verbose_level::Quiet  );
        this->define( "info",       verbose_level::Info   );
        this->define( "debug",      verbose_level::Debug  );
        this->define( "default",    verbose_level::Info   );
    }
};

inline verbose_level& verbosity() { 
    static verbose_level v = verbose_level::Info; // default
    return v; 
}

inline void verbosity( verbose_level lvl ) 
    { verbosity() = lvl; }

inline void verbosity( std::string lvl ) { 
    static const verbose_mapping vmap;
    verbosity(vmap.get_id(lvl));
}

// ------------------------------------------------------------------------

template <class... Args>
inline void print_debug(const char *fmt, Args&&... args) {
    if ( verbosity() < verbose_level::Debug ) return;
    std::cout << drayn_format( fmt, std::forward<Args>(args)... ) << std::endl;
}

template <class... Args>
inline void print_info(const char *fmt, Args&&... args) {
    if ( verbosity() < verbose_level::Info ) return;
    std::cout << drayn_format( fmt, std::forward<Args>(args)... ) << std::endl;
}

LSBM_NS_END

