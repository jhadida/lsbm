
//==================================================
// @title        rescaling.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct rescaling
{
    double mu, sigma;

    // ----------  =====  ----------

    rescaling() { clear(); }
    rescaling( double m, double s ) { configure(m,s); }

    inline void clear()
        { mu=0.0; sigma=1.0; }
    inline bool check() const
        { return sigma > dr::c_num<double>::eps; }

    inline double rescale( double x ) const
        { return (x - mu)/sigma; }
    inline double unscale( double y ) const
        { return mu + y*sigma; }
    inline double derived( double x ) const
        { return 1/sigma; }

    bool configure( double m, double s );

#ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& ms );
#endif

};

LSBM_NS_END
