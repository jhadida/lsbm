
//==================================================
// @title        comparable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

template <class T, class F>
struct comparable 
{
    // the only operation to implement
    virtual T operator- ( const F& that ) const =0;

    inline bool operator== ( const F& that ) const 
        { return operator-(that) == 0; }
    inline bool operator!= ( const F& that ) const 
        { return !operator==(that); }

    inline bool operator< ( const F& that ) const 
        { return operator-(that) < 0; }
    inline bool operator> ( const F& that ) const 
        { return operator-(that) > 0; }

    inline bool operator<= ( const F& that ) const 
        { return !operator>(that); }
    inline bool operator>= ( const F& that ) const 
        { return !operator<(that); }
};

LSBM_NS_END
