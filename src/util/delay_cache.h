
//==================================================
// @title        delay_cache.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

/**
 * This is used to store parametric representations of delayed terms in neuronal systems.
 * We assume that coupled terms are ADDITIVE. In that case, for a given node i and delays
 * {d_j} for j = 1..Nnodes, the delayed coupling term at query-time t_q takes the form:
 *
 * C_i(t_q) = \sum_j a_j X( t_q - d_j )
 *
 * This needs to be computed for each node i, each with a separate set of delays for their
 * neighbours, and therefore takes O(N^2). Furthermore it should be computed at each substep
 * for multi-step method. The motivation behind the following class is to reduce the overhead
 * caused by the computation of delayed terms.
 *
 * If we know that t_q is between t_n and t_n+h, and that we use linear interpolation to
 * compute delayed terms in-between timesteps, then we have:
 *
 * C_i(t_q) = (1-alpha) * C_i(t_n) + alpha * C_i(t_n+h)    with    alpha = (t_q-t_n)/h
 *
 * Therefore, by computing C_i(t_n) and C_i(t_n+h) before each step, we get delayed
 * terms at each node for any query time t_q between t_n and t_n+h in constant time.
 */
template <class T = double>
struct DelayCache
{
    typedef DelayCache<T> self;
    typedef T value_type;

    dr::shared<T> left;
    dr::shared<T> right;
    double t, dt;

    // ----------  =====  ----------

    DelayCache() { clear(); }

    void clear();
    void reset( uidx_t Nnodes, double t, double dt );

    // number of delayed terms
    inline uidx_t size() const { return left.size(); }

    // interpolate at query time
    inline double operator() ( uidx_t n, double tq ) const
    {
        DRAYN_DREJECT_ERR( (tq > t+dt) || (tq < t), "Bad interpolant in delay cache." )
        tq = dr::op_clamp( (tq-t)/dt, 0.0, 1.0 );
        return tq*right[n] + (1.0-tq)*left[n];
    }
};

// ------------------------------------------------------------------------

template <class T>
void DelayCache<T>::clear()
{
    left  .clear();
    right .clear();
    t = dt = 0.0;
}

// ------------------------------------------------------------------------

template <class T>
void DelayCache<T>::reset( uidx_t Nnodes, double t, double dt )
{
    left  .resize( Nnodes, T(0) );
    right .resize( Nnodes, T(0) );

    this->t  = t;
    this->dt = dt;
}

LSBM_NS_END
