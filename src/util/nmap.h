
//==================================================
// @title        nmap.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>
#include <unordered_map>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

// name to integer mapping
template <class ID = int>
struct nmap
{
    std::unordered_map<std::string,ID> mapping;

    inline void define( const std::string& key, ID val )
        { mapping[key] = val; }

    inline void undefine( const std::string& key )
        { mapping.erase(key); }

    inline bool has_key( const std::string& key ) const
        { return mapping.find(key) != mapping.end(); }

    // Can't use map::operator[] because it is not marked const...
    inline ID get_id( const std::string& key ) const { 
        DRAYN_ASSERT_ERR( has_key(key), "Key not found: '%s'", key.c_str() )
        return mapping.find(key)->second; 
    }
};

LSBM_NS_END
