#ifndef LSBM_ACTIVATION_H_INCLUDED
#define LSBM_ACTIVATION_H_INCLUDED

//==================================================
// @title        activation
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "abstract.h"

// ----------  =====  ----------

#include "kernel/step.h"
#include "kernel/linear.h"
#include "kernel/rectifier.h"
#include "kernel/softplus.h"
#include "kernel/smoothstep.h"
#include "kernel/rational.h"
#include "kernel/mixed.h"

// ----------  =====  ----------

#include "factory.h"

#endif
