
//==================================================
// @title        factory.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

activation_ptr activation_factory( const std::string& name )
{
    static const Activation_mapping mapping;
    switch( mapping.get_id(name) )
    {
        case activation_id::Step:
            return activation_ptr(new Activation<Activation_step>());

        case activation_id::Linear:
            return activation_ptr(new Activation<Activation_linear>());

        case activation_id::Rectifier:
            return activation_ptr(new Activation<Activation_rectifier>());
            
        case activation_id::Softplus:
            return activation_ptr(new Activation<Activation_softplus>());

        case activation_id::Smoothstep:
            return activation_ptr(new Activation<Activation_smoothstep<1>>());
        case activation_id::Smoothstep2:
            return activation_ptr(new Activation<Activation_smoothstep<2>>());
        case activation_id::Smoothstep3:
            return activation_ptr(new Activation<Activation_smoothstep<3>>());
        case activation_id::Smoothstep4:
            return activation_ptr(new Activation<Activation_smoothstep<4>>());

        case activation_id::Rational:
            return activation_ptr(new Activation<Activation_rational>());

        case activation_id::Mixed:
            return activation_ptr(new Activation<Activation_mixed>());

        default:
            DRAYN_WARN( "Unknown activation: %s", name.c_str() )
            return activation_ptr();
    }
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX

activation_ptr activation_factory( const jmx::Struct& cfg )
{
    activation_ptr a;

    DRAYN_ASSERT_ERR( cfg.has_field("name"), "Missing field 'name'." )
    a = activation_factory(cfg.getstr("name"));
    DRAYN_ASSERT_ERR( a && a->configure(cfg), "Could not build or configure activation." )

    return a;
}

#endif

LSBM_NS_END
