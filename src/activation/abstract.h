
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_kernel
{
    inline void clear() {}
    inline bool check() const { return true; }

    virtual double operator() ( double ) const =0;

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& ) { return true; }
    #endif
};

// ------------------------------------------------------------------------

struct Activation_abstract
{
    virtual bool check() const =0;
    virtual double threshold() const =0;
    
    virtual double operator() ( double ) const =0;
    inline double val(double x) const { return operator()(x); }

    #ifdef LSBM_USE_JMX
    virtual bool configure( const jmx::Struct& in ) =0;
    #endif
};

// ------------------------------------------------------------------------

template <class Kernel>
struct Activation: public Activation_abstract
{
    Kernel ker;
    rescaling rsc;
    double gain;
    
    // ----------  =====  ----------
    
    Activation() { clear(); }
    virtual ~Activation() {} // -Wdelete-non-virtual-dtor

    inline void clear() 
        { ker.clear(); rsc.clear(); gain=1.0; }

    inline bool check() const 
        { return ker.check() && rsc.check(); }

    inline double threshold() const 
        { return rsc.mu; }

    inline double operator() (double x) const 
        { return gain*ker(rsc.rescale(x)); }
    
    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& in ) { 
        gain = in.getnum("gain",1.0);
        return rsc.configure( in.getnum("thresh",0.0), in.getnum("width",1.0) ) 
            && ker.configure(in) && check(); 
    }
    #endif
};

LSBM_NS_END
