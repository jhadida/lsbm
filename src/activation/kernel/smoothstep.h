
//==================================================
// @title        smoothstep
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

template <unsigned ord>
struct Activation_smoothstep
{
    inline double operator() ( double x ) const
        { DRAYN_ERROR("Not implemented."); }
};

// ------------------------------------------------------------------------

template <>
struct Activation_smoothstep<1>: public Activation_kernel
{
    inline double operator() ( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 1.0;
        return -x*x*(2*x - 3);
    }
};

template <>
struct Activation_smoothstep<2>: public Activation_kernel
{
    inline double operator() ( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 1.0;
        return x*x*x*(x*(6*x - 15) + 10);
    }
};

template <>
struct Activation_smoothstep<3>: public Activation_kernel
{
    inline double operator() ( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 1.0;
        const double y = x*x;
        return -y*y*(x*(x*(20*x - 70) + 84) - 35);
    }
};

template <>
struct Activation_smoothstep<4>: public Activation_kernel
{
    inline double operator() ( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 1.0;
        const double y = x*x;
        return y*y*x*(x*(x*(x*(70*x - 315) + 540) - 420) + 126);
    }
};

LSBM_NS_END
