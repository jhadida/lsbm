
//==================================================
// @title        linear.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_linear: public Activation_kernel
{
    inline double operator() ( double x ) const { return dr::op_clamp(x, 0.0, 1.0); }
};

LSBM_NS_END
