
//==================================================
// @title        softplus.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_softplus: public Activation_kernel
{
    // copied from DLib (http://dlib.net/dlib/dnn/utilities.h.html)
    inline double operator() ( double x ) const {
        if (x <= -37)
            return exp(x);
        else if (-37 < x && x <= 18)
            return log1p(exp(x));
        else if (18 < x && x <= 33.3)
            return x + exp(-x);
        else
            return x;
    }
};

LSBM_NS_END
