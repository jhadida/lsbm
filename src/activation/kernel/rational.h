
//==================================================
// @title        rational.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_rational: public Activation_kernel
{
    double alpha;

    inline void clear() { alpha=1; }
    inline bool check() const { return alpha > 0; }

    inline double operator() ( double x ) const {
        if ( x < 0.0 ) return std::pow( 1-x, -alpha );
        else return std::pow( 1+x, alpha );
    }

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& in ) {
        alpha = in.getnum("alpha",1.0);
        return check();
    }
    #endif
};

LSBM_NS_END
