
//==================================================
// @title        mixed.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_mixed: public Activation_kernel
{
    // not numerically stable...
    inline double operator() ( double x ) const {
        return x==0.0 ? 1.0 : x/(1 - exp(-x));
    }
};

LSBM_NS_END
