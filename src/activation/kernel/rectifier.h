
//==================================================
// @title        rectifier.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Activation_rectifier: public Activation_kernel
{
    inline double operator() ( double x ) const { return x > 0 ? x : 0; }
};

LSBM_NS_END
