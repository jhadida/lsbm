
//==================================================
// @title        factory.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>
#include <memory>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

enum class activation_id
{
    Step,
    Linear,
    Rectifier,
    Softplus,
    Smoothstep,
    Smoothstep2,
    Smoothstep3,
    Smoothstep4,
    Rational,
    Mixed
};

// ------------------------------------------------------------------------

struct Activation_mapping: public nmap<activation_id>
{
    Activation_mapping()
    {
        this->define( "step",           activation_id::Step        );
        this->define( "heaviside",      activation_id::Step        );
        this->define( "rectifier",      activation_id::Rectifier   );
        this->define( "relu",           activation_id::Rectifier   );
        this->define( "linear",         activation_id::Linear      );
        this->define( "softplus",       activation_id::Softplus    );
        this->define( "smoothstep",     activation_id::Smoothstep  );
        this->define( "smoothstep2",    activation_id::Smoothstep2 );
        this->define( "smoothstep3",    activation_id::Smoothstep3 );
        this->define( "smoothstep4",    activation_id::Smoothstep4 );
        this->define( "rational",       activation_id::Rational    );
        this->define( "mixed",          activation_id::Mixed       );
    }
};

// ------------------------------------------------------------------------

// self-managed activation pointer
typedef std::shared_ptr<Activation_abstract> activation_ptr;

template <class K>
activation_ptr make_activation( double onset, double width, double gain=1.0 ) {
    auto x = new Activation<K>();
    x->rsc.mu = onset;
    x->rsc.sigma = width;
    x->gain = gain;
    return activation_ptr(x);
}

activation_ptr activation_factory( const std::string& name );

#ifdef LSBM_USE_JMX
activation_ptr activation_factory( const jmx::Struct& cfg );
#endif

LSBM_NS_END
