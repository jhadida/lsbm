#ifndef LSBM_SYSTEM_H_INCLUDED
#define LSBM_SYSTEM_H_INCLUDED

//==================================================
// @title        system
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "inducer.h"
#include "default.h"
#include "network.h"
#include "abstract.h"

// ----------  =====  ----------


// Wilson-Cowan model
#include "library/wc/include.h"

// Conductance-based model
#include "library/cb/include.h"
#include "library/icb/include.h"

// Hopf model
// #include "library/hopf/include.h"

// Kuramoto model
// #include "library/kuramoto/include.h"

// Robinson models
// #include "library/robinson/include.h"

// Canonical micro-circuit model
// #include "library/cmc/include.h"

#endif
