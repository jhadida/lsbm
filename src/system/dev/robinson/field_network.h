
//==================================================
// @title        field_network.h
// @author       RA & JH
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_Robinson_Field_network
    : public NeuronalSystem
{
public:

    typedef System_Robinson_Field_network  self;
    typedef Robinson_Field_network         param_type;
    typedef NeuronalSystem                 parent;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using deriv_type  = parent::deriv_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_Robinson_Field_network()
        { clear(); }

    inline void clear() { param.clear(); parent::clear(); }
    inline bool reset() { return true; }

    // dimensions
    inline uidx_t  n_nodes    () const { return param.n_nodes(); }
    inline uidx_t  ndims () const { return 4*n_nodes(); }

    // used by lsbm_helper
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_Robinson_Field_network( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && param.check(); }

#endif


    // Derivative (no cache implementation)
    void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const;


    // Handling the case gamma == 0
    // bind to event: after_commit
    template <class IData>
    void callback_after_commit( IData& dat );

    // ----------  =====  ----------

    param_type param; // network parameters

private:

    // form of delayed terms for the Robinson Field network model
    inline double delayed_term( time_type t, const edge_type& edge ) const
    {
        // State variables are [phi = p , dphi/dt = q, v = v, dvdt = w]
        // The delayed term is taken from phi, which is at index 4k
        if ( param.option.induction )
        {
            return ( t < this->inducer.t_start ) ? 0.0 :
                ( this->inducer(t) * edge.coupling * this->delayed_value( t-edge.delay, 4*edge.src, param.option.fixed_step ) );
        }
        else
            return edge.coupling * this->delayed_value( t-edge.delay, 4*edge.src, param.option.fixed_step );
    }

};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class IData>
void System_Robinson_Field_network::callback_after_commit( IData& dat )
{
    for ( uidx_t n = 0; n < n_nodes(); ++n )
    {
        auto& node = param.node(n);

        // If gamma==0, then phi = S(V) and this is computed here
        if( node.gamma == 0 )
            dat.cur.x[4*n] = node.response(dat.cur.x[4*n+2]);
    }
}
