
//==================================================
// @title        param.h
// @author       RA & JH
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct Robinson_options
    : public default_options
{
    bool induction;

    // ----------  =====  ----------

    Robinson_options() { clear(); }

    inline virtual void clear()
        { default_options::clear(); induction = true; }

    void display() const;

    #ifdef LSBM_USE_JMX
    virtual bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct Robinson_node
{
    using sigmoid_type = Sigmoid<sigmoid_logistic>;
    using input_type   = Stimulus_timecourse;

    sigmoid_type S;
    input_type P;

    double qmax; // amplitude of population response
    double alpha, beta; // time-constants for dendrites/synapses

    // ----------  =====  ----------

    Robinson_node() { clear(); }

    void clear();
    bool check() const;

    inline double response ( double x ) const { return qmax * S.val(x); }
    inline double stimulus ( double t ) const { return P.val(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct Robinson_network:
    public TemplateNetwork<Robinson_node,default_edge,Robinson_options>
{};
