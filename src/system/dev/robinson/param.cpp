
//==================================================
// @title        param.cpp
// @author       RA & JH
//==================================================

void Robinson_options::display() const
{
    static const char* b2s[] = { "false", "true" };
    drayn_print( "[lsbm::Robinson_options] Selected options are:" );
    drayn_print( " - fixed step      : %s", b2s[this->fixed_step] );
    drayn_print( " - cache delays    : %s", b2s[this->cache_delays] );
    drayn_print( " - induction       : %s", b2s[induction] );
    drayn_print( " - num threads     : " uidx_f "\n", this->num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool Robinson_options::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "[Robinson_options] Invalid input." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["induction"],induction,true),
        "[Robinson_options] Couldn't extract 'induction'." );

    return default_options::extract(ms);
}
#endif

// ------------------------------------------------------------------------

void Robinson_node::clear()
{
    alpha = 1.0;
    beta  = 1.0;
    qmax  = 1.0;
    S.clear();
    P.clear();
}

// ------------------------------------------------------------------------

bool Robinson_node::check() const
{
    static const double eps = dr::c_num<double>::eps;
    return S.check() && P.check() && (alpha > eps) && (beta > eps) && (qmax >= 0.0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool Robinson_node::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({ // required fields
        "alpha", "beta", "qmax", "S", "P"
    }), "Missing field(s)." );

    DRAYN_ASSERT_RF( S.configure(ms["S"]), "Couldn't configure sigmoid." );
    DRAYN_ASSERT_RF( P.configure(ms["P"]), "Couldn't configure input." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["alpha"],alpha), "Couldn't extract 'alpha'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["beta"],beta), "Couldn't extract 'beta'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["qmax"],qmax), "Couldn't extract 'qmax'." );

    return true;
}
#endif
