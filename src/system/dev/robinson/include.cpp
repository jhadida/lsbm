
//=====================================================
// @title        Robinson Model
// @author       Romesh Abeysuriya
// @contact      romesh.abeysuriya [at] psych.ox.ac.uk
// @author       Jonathan Hadida
// @contact      jhadida [at] fmrib.ox.ac.uk
//=====================================================

namespace lsbm
{
    #include "param.cpp"
    #include "network.cpp"

    #include "field_param.cpp"
    #include "field_network.cpp"
}
