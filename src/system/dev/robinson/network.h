
//==================================================
// @title        network.h
// @author       RA & JH
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_Robinson_network
    : public NeuronalSystem
{
public:

    typedef System_Robinson_network  self;
    typedef Robinson_network         param_type;
    typedef NeuronalSystem           parent;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using deriv_type  = parent::deriv_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_Robinson_network()
        { clear(); }

    void clear();
    bool reset();

    // dimensions
    inline uidx_t  n_nodes    () const { return param.n_nodes(); }
    inline uidx_t  ndims () const { return 2*n_nodes(); }

    // used by lsbm_helper
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_Robinson_network( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && reset(); }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const deriv_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const deriv_type& dxdt ) const;


    // Initialise additional outputs
    // bind to event: after_init
    template <class IData>
    void callback_after_init( IData& dat );

    // Noise sampling and optional delayed terms caching for faster integration.
    // bind to event: before_step
    template <class IData>
    void callback_before_step( IData& dat );


    // ----------  =====  ----------

    param_type  param; // network parameters

private:

    DelayCache<double>       c_delay;
    dr::vector<std::thread>  t_threads;
    dr::vector<uidx_t>      t_delayed_partition;

    // form of delayed terms for the Robinson network model
    inline double delayed_term( time_type t, const edge_type& edge ) const
    {
        // For node k, Vk is at 2k and Uk at 2k+1, here we need the potential Vk
        if ( param.option.induction )
        {
            return ( t < this->inducer.t_start ) ? 0.0 :
            (
                this->inducer(t) * edge.coupling *
                param.node(edge.src).response(
                    this->delayed_value( t-edge.delay, 2*edge.src, param.option.fixed_step )
                )
            );
        }
        else
            return edge.coupling * param.node(edge.src).response(
                this->delayed_value( t-edge.delay, 2*edge.src, param.option.fixed_step )
            );
    }


    // ----------  =====  ----------

    class Cache_Worker
    {
    public:

        typedef System_Robinson_network  parent_type;
        typedef parent_type::time_type   time_type;

        Cache_Worker()
            : m_parent(nullptr) {}

        Cache_Worker( parent_type *ptr )
            : m_parent(ptr) {}

        // replace target time with current time and timestep
        void operator() ( uidx_t start_index, uidx_t stop_index )
        {
            auto& cache        = m_parent->c_delay;
            const time_type t  = cache.t;
            const time_type dt = cache.dt;

            for ( uidx_t i = start_index; i < stop_index; ++i )
            {
                const auto& edge = m_parent->param.delayed(i);

                cache.left [edge.dst] += m_parent->delayed_term( t, edge );
                cache.right[edge.dst] += m_parent->delayed_term( t+dt, edge );
            }
        }

    private:
        parent_type *m_parent;
    };

};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class IData>
void System_Robinson_network::callback_before_step( IData& dat )
{
    // current time and time-step
    const time_type t  = dat.cur.t;
    const time_type dt = dat.cur.dt;

    // delay caching
    if ( param.option.cache_delays )
    {
        c_delay.reset( n_nodes(), t, dt );

        // distribute across threads or start single-thread worker
        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread( Cache_Worker(this), t_delayed_partition[i], t_delayed_partition[i+1] );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else
        {
            Cache_Worker worker(this);
            worker( 0, param.n_delayed() );
        }
    }
}
