
//=====================================================
// @title        Robinson Model
// @author       Romesh Abeysuriya
// @contact      romesh.abeysuriya [at] psych.ox.ac.uk
// @author       Jonathan Hadida
// @contact      jhadida [at] fmrib.ox.ac.uk
//=====================================================

#include <vector>
#include <thread>

// ----------  =====  ----------

namespace lsbm
{
    #include "param.h"
    #include "network.h"

    #include "field_param.h"
    #include "field_network.h"
}
