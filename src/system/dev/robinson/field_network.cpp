
//==================================================
// @title        field_network.cpp
// @author       RA & JH
//==================================================

void System_Robinson_Field_network::derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[robinson_field_network.derivative] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    for ( uidx_t e: param.f_delayed )
    {
        auto& edge = param.edge(e);
        dxdt[ 4*edge.dst + 3 ] += delayed_term( t, edge );
    }

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);

        // Accumulate nu*phi in dw/dt from dv/dt = sum(nu*phi)
        dxdt[ 4*edge.dst + 3 ] += edge.coupling * x[4*edge.src];
    }

    // finalise
    uidx_t p,q,v,w;
    double  ab1,ab2,g2;
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);
        p = 4*n+0;
        q = 4*n+1;
        v = 4*n+2;
        w = 4*n+3;

        ab1 = node.alpha * node.beta;
        ab2 = (node.alpha + node.beta)/ab1;

        // If node.gamma == 0, then x[p] = sigm(x[v]) which is done post-step
        if( node.gamma > 0 )
        {
            g2 = node.gamma * node.gamma;
            dxdt[p] = x[q];
            dxdt[q] = g2*node.response(x[v]) - 2*node.gamma*x[q] - g2*x[p];
        }
        dxdt[v] = x[w];
        dxdt[w] += node.stimulus(t) - x[v] - ab2*x[w];
        dxdt[w] *= ab1;
    }
}
