
//==================================================
// @title        network.cpp
// @author       RA & JH
//==================================================

void System_Robinson_network::clear()
{
    param   .clear();
    c_delay .clear();

    t_threads           .clear();
    t_delayed_partition .clear();

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_Robinson_network::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[robinson_network.reset] Invalid configuration." );

    const uidx_t Nt = param.option.num_threads;
    if ( Nt > 1 )
    {
        // configure threads
        t_threads.create( Nt );
        t_delayed_partition = partition_delayed_edges_by_destination( param, Nt );
    }
    else
    {
        // sort by decreasing delay to optimise memory access pattern on records
        sort_delayed_edges_decreasing( param );
    }

    // clear additional memory
    c_delay.clear();

    return true;
}

// ------------------------------------------------------------------------

void System_Robinson_network::_derivative_full( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[robinson_network._derivative_full] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    for ( uidx_t e: param.f_delayed )
    {
        auto& edge = param.edge(e);
        dxdt[ 2*edge.dst + 1 ] += delayed_term( t, edge );
    }

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[ 2*edge.dst + 1 ] += edge.coupling * param.node(edge.src).response( x[2*edge.src] );
    }

    // finalise
    uidx_t v, u;
    double  p, s;
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);
        v = 2*n+0;
        u = 2*n+1;
        p = node.alpha * node.beta;
        s = (node.alpha + node.beta)/p;

        dxdt[v]  = x[u];
        dxdt[u] += node.stimulus(t) - x[v] - s*x[u];
        dxdt[u] *= p;
    }
}

// ------------------------------------------------------------------------

void System_Robinson_network::_derivative_cache( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[robinson_network._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nx )
    {
        DRAYN_INFO("[robinson_network._derivative_cache] "
            "Cache is empty, falling back to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,x,dxdt);
        return;
    }

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[ 2*edge.dst + 1 ] += edge.coupling * param.node(edge.src).response( x[2*edge.src] );
    }

    // iterate on each node
    uidx_t v, u;
    double  p, s;
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);
        v = 2*n+0;
        u = 2*n+1;
        p = node.alpha * node.beta;
        s = (node.alpha + node.beta)/p;

        dxdt[v]  = x[u];
        dxdt[u] += c_delay(n,t) + node.stimulus(t) - x[v] - s*x[u];
        dxdt[u] *= p;
    }
}
