
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#ifdef LSBM_USE_JMX
bool Kuramoto_node::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_field("P"), "Missing field 'P'." );
    P = stimulus_factory(in.getstruct("P"));
    return check();
}
#endif
