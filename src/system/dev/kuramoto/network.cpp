
//==================================================
// @title        network.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_Kuramoto_network::clear()
{
    param   .clear();
    c_delay .clear();

    t_threads           .clear();
    t_delayed_partition .clear();
    tmp_step_id         = 0;

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_Kuramoto_network::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[kuramoto_network.reset] Invalid configuration." );

    const uidx_t Nt = param.option.num_threads;
    if ( Nt > 1 )
    {
        // configure threads
        t_threads.create( Nt );
        t_delayed_partition = partition_delayed_edges_by_destination( param, Nt );
    }
    else
    {
        // sort by decreasing delay to optimise memory access pattern on records
        sort_delayed_edges_decreasing( param );
    }

    // clear additional memory
    c_delay.clear();
    tmp_step_id = 0;

    return true;
}

// ------------------------------------------------------------------------

void System_Kuramoto_network::_derivative_full( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[kuramoto_network._derivative_full] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    for ( uidx_t e: param.f_delayed )
    {
        auto& edge = param.edge(e);
        dxdt[edge.dst] += delayed_term( t, edge, x );
    }

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[edge.dst] += edge.coupling * sin(x[edge.src] - x[edge.dst]);
    }

    // add stimulus
    for ( uidx_t n = 0; n < nx; ++n )
        dxdt[n] += param.node(n).stimulus(t);
}

// ------------------------------------------------------------------------

void System_Kuramoto_network::_derivative_cache( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[kuramoto_network._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nx )
    {
        DRAYN_INFO("[kuramoto_network._derivative_cache] "
            "Cache is empty, falling back to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,x,dxdt);
        return;
    }

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[edge.dst] += edge.coupling * sin(x[edge.src] - x[edge.dst]);
    }

    // add stimulus
    for ( uidx_t n = 0; n < nx; ++n )
        dxdt[n] += c_delay(n,t) + param.node(n).stimulus(t);
}
