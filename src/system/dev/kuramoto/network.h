
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_Kuramoto_network
    : public NeuronalSystem<false>
{
public:

    using self        = System_Kuramoto_network;
    using parent      = NeuronalSystem<false>;
    using param_type  = Kuramoto_network;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_Kuramoto_network() { clear(); }

    void clear();
    bool reset();

    // dimensions
    inline uidx_t n_nodes() const { return param.n_nodes(); }
    inline uidx_t ndims() const { return n_nodes(); } // override DISOL parent

    // used by lsbm_helper
    inline time_type min_delay () const { return param.min_delay(); }
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_Kuramoto_network( const jmx::Struct& in )
        { configure(in); }

    inline bool configure( const jmx::Struct& in )
        { return param.extract(in) && reset(); }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const array_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const array_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const array_type& dxdt ) const;


    // Cache delayed terms contribution for faster multi-step integration
    // bind to event: before_step
    template <class H>
    void callback_before_step( H& dat );


    // ----------  =====  ----------

    param_type param; // network parameters

private:

    uidx_t                  tmp_step_id;
    DelayCache<double>       c_delay;
    dr::vector<std::thread>  t_threads;
    dr::vector<uidx_t>      t_delayed_partition;

    // form of delayed terms for the Kuramoto network model
    inline double delayed_term( time_type t, const edge_type& e, const state_type& s ) const
    {
        return (t < this->inducer.t_start) ? 0.0 :
            (
                this->inducer(t) * e.coupling *
                sin( this->delayed_value( t-e.delay, e.src, param.option.fixed_step ) - s[e.dst] )
            );
    }


    // ----------  =====  ----------

    class Cache_Worker
    {
    public:

        typedef System_Kuramoto_network  parent_type;
        typedef parent_type::time_type   time_type;
        typedef parent_type::state_type  state_type;

        Cache_Worker()
            : m_parent(nullptr) {}

        Cache_Worker( parent_type *ptr )
            : m_parent(ptr) {}

        // replace target time with current time and timestep
        void operator() ( uidx_t start_index, uidx_t stop_index, const state_type& s )
        {
            auto& cache        = m_parent->c_delay;
            const time_type t  = cache.t;
            const time_type dt = cache.dt;

            for ( uidx_t i = start_index; i < stop_index; ++i )
            {
                const auto& edge = m_parent->param.delayed(i);

                cache.left [edge.dst] += m_parent->delayed_term( t, edge, s );
                cache.right[edge.dst] += m_parent->delayed_term( t+dt, edge, s );
            }
        }

    private:
        parent_type *m_parent;
    };
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class H>
void System_Kuramoto_network::callback_before_step( H& dat )
{
    const time_type t   = dat.cur.t;
    const time_type dt  = dat.cur.dt;
    const state_type s  = dat.cur.x;

    if ( param.option.cache_delays && (tmp_step_id != dat.next.index) )
    {
        tmp_step_id = dat.next.index;
        c_delay.reset( dat.size(), t, dt );

        // distribute across threads or start single-thread worker
        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread( Cache_Worker(this), t_delayed_partition[i], t_delayed_partition[i+1], s );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else
        {
            Cache_Worker worker(this);
            worker( 0, param.n_delayed(), s );
        }
    }
}
