
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct Kuramoto_node
{
    stimulus_ptr P;

    // ----------  =====  ----------

    Kuramoto_node() { clear(); }

    inline void clear() { P.reset(); }
    inline bool check() const { return P && P->check(); }

    inline double stim( double t ) const { return (*P)(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Kuramoto_network:
    public TemplateNetwork< Kuramoto_node, default_edge, default_options >
{};
