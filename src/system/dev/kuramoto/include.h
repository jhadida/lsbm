
//==================================================
// @title        Kuramoto Model
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <vector>

// ----------  =====  ----------

namespace lsbm
{
    #include "param.h"
    #include "network.h"
}
