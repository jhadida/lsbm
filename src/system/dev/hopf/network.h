
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_Hopf_network
    : public NeuronalSystem
{
public:

    typedef System_Hopf_network  self;
    typedef Hopf_network         param_type;
    typedef NeuronalSystem       parent;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using deriv_type  = parent::deriv_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_Hopf_network()
        { clear(); }

    void clear();
    bool reset();

    // dimensions
    inline uidx_t  n_nodes    () const { return param.n_nodes(); }
    inline uidx_t  ndims () const { return 2*n_nodes(); } // override DISOL parent

    // used by lsbm_helper
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_Hopf_network( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && reset(); }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& z, const deriv_type& dzdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,z,dzdt);
        else
            _derivative_full(t,z,dzdt);
    }

    void _derivative_full  ( time_type t, const state_type& z, const deriv_type& dzdt ) const;
    void _derivative_cache ( time_type t, const state_type& z, const deriv_type& dzdt ) const;


    // Cache delayed terms contribution for faster multi-step integration
    // bind to event: before_step
    template <class IData>
    void callback_before_step( IData& dat );


    // ----------  =====  ----------

    param_type  param; // network params

private:

    DelayCache<double> c_delay;

    // form of delayed terms for the Hopf network model
    inline double delayed_term_x( time_type t, const edge_type& e, const state_type& s ) const
    {
        const double ind = this->induction(t);
        if ( ind == 0.0 ) return 0.0;
        return ind * e.coupling *
                ( this->delayed_value( t-e.delay, 2*e.src, param.option.fixed_step ) - s[2*e.dst] );
    }
    inline double delayed_term_y( time_type t, const edge_type& e, const state_type& s ) const
    {
        const double ind = this->induction(t);
        if ( ind == 0.0 ) return 0.0;
        return ind * e.coupling *
                ( this->delayed_value( t-e.delay, 2*e.src+1, param.option.fixed_step ) - s[2*e.dst+1] );
    }

};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class IData>
void System_Hopf_network::callback_before_step( IData& dat )
{
    const state_type s  = dat.cur.x;
    const time_type  t  = dat.cur.t;
    const time_type  dt = dat.cur.dt;

    // delay caching
    if ( param.option.cache_delays )
    {
        c_delay.reset( dat.size(), t, dt );

        // iterate on network couplings
        if ( ! this->before_induction() ) for ( uidx_t e: param.f_delayed )
        {
            auto& edge = param.edge(e);

            c_delay.left [2*edge.dst+0] += delayed_term_x( t, edge, s );
            c_delay.right[2*edge.dst+0] += delayed_term_x( t+dt, edge, s );

            c_delay.left [2*edge.dst+1] += delayed_term_y( t, edge, s );
            c_delay.right[2*edge.dst+1] += delayed_term_y( t+dt, edge, s );
        }
    }
}
