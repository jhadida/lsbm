
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct Hopf_node
{
    using input_type = Stimulus_timecourse;
    input_type P;

    double alpha, omega; // bifurcation and frequency parameter (in rad/sec)

    // ----------  =====  ----------

    Hopf_node() { clear(); }

    void clear();
    bool check() const;

    inline double stimulus( double t ) const { return P.val(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct Hopf_network:
    public TemplateNetwork<Hopf_node,default_edge,default_options>
{};
