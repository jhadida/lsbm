
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void Hopf_node::clear()
{
    P.clear();
    alpha = 0.0;
    omega = 1.0;
}

// ------------------------------------------------------------------------

bool Hopf_node::check() const
{
    return P.check() && (omega > 0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool Hopf_node::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({"alpha","omega","P"}), "Missing field(s)." );

    DRAYN_ASSERT_RF( P.configure(ms["P"]), "Couldn't configure input." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["alpha"],alpha), "Couldn't extract 'alpha'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["omega"],omega), "Couldn't extract 'omega'." );

    return true;
}
#endif
