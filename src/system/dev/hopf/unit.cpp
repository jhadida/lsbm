
//==================================================
// @title        unit.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_Hopf_Unit::clear()
{
    Z.clear();
    parent::clear();
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool System_Hopf_Unit::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( Z.extract(ms), "Couldn't extract unit params." );
    DRAYN_ASSERT_RF( Z.check(), "Invalid unit params." );

    return true;
}
#endif

// ------------------------------------------------------------------------

void System_Hopf_Unit::derivative( time_type t, const state_type& z, const deriv_type& dzdt ) const
{
    const double x = z[0];
    const double y = z[1];

    dzdt[0] = (Z.alpha - x*x - y*y) * x - Z.omega*y + Z.stimulus(t);
    dzdt[1] = (Z.alpha - x*x - y*y) * y + Z.omega*x + Z.stimulus(t);
}
