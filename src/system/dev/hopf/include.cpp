
//==================================================
// @title        Hopf Model
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

namespace lsbm
{
    #include "param.cpp"
    #include "unit.cpp"
    #include "network.cpp"
}
