
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_Hopf_Unit
    : public NeuronalSystem
{
public:

    typedef System_Hopf_Unit    self;
    typedef Hopf_node           param_type;
    typedef NeuronalSystem      parent;

    typedef parent::time_type   time_type;
    typedef parent::state_type  state_type;
    typedef parent::deriv_type  deriv_type;

    // ----------  =====  ----------


    System_Hopf_Unit()
        { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes    () const { return 1; }
    inline uidx_t ndims () const { return 2*n_nodes(); } // override DISOL parent

    // Matlab stuff
    #ifdef LSBM_USE_JMX
    System_Hopf_Unit( const jmx::Struct& ms )
        { configure(ms); }

    bool configure( const jmx::Struct& ms );
    #endif

    void derivative( time_type t, const state_type& z, const deriv_type& dzdt ) const;

    // ----------  =====  ----------

    param_type Z;
};
