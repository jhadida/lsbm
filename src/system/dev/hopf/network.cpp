
//==================================================
// @title        network.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_Hopf_network::clear()
{
    param   .clear();
    c_delay .clear();

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_Hopf_network::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[hopf_network.reset] Invalid configuration." );
    c_delay.clear();
    return true;
}

// ------------------------------------------------------------------------

void System_Hopf_network::_derivative_full( time_type t, const state_type& z, const deriv_type& dzdt ) const
{
    const uidx_t nz = z.size();
    DRAYN_DASSERT_R( nz == ndims(), "[hopf_network._derivative_full] Unexpected state size." );

    // initialize output
    dr::fill( dzdt, 0.0 );

    // set delayed contributions
    if ( !this->before_induction(t) ) for ( uidx_t e: param.f_delayed )
    {
        auto& edge = param.edge(e);

        dzdt[2*edge.dst+0] += delayed_term_x( t, edge, z );
        dzdt[2*edge.dst+1] += delayed_term_y( t, edge, z );
    }

    // set instant contributions
    uidx_t x_src, x_dst, y_src, y_dst;
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        x_src = 2*edge.src;  x_dst = 2*edge.dst;
        y_src = x_src+1;     y_dst = x_dst+1;

        dzdt[ x_dst ] += edge.coupling * ( z[x_src] - z[x_dst] );
        dzdt[ y_dst ] += edge.coupling * ( z[y_src] - z[y_dst] );
    }

    // iterate on each node
    for ( uidx_t n = 0; n < n_nodes(); ++n )
    {
        auto& node = param.node(n);

        const double x = z[2*n+0];
        const double y = z[2*n+1];
        const double p = node.stimulus(t);

        dzdt[2*n+0] = (node.alpha - x*x - y*y)*x - node.omega*y + dzdt[2*n+0] + p; // x
        dzdt[2*n+1] = (node.alpha - x*x - y*y)*y + node.omega*x + dzdt[2*n+1] + p; // y
    }
}

// ------------------------------------------------------------------------

void System_Hopf_network::_derivative_cache( time_type t, const state_type& z, const deriv_type& dzdt ) const
{
    const uidx_t nz = z.size();
    DRAYN_DASSERT_R( nz == ndims(), "[hopf_network._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nz )
    {
        DRAYN_INFO("[hopf_network._derivative_cache] "
            "Cache is empty, falling back to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,z,dzdt);
        return;
    }

    // initialize output
    dr::fill( dzdt, 0.0 );

    // set instant contributions
    uidx_t x_src, x_dst, y_src, y_dst;
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        x_src = 2*edge.src;  x_dst = 2*edge.dst;
        y_src = x_src+1;     y_dst = x_dst+1;

        dzdt[ x_dst ] += edge.coupling * ( z[x_src] - z[x_dst] );
        dzdt[ y_dst ] += edge.coupling * ( z[y_src] - z[y_dst] );
    }

    // iterate on each node
    for ( uidx_t n = 0; n < n_nodes(); ++n )
    {
        auto& node = param.node(n);

        const double x = z[2*n+0];
        const double y = z[2*n+1];
        const double p = node.stimulus(t);

        dzdt[2*n+0] = (node.alpha - x*x - y*y)*x - node.omega*y + dzdt[2*n+0] + c_delay(2*n+0,t) + p;
        dzdt[2*n+1] = (node.alpha - x*x - y*y)*y + node.omega*x + dzdt[2*n+1] + c_delay(2*n+1,t) + p;
    }
}
