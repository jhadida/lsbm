
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct CMC_Edge
{
    uidx_t src, dst;
    double  delay, forward, backward, lateral;

    // ----------  =====  ----------

    CMC_Edge() { clear(); }

    void clear();
    inline virtual bool check() const
        { return delay >= 0.0; }

    // display method
    inline void display() const
    {
        drayn_print( "(%u,%u) = {delay=%.2f,forward=%.2f,backward=%.2f,lateral=%.2f}",
            src, dst, delay, forward, backward, lateral );
    }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    virtual bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct CMC_node
{
    using sigmoid_type = Sigmoid<sigmoid_logistic>;
    using input_type   = Stimulus_timecourse;

    sigmoid_type S;
    input_type P;

    double tau_e, tau_i; // membrane time-constants
    double He, Hi;       // synaptic gains
    double gamma[4];     // local couplings

    /**
     * NOTE:
     * The sigmoid parameters (in the paper) nu0 and r are mapped to the sigmoid parameters mu and sigma.
     * We have (mu = nu0), and (sigma = 1/r).
     *
     * Here we use a natural sigmoid function.
     * If e0 is different from 1, simply multiply by as much:
     *  - the local couplings gamma (1,2,3,4)
     *  - the edges strengths (forward,backward,lateral)
     */

    // ----------  =====  ----------

    CMC_node() { clear(); }

    void clear();
    bool check() const;

    inline double sigmoid  ( double x ) const { return 2*S.val(x)-1.0; }
    inline double stimulus ( double t ) const { return P.val(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct CMC_network:
    public TemplateNetwork<CMC_node,CMC_Edge,default_options>
{};
