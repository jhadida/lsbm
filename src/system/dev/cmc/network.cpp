
//==================================================
// @title        network.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

double System_CMC_network::delayed_term( time_type t, const edge_type& edge ) const
{
    disol::LinearInterpolant lin;

    // induction constraint
    if ( t < this->inducer.t_start )
        return 0.0;

    // current record index (= effective size)
    const uidx_t ridx = this->records.index();

    // query time
    const time_type tq = t - edge.delay;

    DRAYN_DASSERT_RV( ridx > 0, 0.0, "Empty records." );
    DRAYN_DASSERT_RV( tq >= this->records[0] && tq <= this->records[ridx-1], 0.0,
        "Query outside known timeframe (extrapolation)." );

    // find interval in which the query time falls
    if ( param.option.fixed_step )
        disol::interp_linear_arithmetic( this->records, ridx, tq, lin );
    else
        disol::interp_linear_ascending( this->records, ridx, tq, lin );

    /**
     * States variables for unit k:
     * 8*k + 0: x
     * 8*k + 1: dx
     * 8*k + 2: y
     * 8*k + 3: dy
     * 8*k + 4: z
     * 8*k + 5: dz
     * 8*k + 6: v
     * 8*k + 7: dv
     */

    // components of average pyramidal membrane depolarisation at the source
    double Z =
        lin.wleft  * this->records( lin.left,  8*edge.src + 4 ) +
        lin.wright * this->records( lin.right, 8*edge.src + 4 ) ;

    double V =
        lin.wleft  * this->records( lin.left,  8*edge.src + 6 ) +
        lin.wright * this->records( lin.right, 8*edge.src + 6 ) ;

    // Pyramidal cell firing rate of region edge.src
    return this->inducer(t) * param.node(edge.src).sigmoid(Z-V);
}

// ------------------------------------------------------------------------

void System_CMC_network::derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[cmc_network.derivative] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    double Sy;

    // set delayed contributions
    for ( uidx_t e: param.f_delayed )
    {
        auto& edge = param.edge(e);
        Sy = delayed_term( t, edge );

        dxdt[ 8*edge.dst + 1 ] += (edge.forward  + edge.lateral) * Sy;
        dxdt[ 8*edge.dst + 3 ] += (edge.backward + edge.lateral) * Sy;
        // dxdt[ 8*edge.dst + 5 ] += (edge.backward + edge.lateral) * Sy;
    }

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        Sy = param.node(edge.src).sigmoid( x[8*edge.src+4] - x[8*edge.src+6] );

        dxdt[ 8*edge.dst + 1 ] += (edge.forward  + edge.lateral) * Sy;
        dxdt[ 8*edge.dst + 3 ] += (edge.backward + edge.lateral) * Sy;
        // dxdt[ 8*edge.dst + 5 ] += (edge.backward + edge.lateral) * Sy;
    }

    /**
     * States variables for unit k:
     * 8*k + 0: x
     * 8*k + 1: dx
     * 8*k + 2: y
     * 8*k + 3: dy
     * 8*k + 4: z
     * 8*k + 5: dz
     * 8*k + 6: v
     * 8*k + 7: dv
     */

    // finalise
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);
        Sy = node.sigmoid( x[8*n+4] - x[8*n+6] );

        // (optimisation) avoid computing the sum twice
        dxdt[ 8*n + 5 ] = dxdt[ 8*n + 3 ];

        // assign derivatives
        dxdt[ 8*n + 0 ] = x[ 8*n + 1 ];
        dxdt[ 8*n + 2 ] = x[ 8*n + 3 ];
        dxdt[ 8*n + 4 ] = x[ 8*n + 5 ];
        dxdt[ 8*n + 6 ] = x[ 8*n + 7 ];

        dxdt[ 8*n + 1 ] =
            (node.He/node.tau_e)*( dxdt[ 8*n + 1 ] + node.gamma[0]*Sy + node.stimulus(t) )
            - ( 2*x[ 8*n + 1 ] + x[ 8*n + 0 ]/node.tau_e )/node.tau_e ;

        dxdt[ 8*n + 3 ] =
            (node.He/node.tau_e)*( dxdt[ 8*n + 3 ] + node.gamma[1]*Sy )
            - ( 2*x[ 8*n + 3 ] + x[ 8*n + 2 ]/node.tau_e )/node.tau_e ;

        dxdt[ 8*n + 5 ] =
            (node.He/node.tau_e)*( dxdt[ 8*n + 5 ] + node.gamma[2]*node.sigmoid(x[ 8*n + 0 ]) )
            - ( 2*x[ 8*n + 5 ] + x[ 8*n + 4 ]/node.tau_e )/node.tau_e ;

        dxdt[ 8*n + 7 ] =
            (node.Hi/node.tau_i)*node.gamma[3]*node.sigmoid(x[ 8*n + 2 ])
            - ( 2*x[ 8*n + 7 ] + x[ 8*n + 6 ]/node.tau_i )/node.tau_i ;
    }
}
