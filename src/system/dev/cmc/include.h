
//==================================================
// @title        M/EEG Canonical Microcircuit
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * See paper:
 * Modelling event-related responses in the brain, O. David, NeuroImage 2005
 * DOI: 10.1016/j.neuroimage.2004.12.030
 */


#include <vector>

// ----------  =====  ----------

namespace lsbm
{
    #include "param.h"
    #include "unit.h"
    #include "network.h"
}
