
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void CMC_Edge::clear()
{
    src = dst = 0;
    delay = 0.0;
    forward = backward = lateral = 0.0;
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool CMC_Edge::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "[CMC_Edge.extract] Input structure is invalid." );
    DRAYN_ASSERT_RF( ms.has_fields({"src","dst","delay","forward","backward","lateral"}),
        "[CMC_Edge.extract] Missing field(s)." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["src"],src), "[CMC_Edge.extract] Couldn't extract field 'src'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["dst"],dst), "[CMC_Edge.extract] Couldn't extract field 'dst'." );
    DRAYN_ASSERT_RF( src*dst > 0, "[CMC_Edge.extract] Indices should be positive." );
    --src; --dst; // Matlab indexing begins at 1

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["delay"],delay), "[CMC_Edge.extract] Couldn't extract field 'delay'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["forward"],forward), "[CMC_Edge.extract] Couldn't extract field 'forward'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["backward"],backward), "[CMC_Edge.extract] Couldn't extract field 'backward'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["lateral"],lateral), "[CMC_Edge.extract] Couldn't extract field 'lateral'." );

    double max_coupling = std::max( dr::op_abs(forward), dr::op_abs(backward) );
           max_coupling = std::max(      max_coupling, dr::op_abs(lateral)  );

    DRAYN_ASSERT_ERR( max_coupling > dr::c_num<double>::eps,
        "[CMC_Edge.extract] Very small coupling magnitude, consider neglecting weak edges for speed." );

    return check();
}
#endif

// ------------------------------------------------------------------------

void CMC_node::clear()
{
    tau_e = tau_i = 0.0;
    He = Hi = 0.0;
    std::fill_n( gamma, 4, 0.0 );

    S.clear();
    P.clear();
}

// ------------------------------------------------------------------------

bool CMC_node::check() const
{
    static const double eps = dr::c_num<double>::eps;

    return S.check() && P.check() &&
        (tau_e > eps) && (tau_i > eps) && (He >= 0.0 ) && (Hi >= 0.0 ) &&
        (gamma[0] >= 0.0) && (gamma[1] >= 0.0) && (gamma[2] >= 0.0) && (gamma[3] >= 0.0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool CMC_node::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({
        "S", "P", "tau_e", "tau_i", "He", "Hi", "gamma"
    }), "Missing field(s)." );

    DRAYN_ASSERT_RF( S.configure(ms["S"]), "Couldn't configure sigmoid." );
    DRAYN_ASSERT_RF( P.configure(ms["P"]), "Couldn't configure input." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["tau_e"],tau_e), "Couldn't extract 'tau_e'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["tau_i"],tau_i), "Couldn't extract 'tau_i'." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["He"],He), "Couldn't extract 'He'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["Hi"],Hi), "Couldn't extract 'Hi'." );

    // copy values of gamma locally
    const mxArray *mgamma = ms["gamma"];
    DRAYN_ASSERT_RF( mgamma, "Invalid mex array." );
    DRAYN_ASSERT_RF( mxGetClassID(mgamma) == dr::mx_type<double>::id, "Field 'gamma' should be of class double." );
    DRAYN_ASSERT_RF( mxGetNumberOfElements(mgamma) == 4, "Field 'gamma' should contain 4 elements." );
    std::copy_n( static_cast<const double*>(mxGetData(mgamma)), 4, gamma );

    return true;
}
#endif
