
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_CMC_network
    : public NeuronalSystem
{
public:

    typedef System_CMC_network  self;
    typedef CMC_network         param_type;
    typedef NeuronalSystem      parent;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using deriv_type  = parent::deriv_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_CMC_network()
        { clear(); }

    inline void clear() { param.clear(); parent::clear(); }
    inline bool reset() { return true; }

    // dimensions
    inline uidx_t  n_nodes    () const { return param.n_nodes(); }
    inline uidx_t  ndims () const { return 8*n_nodes(); }

    // used by lsbm_helper
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_CMC_network( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && param.check(); }

#endif


    // Derivative (no cache implementation)
    void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const;

    // ----------  =====  ----------

    param_type param; // network parameters

private:

    // form of delayed terms for the canonical micro-circuit
    double delayed_term( time_type tq, const edge_type& edge ) const;

};
