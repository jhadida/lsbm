
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_CMC_Unit
    : public NeuronalSystem
{
public:

    typedef System_CMC_Unit     self;
    typedef CMC_node            param_type;
    typedef NeuronalSystem      parent;

    typedef parent::time_type   time_type;
    typedef parent::state_type  state_type;
    typedef parent::deriv_type  deriv_type;

    // ----------  =====  ----------


    System_CMC_Unit()
        { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes    () const { return 1; }
    inline uidx_t ndims () const { return 8*n_nodes(); } // override DISOL parent

    // Matlab stuff
    #ifdef LSBM_USE_JMX
    System_CMC_Unit( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && param.check(); }
    #endif

    void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const;

    // ----------  =====  ----------

    param_type param;
};
