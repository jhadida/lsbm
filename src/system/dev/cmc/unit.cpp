
//==================================================
// @title        unit.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_CMC_Unit::clear()
{
    param.clear();
    parent::clear();
}

// ------------------------------------------------------------------------

/**
 * States variables:
 * 0: x
 * 1: dx
 * 2: y
 * 3: dy
 * 4: z
 * 5: dz
 * 6: v
 * 7: dv
 */

void System_CMC_Unit::derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const param_type& p = param; // short name
    const double Sy = p.sigmoid( x[4] - x[6] );

    dxdt[0] = x[1];
    dxdt[2] = x[3];
    dxdt[4] = x[5];
    dxdt[6] = x[7];

    dxdt[1] = (p.He/p.tau_e)*( p.gamma[0]*Sy + p.stimulus(t) ) - ( 2*x[1] + x[0]/p.tau_e )/p.tau_e ;
    dxdt[3] = (p.He/p.tau_e)*  p.gamma[1]*Sy                   - ( 2*x[3] + x[2]/p.tau_e )/p.tau_e ;
    dxdt[5] = (p.He/p.tau_e)*  p.gamma[2]*p.sigmoid(x[0])      - ( 2*x[5] + x[4]/p.tau_e )/p.tau_e ;
    dxdt[7] = (p.Hi/p.tau_i)*  p.gamma[3]*p.sigmoid(x[2])      - ( 2*x[7] + x[6]/p.tau_i )/p.tau_i ;
}
