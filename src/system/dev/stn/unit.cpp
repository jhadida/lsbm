
//==================================================
// @title        unit.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_STN_Unit::clear()
{
    E.clear();
    I.clear();

    cee = cei = cie = cii = 0.0;
    parent::clear();
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool System_STN_Unit::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({
        "E", "I", "cee", "cei", "cie", "cii"
    }), "Missing field(s)." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["cee"],cee), "Couldn't extract 'cee'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["cei"],cei), "Couldn't extract 'cei'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["cie"],cie), "Couldn't extract 'cie'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["cii"],cii), "Couldn't extract 'cii'." );

    DRAYN_ASSERT_RF( E.extract(ms["E"]), "Couldn't extract excitatory params." );
    DRAYN_ASSERT_RF( I.extract(ms["I"]), "Couldn't extract inhibitory params." );

    DRAYN_ASSERT_RF( E.check(), "Invalid excitatory params." );
    DRAYN_ASSERT_RF( I.check(), "Invalid inhibitory params." );

    return true;
}
#endif

// ------------------------------------------------------------------------

void System_STN_Unit::derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const double xe = x[0];
    const double xi = x[1];

    dxdt[0] = (-xe + (1 - E.rp*xe) * E.sigmoid( cee*xe + cie*xi + E.stimulus(t) )) / E.tau;
    dxdt[1] = (-xi + (1 - I.rp*xi) * I.sigmoid( cii*xi + cei*xe + I.stimulus(t) )) / I.tau;
}
