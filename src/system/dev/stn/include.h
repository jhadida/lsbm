
//==================================================
// @title        Srinivasan, Thorpe and Nunez
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// See: doi(10.3389/fncom.2013.00029)

#include <vector>
#include <thread>

// ----------  =====  ----------

namespace lsbm
{
    #include "param.h"
    #include "unit.h"
}
