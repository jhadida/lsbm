
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct STN_options
    : public default_options
{
    bool induction;

    // ----------  =====  ----------

    STN_options() { clear(); }

    inline virtual void clear()
        { default_options::clear(); induction = true; }

    void display() const;

    #ifdef LSBM_USE_JMX
    virtual bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct STN_node
{
    using sigmoid_type = Sigmoid<sigmoid_logistic>;
    using input_type   = Stimulus_timecourse;

    sigmoid_type S;
    input_type P;

    double A; // scale parameter, defined as tau_i / tau_e
    double T; // target point

    // ----------  =====  ----------

    STN_node() { clear(); }

    void clear();
    bool check() const;

    inline bool is_plastic() const { return eta > 0.0; }

    inline double sigmoid_prime ( double x ) const { return S.der(x); }
    inline double sigmoid       ( double x ) const { return S.val(x); }
    inline double stimulus      ( double t ) const { return P.val(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& ms );
    #endif
};

// ------------------------------------------------------------------------

struct STN_network:
    public TemplateNetwork<STN_node,default_edge,STN_options>
{};
