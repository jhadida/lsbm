
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void STN_options::display() const
{
    static const char* b2s[] = { "false", "true" };
    drayn_print( "[lsbm::STN_options] Selected options are:" );
    drayn_print( " - fixed step      : %s", b2s[this->fixed_step] );
    drayn_print( " - cache delays    : %s", b2s[this->cache_delays] );
    drayn_print( " - use induction   : %s", b2s[induction] );
    drayn_print( " - enable ISP      : %s", b2s[enable_isp] );
    drayn_print( " - record feedback : %s", b2s[record_feedback] );
    drayn_print( " - use PCHIP       : %s", b2s[use_pchip] );
    drayn_print( " - num threads     : " uidx_f "\n", this->num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool STN_options::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "[STN_options] Invalid input." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["enable_isp"],enable_isp,false),
        "[STN_options] Couldn't extract 'enable_isp'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["record_feedback"],record_feedback,false),
        "[STN_options] Couldn't extract 'record_feedback'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["use_pchip"],use_pchip,false),
        "[STN_options] Couldn't extract 'use_pchip'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["induction"],induction,true),
        "[STN_options] Couldn't extract 'induction'." );
    return default_options::extract(ms);
}
#endif

// ------------------------------------------------------------------------

void STN_node::clear()
{
    rp = tau = 0.0;
    eta = rho = 0.0;
    normalise = false;

    S.clear();
    P.clear();
}

// ------------------------------------------------------------------------

bool STN_node::check() const
{
    return S.check() && P.check() &&
        (rp >= 0.0) && (tau >= dr::c_num<double>::eps) &&
        (eta >= 0.0) && (rho >= 0.0) && (rho <= 1.0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool STN_node::extract( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({ "rp", "tau", "S", "P" }), "Missing field(s)." );

    DRAYN_ASSERT_RF( S.configure(ms["S"]), "Couldn't configure sigmoid." );
    DRAYN_ASSERT_RF( P.configure(ms["P"]), "Couldn't configure input." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["tau"],tau), "Couldn't extract 'tau'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["rp"],rp), "Couldn't extract 'rp'." );

    // these two must come together
    if ( ms.has_fields({"eta","rho"}) )
    {
        DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["eta"],eta), "Couldn't extract 'eta'." );
        DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["rho"],rho), "Couldn't extract 'rho'." );
    }
    else eta = rho = 0.0;

    DRAYN_ASSERT_RF( dr::mx_extract_scalar(ms["normalise"],normalise,false), "Couldn't extract 'normalise'." );

    return true;
}
#endif
