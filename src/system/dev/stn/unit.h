
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_STN_Unit
    : public NeuronalSystem
{
public:

    typedef System_STN_Unit  self;
    typedef WilsonCowan_node         param_type;
    typedef NeuronalSystem           parent;

    typedef parent::time_type        time_type;
    typedef parent::state_type       state_type;
    typedef parent::deriv_type       deriv_type;

    // ----------  =====  ----------


    System_STN_Unit()
        { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes    () const { return 2; }
    inline uidx_t ndims () const { return n_nodes(); } // override DISOL parent

    // Matlab stuff
    #ifdef LSBM_USE_JMX
    System_STN_Unit( const jmx::Struct& ms )
        { configure(ms); }

    bool configure( const jmx::Struct& ms );
    #endif

    void derivative ( time_type t, const state_type& x, const deriv_type& dxdt ) const;


    // Inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class IData>
    void callback_after_commit( IData& dat );


    // Public attributes so the object can be configured externally.
    // ----------  =====  ----------

    param_type E, I;
    double cee, cei, cie, cii;
};

// ------------------------------------------------------------------------

template <class IData>
void System_STN_Unit::callback_after_commit( IData& dat )
{
    if ( I.is_plastic() )
    {
        const double x_post = dat.cur.x[0];
        const double x_pre  = dat.cur.x[1];

        // Backward Euler step (-= because cie is negative)
        cie -= dat.cur.dt * I.eta*x_pre * ( x_post - I.rho );
        cie  = Drayn_MIN( cie, 0.0 );
    }
}
