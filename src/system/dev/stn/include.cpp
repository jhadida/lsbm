
//==================================================
// @title        Srinivasan, Thorpe and Nunez
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// See: doi(10.3389/fncom.2013.00029)

namespace lsbm
{
    #include "param.cpp"
    #include "unit.cpp"
}
