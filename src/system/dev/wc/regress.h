
//==================================================
// @title        regress.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



class System_WilsonCowan_regress
    : public NeuronalSystem<false>
{
public:

    using self        = System_WilsonCowan_regress;
    using parent      = NeuronalSystem<false>;
    using param_type  = WilsonCowan_network;

    using pool_type   = traits::pool;
    using time_type   = parent::time_type;
    using value_type  = parent::value_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;
    using edge_type   = param_type::edge_type;
    using ts_type     = disol::ts_ptr<const value_type,const time_type>;

    // ----------  =====  ----------


    System_WilsonCowan_regress() { clear(); }

    void clear();
    bool reset(); // If the parameters are set externally, call this method before integration

    // Properties
    inline uidx_t n_nodes() const { return param.n_nodes(); }
    inline uidx_t ndims() const { return n_nodes()/2; } // override DISOL parent

    // used by lsbm_helper
    inline time_type min_delay () const { return param.min_delay(); }
    inline time_type max_delay () const { return param.max_delay(); }


// Matlab stuff
#ifdef LSBM_USE_JMX

    System_WilsonCowan_regress( const jmx::Struct& in )
        { configure(in); }

    inline bool configure( const jmx::Struct& in )
        { return param.extract(in) && reset(); }

    // overload additional outputs
    inline void additional_outputs( jmx::Struct& out ) const {
        disol::jmx_export( a_stimulus, out.mkstruct("stimulus") );
    }

#endif


    // Derivative
    void derivative( time_type t, const state_type& x, const array_type& dxdt ) const;

    // Stimulus
    inline value_type stimulus( time_type t, uidx_t k ) const {

    }


    // Initialise additional outputs
    // bind to event: after_init
    template <class H>
    void callback_after_init( H& dat );

    // Implement inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class H>
    void callback_after_commit( H& dat );


    // ----------  =====  ----------

    param_type param;   // network parameters
    ts_type m_E, m_dE;  // measured excitatory / derivative

private:

    dr::shared<uidx_t>       t_delayed_partition, t_local_couplings;
    dr::shared<std::thread>  t_threads;
    dr::shared<double>       m_delayed;
    pool_type                a_stimulus;


    // ----------  =====  ----------
    // DELAYED TERM COMPUTATION

    // assign each element to delayed term in stimulus formula
    void assign_delayed( time_type t, const array_type& p ) const;

    // form of delayed terms for the Wilson-Cowan network model
    inline double delayed_term( time_type t, const edge_type& edge ) const {
        if ( edge.src % 2 ) // inhibitory
            return edge.coupling * this->records.pinterp( t-edge.delay, edge.src/2 );
        else // excitatory
            return edge.coupling * mE.pinterp( t-edge.delay, edge.src/2 );
    }

    // multi-thread workers
    void worker_deriv( time_type t, const array_type& p, uidx_t start, uidx_t stop ) const;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class H>
void System_WilsonCowan_regress::callback_after_init( H& dat )
{
    a_stimulus.init( ndims(), this->records.bsize(), dat.fix );
    a_stimulus.time() = dat.cur.t;
    for ( uidx_t i = 0; i < ndims(); i++ )
        a_stimulus.value(i) = 0.0; // TODO: set value
}

// ------------------------------------------------------------------------

template <class H>
void System_WilsonCowan_regress::callback_after_commit( H& dat )
{
    double x_post, x_pre;

    a_stimulus.increment();

    for ( uidx_t i = 1; i < n_nodes(); i += 2 ) // iterate over inhibitory nodes
    {
        auto& Inode = param.node(i);
        auto& ItoE  = param.edge(t_local_couplings[ 2*i + 1 ]);

        if ( Inode.is_plastic() )
        {
            // using "cur" because this is called after commit by the integrator
            x_post = dat.cur.x[ i-1 ]; // excitatory
            x_pre  = dat.cur.x[ i   ]; // inhibitory

            // Backward Euler step
            ItoE.coupling -= dat.cur.dt * Inode.eta*x_pre * ( x_post - Inode.rho ); // -= because I->E couplings are negative
            ItoE.coupling  = std::min( ItoE.coupling, 0.0 ); // make sure I doesn't become excitatory
        }

        // save coupling value
        a_stimulus.time() = dat.cur.t;
        a_stimulus.value(i/2) = ItoE.coupling;
    }
}
