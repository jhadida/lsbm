
//==================================================
// @title        system
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "inducer.cpp"
#include "default.cpp"

// ----------  =====  ----------


// Wilson-Cowan model
#include "library/wc/include.cpp"

// Conductance-based model
#include "library/cb/include.cpp"
#include "library/icb/include.cpp"

// Hopf model
// #include "library/hopf/include.cpp"

// Kuramoto model
// #include "library/kuramoto/include.cpp"

// Robinson models
// #include "library/robinson/include.cpp"

// Canonical micro-circuit model
// #include "library/cmc/include.cpp"
