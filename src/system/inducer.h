
//==================================================
// @title        inducer.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <memory>
#include <string>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

enum class inducer_id
{
    Step,
    Linear,
    Smoothstep,
    Smoothstep2,
    Smoothstep3,
    Smoothstep4
};

// ------------------------------------------------------------------------

struct Inducer_mapping: public nmap<inducer_id>
{
    Inducer_mapping()
    {
        this->define( "step",           inducer_id::Step        );
        this->define( "heaviside",      inducer_id::Step        );
        this->define( "linear",         inducer_id::Linear      );
        this->define( "smoothstep",     inducer_id::Smoothstep  );
        this->define( "smoothstep2",    inducer_id::Smoothstep2 );
        this->define( "smoothstep3",    inducer_id::Smoothstep3 );
        this->define( "smoothstep4",    inducer_id::Smoothstep4 );
    }
};

// ------------------------------------------------------------------------

// self-managed pointer to inducer function
typedef std::shared_ptr<Activation_abstract> inducer_ptr;

template <class K>
inducer_ptr make_inducer( double onset, double length ) {
    auto x = new Activation<K>();
    x->rsc.configure( onset, length );
    x->gain = 1.0;
    return inducer_ptr(x);
}

inducer_ptr inducer_factory( const std::string& name, double onset, double length );

#ifdef LSBM_USE_JMX
inducer_ptr inducer_factory( const jmx::Struct& cfg );
#endif

LSBM_NS_END
