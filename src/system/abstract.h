
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

/**
 * Abstract base class for all neuronal systems.
 */
template <bool sto=false>
class NeuronalSystem
    : public std::conditional< sto, 
        traits::system_ito, 
        traits::system
    >::type
{
public:

    using parent = typename std::conditional< sto, 
        traits::system_ito, 
        traits::system
    >::type;

    using state_type   = traits::state;
    using time_type    = traits::time;
    using value_type   = traits::value;
    using pool_type    = traits::pool;

    // ----------  =====  ----------

    using parent::records;
    inducer_ptr inducer;

    // Number of nodes in the system
    virtual uidx_t n_nodes() const =0;

    // Delay bounds (NETWORKS SHOULD OVERLOAD THIS)
    inline time_type min_delay() const { return dr::c_num<time_type>::max; }
    inline time_type max_delay() const { return 0.0; }

#ifdef LSBM_USE_JMX

    // Configure the system before integration
    virtual bool configure( const jmx::Struct& ) =0;

    // Additional outputs that the user might require through configuration
    void additional_outputs( jmx::Struct& ) const {}

#endif

protected:

    inline bool before_induction( time_type tq ) const {
        return inducer ? (inducer->threshold() > tq) : true;
    }

    inline double induction( time_type tq ) const {
        return inducer ? (*inducer)(tq) : 1.0;
    }

    // Interpolate past recorded states at query time tq
    inline void delayed_check() const {
        // tq < records.tstart() || tq > records.tend()
        if ( records.empty() ) 
            throw std::runtime_error("[lsbm.NeuronalSystem.delay] Records are not ready.");
    }

    inline void delayed_state( time_type tq, const state_type& state ) const {
        dr::interp_linear_state( records, tq, state, records.is_fixed_step() );
    }

    inline double delayed_value( time_type tq, uidx_t index ) const {
        return records.linterp( tq, index );
    }

};

LSBM_NS_END
