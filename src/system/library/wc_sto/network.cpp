
//==================================================
// @title        network_new.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_WilsonCowan_network_New::clear()
{
    param.clear();

    t_threads.clear();
    t_delayed_partition.clear();
    c_delay.clear();

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_WilsonCowan_network_New::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[wilson_cowan_network_new.reset] Invalid parameters." );

    const uidx_t Nt = param.option.num_threads;

    // configure threads
    if ( Nt > 1 )
    {
        t_threads.create( Nt );
        t_delayed_partition = partition_delayed_edges_by_destination( param, Nt );
    }
    // sort by decreasing delay to optimise memory access pattern on records
    else sort_delayed_edges_decreasing( param );

    // clear additional memory
    c_delay.clear();

    return true;
}

// ------------------------------------------------------------------------

double System_WilsonCowan_network_New::delayed_term( time_type t, const edge_type& edge ) const
{
    auto& node = param.node( edge.src );

    double Zj, Pj, Xj;
    Zj = delayed_value( t-edge.delay, 2*edge.src, param.option.fixed_step );
    Xj = delayed_value( t-edge.delay, 2*edge.src+1, param.option.fixed_step );
    Pj = node.stimulus( t-edge.delay );

    double ans = -Xj + (1 - node.rp*Xj) * node.sigmoid( Zj + Pj );
    ans = edge.coupling * ans / node.tau;

    return this->induction(t)*ans;
}

void System_WilsonCowan_network_New::worker_cache( uidx_t start, uidx_t stop ) const
{
    const time_type t  = c_delay.t;
    const time_type dt = c_delay.dt;

    for ( uidx_t i = start; i < stop; ++i )
    {
        const auto& edge = param.delayed(i);

        c_delay.left [edge.dst] += delayed_term( t, edge );
        c_delay.right[edge.dst] += delayed_term( t+dt, edge );
    }
}

void System_WilsonCowan_network_New::worker_deriv(
    time_type t, const deriv_type& dxdt, uidx_t start, uidx_t stop ) const
{
    for ( uidx_t i = start; i < stop; ++i )
    {
        auto& edge = param.delayed(i);
        dxdt[2*edge.dst] += delayed_term( t, edge ); // add to dZ_k
    }
}

// ------------------------------------------------------------------------

void System_WilsonCowan_network_New::_derivative_full( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[wilson_cowan_network_new._derivative_full] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    if (param.option.num_threads > 1)
    {
        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i] = std::thread(
                [this,t,dxdt]( uidx_t b, uidx_t e ){ this->worker_deriv(t,dxdt,b,e); },
                t_delayed_partition[i], t_delayed_partition[i+1]
            );

        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i].join();
    }
    else worker_deriv( t, dxdt, 0, param.n_delayed() );

    // set instantaneous contributions
    double Zj, Pj, Xj;
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        auto& node = param.node(edge.src);

        Zj = x[2*edge.src];
        Xj = x[2*edge.src+1];
        Pj = node.stimulus(t);

        dxdt[2*edge.dst] += edge.coupling * ( -Xj + (1 - node.rp*Xj) * node.sigmoid(Zj + Pj) ) / node.tau;
    }

    // finalise
    for ( uidx_t n = 0; n < n_nodes(); ++n )
    {
        auto& node = param.node(n);
        Zj = x[2*n];
        Xj = x[2*n+1];
        Pj = node.stimulus(t);

        dxdt[2*n+1] = ( -Xj + (1 - node.rp*Xj) * node.sigmoid(Zj + Pj) ) / node.tau;
    }
}

// ------------------------------------------------------------------------

void System_WilsonCowan_network_New::_derivative_cache( time_type t, const state_type& x, const deriv_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[wilson_cowan_network_new._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nx/2 )
    {
        DRAYN_INFO("[wilson_cowan_network_new._derivative_cache] "
            "Cache is empty, falling back to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,x,dxdt);
        return;
    }

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set instantaneous contributions
    double Zj, Pj, Xj;
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        auto& node = param.node(edge.src);

        Zj = x[2*edge.src];
        Xj = x[2*edge.src+1];
        Pj = node.stimulus(t);

        dxdt[2*edge.dst] += edge.coupling * ( -Xj + (1 - node.rp*Xj) * node.sigmoid(Zj + Pj) ) / node.tau;
    }

    // iterate on each node
    for ( uidx_t n = 0; n < n_nodes(); ++n )
    {
        auto& node = param.node(n);
        Zj = x[2*n];
        Xj = x[2*n+1];
        Pj = node.stimulus(t);

        dxdt[2*n]   += c_delay(n,t);
        dxdt[2*n+1]  = ( -Xj + (1 - node.rp*Xj) * node.sigmoid(Zj + Pj) ) / node.tau;
    }
}
