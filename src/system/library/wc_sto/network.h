
//==================================================
// @title        network_new.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_WilsonCowan_network_New
    : public NeuronalSystem
{
public:

    using self        = System_WilsonCowan_network_New;
    using parent      = NeuronalSystem;
    using param_type  = WilsonCowan_network;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using deriv_type  = parent::deriv_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_WilsonCowan_network_New()
        { clear(); }

    void clear();
    bool reset(); // If the parameters are set externally, call this method before integration

    // Properties
    inline uidx_t  n_nodes    () const { return param.n_nodes(); }
    inline uidx_t  ndims () const { return 2*n_nodes(); } // override DISOL parent
    // each node has two state equations: Zk (at index 2k) and Xk (at index 2k+1)

    // used by lsbm_helper
    inline time_type max_delay () const { return param.max_delay(); }


// Matlab stuff
#ifdef LSBM_USE_JMX

    System_WilsonCowan_network_New( const jmx::Struct& ms )
        { configure(ms); }

    inline bool configure( const jmx::Struct& ms )
        { return param.extract(ms) && reset(); }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const deriv_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const deriv_type& dxdt ) const;


    // Noise sampling and optional delayed terms caching for faster integration.
    // bind to event: before_step
    template <class IData>
    void callback_before_step( IData& dat );


    // ----------  =====  ----------

    param_type param; // network parameters

private:

    DelayCache<double>       c_delay;
    dr::vector<std::thread>  t_threads;
    dr::vector<uidx_t>      t_delayed_partition;

    // ----------  =====  ----------
    // DELAYED TERM COMPUTATION

    // form of delayed terms for the Wilson-Cowan network model
    double delayed_term( time_type t, const edge_type& edge ) const;

    // multi-thread workers
    void worker_cache( uidx_t start, uidx_t stop ) const;
    void worker_deriv( time_type t, const deriv_type& dxdt, uidx_t start, uidx_t stop ) const;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//

template <class IData>
void System_WilsonCowan_network_New::callback_before_step( IData& dat )
{
    // current time and time-step
    const time_type t  = dat.cur.t;
    const time_type dt = dat.cur.dt;

    // distribute across threads or start single-thread worker
    if ( param.option.cache_delays )
    {
        c_delay.reset( n_nodes(), t, dt );

        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread(
                    [this]( uidx_t b, uidx_t e ){ this->worker_cache(b,e); },
                    t_delayed_partition[i], t_delayed_partition[i+1]
                );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else worker_cache( 0, param.n_delayed() );
    }
}
