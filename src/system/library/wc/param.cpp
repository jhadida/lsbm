
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void WilsonCowan_options::display() const
{
    const char* b2s[] = { "false", "true" };
    drayn_print( "[lsbm::WilsonCowan_options] Selected options are:" );
    drayn_print( " - cache delays    : %s", b2s[this->cache_delays] );
    drayn_print( " - enable ISP      : %s", b2s[enable_isp] );
    drayn_print( " - num threads     : " uidx_f "\n", this->num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool WilsonCowan_options::extract( const jmx::Struct& in )
{
    enable_isp = in.getbool("enable_isp",false);
    return default_options::extract(in);
}
#endif

// ------------------------------------------------------------------------

void WilsonCowan_node::clear()
{
    S.reset();
    P.reset();
    am = tau = 0.0;
    eta = rho = 0.0;
    norm = false;
}

// ------------------------------------------------------------------------

bool WilsonCowan_node::check() const
{
    return S && P && S->check() && P->check() &&
        (am >= 0.0) && (tau >= dr::c_num<double>::eps) &&
        (eta >= 0.0) && (rho >= 0.0) && (rho <= 1.0);
}

// ------------------------------------------------------------------------

double WilsonCowan_node::sigm( double x ) const
{
    double v = S->val(x);
    if (norm) {
        x = S->val(0);
        v = std::max( (v-x)/(1-x), 0.0 );
    }
    return v;
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool WilsonCowan_node::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({ "am", "tau", "S", "P" }), "Missing field(s)." );

    S = sigmoid_factory(in.getstruct("S"));
    P = stimulus_factory(in.getstruct("P"));

    am = in.getnum("am");
    tau = in.getnum("tau");
    eta = in.getnum("eta",0.0);
    rho = in.getnum("rho",0.0);
    norm = in.getbool("norm",false);

    return check();
}
#endif
