
//==================================================
// @title        plugin.h
// @author       Jonathan Hadida
// @contact      jhadida.pub [at] gmail
//==================================================

template <class H>
struct Plugin_WilsonCowan_clamp
    : public disol::plugin_clamp<H>
{
public:

    using self        = Plugin_WilsonCowan_clamp<H>;
    using parent      = disol::plugin_clamp<H>;
    using handle_type = H;
    using signal_type = typename parent::signal_type;
    using time_type   = typename H::time_type;
    using value_type  = typename H::value_type;

    // ----------  =====  ----------

    Plugin_WilsonCowan_clamp()
        { this->configure(0,1); }
    Plugin_WilsonCowan_clamp( signal_type& sig ) { 
        this->configure(0,1);
        this->attach(sig); 
    }

protected:

    virtual void callback( handle_type& dat )
    {
        const uidx_t n = dat.next.x.size();
        for ( uidx_t i = 0; i < n; i++ )
            dat.next.x[i] = dr::op_clamp( dat.next.x[i], this->lower, this->upper );
    }
};
