
//==================================================
// @title        unit.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_WilsonCowan_unit::clear()
{
    E.clear();
    I.clear();

    cee = cei = cie = cii = 0.0;
    parent::clear();
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool System_WilsonCowan_unit::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({
        "E", "I", "cee", "cei", "cie", "cii"
    }), "Missing field(s)." );

    cee = in.getnum("cee");
    cei = in.getnum("cei");
    cie = in.getnum("cie");
    cii = in.getnum("cii");

    DRAYN_ASSERT_RF( E.extract(in.getstruct("E")), "Couldn't extract excitatory params." );
    DRAYN_ASSERT_RF( I.extract(in.getstruct("I")), "Couldn't extract inhibitory params." );

    return true;
}
#endif

// ------------------------------------------------------------------------

void System_WilsonCowan_unit::derivative( time_type t, const state_type& x, const array_type& dxdt ) const
{
    const double xe = x[0];
    const double xi = x[1];

    const double Pe = E.stim(t);
    const double Pi = I.stim(t);

    dxdt[0] = (-xe + (1 - E.am*xe) * E.sigm( cee*xe + cie*xi + Pe )) / E.tau;
    dxdt[1] = (-xi + (1 - I.am*xi) * I.sigm( cii*xi + cei*xe + Pi )) / I.tau;
}
