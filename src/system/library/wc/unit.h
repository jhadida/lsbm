
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_WilsonCowan_unit
    : public NeuronalSystem<false>
{
public:

    using self = System_WilsonCowan_unit;
    using parent = NeuronalSystem<false>;
    using param_type = WilsonCowan_node;

    using time_type = parent::time_type;
    using state_type = parent::state_type;
    using array_type = parent::array_type;

    // ----------  =====  ----------

    System_WilsonCowan_unit() { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes() const { return 2; }
    inline uidx_t ndims() const { return n_nodes(); } // override DISOL parent

    void derivative( time_type t, const state_type& x, const array_type& dxdt ) const;

    // Inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class H>
    void callback_after_commit( H& dat );

    // Matlab stuff
#ifdef LSBM_USE_JMX

    System_WilsonCowan_unit( const jmx::Struct& in ) 
        { configure(in); }

    bool configure( const jmx::Struct& in );

#endif


    // Public attributes so the object can be configured externally.
    // ----------  =====  ----------

    param_type E, I;
    double cee, cei, cie, cii;
};

// ------------------------------------------------------------------------

template <class H>
void System_WilsonCowan_unit::callback_after_commit( H& dat )
{
    if ( I.is_plastic() )
    {
        const double x_post = dat.cur.x[0];
        const double x_pre  = dat.cur.x[1];

        // Backward Euler step (-= because cie is negative)
        cie -= dat.cur.dt * I.eta*x_pre * ( x_post - I.rho );
        cie  = std::min( cie, 0.0 );
    }
}
