
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



class System_WilsonCowan_network
    : public NeuronalSystem<false>
{
public:

    using self        = System_WilsonCowan_network;
    using parent      = NeuronalSystem<false>;
    using param_type  = WilsonCowan_network;

    using pool_type   = traits::pool;
    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_WilsonCowan_network() { clear(); }

    void clear();
    bool reset(); // If the parameters are set externally, call this method before integration

    // Properties
    inline uidx_t n_nodes() const { return param.n_nodes(); }
    inline uidx_t ndims() const { return n_nodes(); } // override DISOL parent

    // used by lsbm_helper
    inline time_type min_delay () const { return param.min_delay(); }
    inline time_type max_delay () const { return param.max_delay(); }


// Matlab stuff
#ifdef LSBM_USE_JMX

    System_WilsonCowan_network( const jmx::Struct& in )
        { configure(in); }

    inline bool configure( const jmx::Struct& in )
        { return param.extract(in) && reset(); }

    // overload additional outputs
    inline void additional_outputs( jmx::Struct& out ) const {
        if ( param.option.enable_isp )
            disol::jmx_export( a_isp, out.mkstruct("isp") );
    }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const array_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const array_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const array_type& dxdt ) const;


    // Initialise additional outputs
    // bind to event: after_init
    template <class H>
    void callback_after_init( H& dat );

    // Noise sampling and optional delayed terms caching for faster integration.
    // bind to event: before_step
    template <class H>
    void callback_before_step( H& dat );

    // Implement inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class H>
    void callback_after_commit( H& dat );


    // ----------  =====  ----------

    param_type param; // network parameters

private:

    DelayCache<double>       c_delay;
    dr::shared<uidx_t>       t_delayed_partition, t_local_couplings;
    dr::shared<std::thread>  t_threads;
    pool_type                a_isp;


    // ----------  =====  ----------
    // DELAYED TERM COMPUTATION

    // form of delayed terms for the Wilson-Cowan network model
    inline double delayed_term( time_type t, const edge_type& edge ) const {
        const double fac = this->induction(t);
        return fac * edge.coupling * this->delayed_value( t-edge.delay, edge.src );
    }

    // multi-thread workers
    void worker_cache( uidx_t start, uidx_t stop ) const;
    void worker_deriv( time_type t, const array_type& dxdt, uidx_t start, uidx_t stop ) const;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class H>
void System_WilsonCowan_network::callback_after_init( H& dat )
{
    if ( param.option.enable_isp )
    {
        a_isp.init( n_nodes()/2, this->records.bsize(), dat.fix );
        a_isp.time() = dat.cur.t;
        for ( uidx_t i = 1; i < n_nodes(); i += 2 ) // iterate over inhibitory nodes
            a_isp.value(i/2) = param.edge(t_local_couplings[ 2*i + 1 ]).coupling; // cie
    }
}

// ------------------------------------------------------------------------

template <class H>
void System_WilsonCowan_network::callback_before_step( H& dat )
{
    // current time and time-step
    const time_type t  = dat.cur.t;
    const time_type dt = dat.cur.dt;

    // distribute across threads or start single-thread worker
    if ( param.option.cache_delays )
    {
        c_delay.reset( dat.size(), t, dt );

        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread(
                    [this]( uidx_t b, uidx_t e ){ this->worker_cache(b,e); },
                    t_delayed_partition[i], t_delayed_partition[i+1]
                );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else worker_cache( 0, param.n_delayed() );
    }
}

// ------------------------------------------------------------------------

template <class H>
void System_WilsonCowan_network::callback_after_commit( H& dat )
{
    double x_post, x_pre;

    // update local couplings
    if ( param.option.enable_isp )
    {
        a_isp.increment();

        for ( uidx_t i = 1; i < n_nodes(); i += 2 ) // iterate over inhibitory nodes
        {
            auto& Inode = param.node(i);
            auto& ItoE  = param.edge(t_local_couplings[ 2*i + 1 ]);

            if ( Inode.is_plastic() )
            {
                // using "cur" because this is called after commit by the integrator
                x_post = dat.cur.x[ i-1 ]; // excitatory
                x_pre  = dat.cur.x[ i   ]; // inhibitory

                // Backward Euler step
                ItoE.coupling -= dat.cur.dt * Inode.eta*x_pre * ( x_post - Inode.rho ); // -= because I->E couplings are negative
                ItoE.coupling  = std::min( ItoE.coupling, 0.0 ); // make sure I doesn't become excitatory
            }

            // save coupling value
            a_isp.time() = dat.cur.t;
            a_isp.value(i/2) = ItoE.coupling;
        }
    }
}
