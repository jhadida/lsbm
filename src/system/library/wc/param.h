
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct WilsonCowan_options: public default_options
{
    bool enable_isp;

    // ----------  =====  ----------

    WilsonCowan_options() { clear(); }

    inline void clear() { 
        default_options::clear(); 
        enable_isp = false; 
    }

    void display() const;

    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct WilsonCowan_node
{
    sigmoid_ptr S;
    stimulus_ptr P;

    double am;          // amortisation factor
    double tau;         // time-constant of population variations
    double eta, rho;    // plasticity parameters
    bool norm;          // use normalised version of the sigmoid

    // ----------  =====  ----------

    WilsonCowan_node() { clear(); }

    void clear();
    bool check() const;

    inline bool is_plastic() const { return eta > 0.0; }

    double sigm( double x ) const;
    inline double stim( double t ) const { return (*P)(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct WilsonCowan_network:
    public TemplateNetwork< WilsonCowan_node, default_edge, WilsonCowan_options >
{};
