
//==================================================
// @title        network.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_WilsonCowan_network::clear()
{
    param.clear();

    t_threads           .clear();
    t_delayed_partition .clear();
    t_local_couplings   .clear();

    c_delay .clear();
    a_isp   .clear();

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_WilsonCowan_network::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[WilsonCowan_network.reset] Invalid parameters." );

    const uidx_t Nt = param.option.num_threads;

    // configure threads
    if ( Nt > 1 )
    {
        t_threads.resize( Nt );
        t_delayed_partition = partition_delayed_edges_by_destination( param, Nt );
    }
    // sort by decreasing delay to optimise memory access pattern on records
    else sort_delayed_edges_decreasing( param );

    // find out indices of local couplings (used by ISP)
    t_local_couplings.resize( 2*param.n_nodes() );
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        if ( (edge.src/2) == (edge.dst/2) ) // same unit
            t_local_couplings.at( 2*edge.src + (edge.src != edge.dst) ) = e; // order: cee, cei, cii, cie
    }

    // clear additional memory
    a_isp.clear();
    c_delay.clear();

    return true;
}

// ------------------------------------------------------------------------

void System_WilsonCowan_network::worker_cache( uidx_t start, uidx_t stop ) const
{
    const time_type t  = c_delay.t;
    const time_type dt = c_delay.dt;

    for ( uidx_t i = start; i < stop; ++i )
    {
        const auto& edge = param.delayed(i);

        c_delay.left [edge.dst] += delayed_term( t, edge );
        c_delay.right[edge.dst] += delayed_term( t+dt, edge );
    }
}

void System_WilsonCowan_network::worker_deriv(
    time_type t, const array_type& dxdt, uidx_t start, uidx_t stop ) const
{
    for ( uidx_t i = start; i < stop; ++i )
    {
        auto& edge = param.delayed(i);
        dxdt[edge.dst] += delayed_term( t, edge );
    }
}

// ------------------------------------------------------------------------

void System_WilsonCowan_network::_derivative_full( time_type t, const state_type& x, const array_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[WilsonCowan_network._derivative_full] Unexpected state size." );

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    if (param.option.num_threads > 1)
    {
        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i] = std::thread(
                [this,t,dxdt]( uidx_t b, uidx_t e ){ this->worker_deriv(t,dxdt,b,e); },
                t_delayed_partition[i], t_delayed_partition[i+1]
            );

        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i].join();
    }
    else worker_deriv( t, dxdt, 0, param.n_delayed() );

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[edge.dst] += edge.coupling * x[edge.src];
    }

    // finalise
    for ( uidx_t n = 0; n < nx; ++n )
    {
        auto& node = param.node(n);
        dxdt[n] = ( -x[n] + (1.0 - node.am*x[n]) * node.sigm( dxdt[n] + node.stim(t) ) ) / node.tau;
    }
}

// ------------------------------------------------------------------------

void System_WilsonCowan_network::_derivative_cache( time_type t, const state_type& x, const array_type& dxdt ) const
{
    const uidx_t nx = x.size();
    DRAYN_DASSERT_R( nx == ndims(), "[WilsonCowan_network._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nx )
    {
        DRAYN_INFO("[WilsonCowan_network._derivative_cache] "
            "Cache is empty, falling back to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,x,dxdt);
        return;
    }

    // initialize output
    dr::fill( dxdt, 0.0 );

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        dxdt[edge.dst] += edge.coupling * x[edge.src];
    }

    // iterate on each node
    for ( uidx_t n = 0; n < nx; ++n )
    {
        auto& node = param.node(n);
        dxdt[n] = ( -x[n] + (1.0 - node.am*x[n]) * node.sigm(dxdt[n] + c_delay(n,t) + node.stim(t)) ) / node.tau;
    }
}
