
/**
 * Old implementation of all-to-all inhibitory synaptic plasticity.
 * This is now deprecated, but kept for reference.
 */
class ISP_Worker
{
public:

 typedef System_WilsonCowan_network  parent_type;

 ISP_Worker()
     : m_parent(nullptr) {}

 ISP_Worker( parent_type *ptr )
     : m_parent(ptr) {}

 template <class H>
 void operator() ( H& dat, uidx_t start_index, uidx_t stop_index )
 {
     static const auto is_inhibitory = []( uidx_t i )-> bool { return (i & 1) > 0; };
     static const auto is_excitatory = []( uidx_t i )-> bool { return !is_inhibitory(i); };

     auto& param = m_parent->param;

     double   x_post, x_pre;
     uidx_t  i_post, i_pre;

     for ( uidx_t i = start_index; i < stop_index; ++i )
     {
         auto& edge = param.edge(i);

         i_post = edge.dst; // post-synaptic (excitatory)
         i_pre  = edge.src; // pre-synaptic  (inhibitory)

         const auto& pre = param.node(i_pre);

         // only inhibitory -> excitatory synapses are plastic
         if ( !pre.is_plastic() || is_excitatory(i_pre) || is_inhibitory(i_post) ) continue;

         // using "cur" because this is called after commit by the integrator
         x_post = dat.cur.x[ i_post ];
         x_pre  = dat.cur.x[ i_pre  ];

         // Backward Euler step (-= because I->E couplings are negative)
         edge.coupling -= dat.cur.dt * pre.eta*x_pre * ( x_post - pre.rho );
         edge.coupling  = Drayn_MIN( edge.coupling, 0.0 );
     }
 }

private:
 parent_type *m_parent;
};
