
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

bool InstConductanceBased_model::check() const
{
    return (R_dep > R_hyp) && (R_dep > R_leak);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool InstConductanceBased_model::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({ // required fields
        "R_leak", "R_dep", "R_hyp"
    }), "Missing field(s)." );

    R_leak = in.getnum("R_leak");
    R_dep = in.getnum("R_dep");
    R_hyp = in.getnum("R_hyp");

    return check();
}
#endif



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



void InstConductanceBased_options::display() const
{
    const char* b2s[] = { "false", "true" };
    drayn_print( "[lsbm::InstConductanceBased_options] Selected options are:" );
    drayn_print( " - cache delays    : %s", b2s[this->cache_delays] );
    drayn_print( " - num threads     : " uidx_f "\n", this->num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool InstConductanceBased_options::extract( const jmx::Struct& in )
{
    return default_options::extract(in) && model.extract(in["model"]);
}
#endif

// ------------------------------------------------------------------------

void InstConductanceBased_node::clear()
{
    S.reset();
    C.reset();
    P.reset();

    tau = g_leak = 0.0;
}

// ------------------------------------------------------------------------

bool InstConductanceBased_node::check() const
{
    return S && C && P && 
        S->check() && C->check() && P->check() &&
        (tau >= dr::c_num<double>::eps) && (g_leak >= 0.0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool InstConductanceBased_node::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({ // required fields
        "S", "C", "P", "tau", "g_leak"
    }), "Missing field(s)." );

    S = sigmoid_factory(in.getstruct("S"));
    C = stimulus_factory(in.getstruct("C"));
    P = stimulus_factory(in.getstruct("P"));

    tau = in.getnum("tau");
    g_leak = in.getnum("g_leak");

    return check();
}
#endif
