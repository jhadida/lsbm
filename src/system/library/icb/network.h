
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_InstConductanceBased_network
    : public NeuronalSystem<false>
{
public:

    using self        = System_InstConductanceBased_network;
    using parent      = NeuronalSystem<false>;
    using param_type  = InstConductanceBased_network;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_InstConductanceBased_network() { clear(); }

    void clear();
    bool reset();

    // dimensions
    inline uidx_t n_nodes() const { return param.n_nodes(); }
    inline uidx_t ndims() const { return 3*n_nodes(); } // override DISOL parent

    // used by lsbm_helper
    inline time_type min_delay () const { return param.min_delay(); }
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_InstConductanceBased_network( const jmx::Struct& in )
        { configure(in); }

    inline bool configure( const jmx::Struct& in )
        { return param.extract(in) && reset(); }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const array_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const array_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const array_type& dxdt ) const;

    // Cache delayed firing-rates for faster multi-step integration
    // bind to event: before_step
    template <class H>
    void callback_before_step( H& dat );


    // ----------  =====  ----------

    param_type   param;

private:

    dr::shared<double>       m_cond; // conductances
    DelayCache<double>       c_delay;
    dr::shared<std::thread>  t_threads;
    dr::shared<uidx_t>       t_delayed_partition, t_local_couplings;


    // form of delayed terms for the conductance-based network model
    inline double delayed_term( time_type t, const edge_type& edge ) const
    {
        const double fac = this->induction(t);
        return fac * edge.coupling * param.node(edge.src).sigm( // delayed pre-synaptic potential
                this->delayed_value( t-edge.delay, edge.src )
            );
    }

    // multi-thread workers
    void worker_cache( uidx_t start, uidx_t stop ) const;
    void worker_deriv( time_type t, uidx_t start, uidx_t stop ) const;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//

template <class H>
void System_InstConductanceBased_network::callback_before_step( H& dat )
{
    // current time and time-step
    const time_type t  = dat.cur.t;
    const time_type dt = dat.cur.dt;

    // delay caching
    if ( param.option.cache_delays )
    {
        c_delay.reset( 2*n_nodes(), t, dt ); // two conductances for each node

        // iterate on compressed storage
        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread(
                    [this]( uidx_t b, uidx_t e ){ this->worker_cache(b,e); },
                    t_delayed_partition[i], t_delayed_partition[i+1]
                );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else worker_cache( 0, param.n_delayed() );
    }
}
