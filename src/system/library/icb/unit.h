
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_InstConductanceBased_unit
    : public NeuronalSystem<false>
{
public:

    using self        =  System_InstConductanceBased_unit;
    using parent      =  NeuronalSystem<false>;
    using param_type  =  InstConductanceBased_node;
    using model_type  =  InstConductanceBased_model;

    using time_type   =  parent::time_type;
    using state_type  =  parent::state_type;
    using array_type  =  parent::array_type;

    // ----------  =====  ----------


    System_InstConductanceBased_unit() { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes() const { return 2; }
    inline uidx_t ndims() const { return n_nodes(); } // override DISOL parent

    void derivative( time_type t, const state_type& x, const array_type& dxdt ) const;

    // Matlab stuff
#ifdef LSBM_USE_JMX

    System_InstConductanceBased_unit( const jmx::Struct& ms )
        { configure(ms); }

    bool configure( const jmx::Struct& ms );

#endif


    // ----------  =====  ----------

    param_type  E, I;
    model_type  model;

    double wee, wei, wii, wie;
};
