
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct InstConductanceBased_model
{
    // Reversal potentials
    double R_leak;
    double R_hyp; // hyperpolarising (inhibitory)
    double R_dep; // depolarising (excitatory)

    // ----------  =====  ----------

    InstConductanceBased_model():
        R_leak (-60.0),
        R_hyp  (-80.0),
        R_dep  (  0.0)
    {}

    bool check() const;

    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct InstConductanceBased_options
    : public default_options
{
    InstConductanceBased_model model;

    // ----------  =====  ----------

    InstConductanceBased_options() { clear(); }

    inline virtual void clear()
        { default_options::clear(); }

    void display() const;

    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct InstConductanceBased_node
{
    sigmoid_ptr S;
    stimulus_ptr C, P;

    double tau, g_leak;

    // ----------  =====  ----------

    InstConductanceBased_node() { clear(); }

    void clear();
    bool check() const;

    inline double sigm( double v ) const { return S->val(v); }
    inline double stim( double t ) const { return (*P)(t); }
    inline double curr( double t ) const { return (*C)(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct InstConductanceBased_network
    : public TemplateNetwork< InstConductanceBased_node, default_edge, InstConductanceBased_options >
{};
