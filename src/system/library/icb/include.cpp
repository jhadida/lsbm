
//==================================================
// @title        Instantaneous Conductance-Based Model
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

LSBM_NS_START

#include "param.cpp"
#include "unit.cpp"
#include "network.cpp"

LSBM_NS_END
