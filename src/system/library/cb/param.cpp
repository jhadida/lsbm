
//==================================================
// @title        param.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

bool ConductanceBased_model::check() const
{
    const double eps = dr::c_num<double>::eps;
    return (tau_gE >= eps) && (tau_gI >= eps)
        && (R_dep > R_hyp) && (R_dep > R_leak);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool ConductanceBased_model::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({ // required fields
        "tau_gE", "tau_gI",
        "R_leak", "R_dep", "R_hyp"
    }), "Missing field(s)." );

    tau_gE = in.getnum("tau_gE");
    tau_gI = in.getnum("tau_gI");

    R_leak = in.getnum("R_leak");
    R_dep = in.getnum("R_dep");
    R_hyp = in.getnum("R_hyp");

    return check();
}
#endif



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



void ConductanceBased_options::display() const
{
    const char* b2s[] = { "false", "true" };
    drayn_print( "[lsbm::ConductanceBased_options] Selected options are:" );
    drayn_print( " - cache delays    : %s", b2s[this->cache_delays] );
    drayn_print( " - enable ISP      : %s", b2s[enable_isp] );
    drayn_print( " - num threads     : " uidx_f "\n", this->num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool ConductanceBased_options::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_field("model"), "Missing field 'model'." );

    enable_isp = in.getbool("enable_isp",false);
    return default_options::extract(in) && model.extract(in["model"]);
}
#endif

// ------------------------------------------------------------------------

void ConductanceBased_node::clear()
{
    S.reset();
    C.reset();
    P.reset();

    tau_v = g_leak = 0.0;
    eta = rho = 0.0;
}

// ------------------------------------------------------------------------

bool ConductanceBased_node::check() const
{
    const double eps = dr::c_num<double>::eps;
    return S && C && P && 
        S->check() && C->check() && P->check() &&
        (tau_v >= eps) && (g_leak >= 0.0) &&
        (eta >= 0.0) && (rho >= 0.0);
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool ConductanceBased_node::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({ // required fields
        "S", "C", "P", "tau_v", "g_leak"
    }), "Missing field(s)." );

    S = sigmoid_factory(in.getstruct("S"));
    C = stimulus_factory(in.getstruct("C"));
    P = stimulus_factory(in.getstruct("P"));

    tau_v = in.getnum("tau_v");
    g_leak = in.getnum("g_leak");
    eta = in.getnum("eta",0.0);
    rho = in.getnum("rho",0.0);

    return check();
}
#endif
