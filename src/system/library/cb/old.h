
/**
 * Old implementation of all-to-all inhibitory synaptic plasticity.
 * This is now deprecated, but kept for reference.
 */
class ISP_Worker
{
public:

 typedef System_ConductanceBased_network  parent_type;

 ISP_Worker()
     : m_parent(nullptr) {}

 ISP_Worker( parent_type *ptr )
     : m_parent(ptr) {}

 template <class H>
 void operator() ( H& dat, uidx_t start_index, uidx_t stop_index )
 {
     static const auto is_inhibitory = []( uidx_t i )-> bool { return (i & 1); };
     static const auto is_excitatory = []( uidx_t i )-> bool { return !is_inhibitory(i); };

     auto& param = m_parent->param;

     double   v_post, v_pre;
     double   f_post, f_pre;
     uidx_t  i_post, i_pre;

     for ( uidx_t i = start_index; i < stop_index; ++i )
     {
         auto& edge = param.edge(i);

         i_post = edge.dst; // post-synaptic (excitatory)
         i_pre  = edge.src; // pre-synaptic  (inhibitory)

         const auto& pre = param.node(i_pre);

         // only inhibitory -> excitatory synapses are plastic
         if ( !pre.is_plastic() || is_excitatory(i_pre) || is_inhibitory(i_post) ) continue;

         // using "cur" because this is called after commit by the integrator
         v_post = dat.cur.x[ 3*i_post ];
         v_pre  = dat.cur.x[ 3*i_pre  ];

         f_post = param.node(i_post) .firing_rate( v_post );
         f_pre  = pre                .firing_rate( v_pre  );

         // Backward Euler step
         edge.coupling += dat.cur.dt * pre.eta*f_pre * ( f_post - pre.rho );
         edge.coupling  = Drayn_MAX( edge.coupling, 0.0 );
     }
 }

private:
 parent_type *m_parent;
};