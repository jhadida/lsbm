
//==================================================
// @title        network.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_ConductanceBased_network::clear()
{
    param.clear();

    t_threads           .clear();
    t_delayed_partition .clear();
    t_local_couplings   .clear();

    c_delay .clear();
    a_isp   .clear();

    parent::clear();
}

// ------------------------------------------------------------------------

bool System_ConductanceBased_network::reset()
{
    DRAYN_ASSERT_RF( param.check(), "[ConductanceBased_network.reset] Invalid parameters." );

    const uidx_t Nt = param.option.num_threads;

    // configure threads
    if ( Nt > 1 )
    {
        t_threads.resize( Nt );
        t_delayed_partition = partition_delayed_edges_by_destination( param, Nt );
    }
    // sort by decreasing delay to optimise memory access pattern on records
    else sort_delayed_edges_decreasing( param );

    // find out indices of local couplings (used by ISP)
    t_local_couplings.resize( 2*param.n_nodes() );
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);
        if ( (edge.src/2) == (edge.dst/2) ) // same unit
            t_local_couplings.at( 2*edge.src + (edge.src != edge.dst) ) = e; // order: wee, wei, wii, wie
    }

    // clear additional memory
    a_isp.clear();
    c_delay.clear();

    return true;
}

// ------------------------------------------------------------------------

void System_ConductanceBased_network::worker_cache( uidx_t start, uidx_t stop ) const
{
    const time_type t  = c_delay.t;
    const time_type dt = c_delay.dt;

    uidx_t i_post, i_pre, post_var;
    for ( uidx_t i = start; i < stop; ++i )
    {
        auto& edge = param.delayed(i);

        i_post = edge.dst; // post-synaptic
        i_pre  = edge.src; // pre-synaptic

        // gE is at 3*i_post+1
        // gI is at 3*i_post+2
        //
        // (i_pre & 1) is 1 if i_pre is odd (ie inhibitory)
        post_var = 3*i_post + 1+(i_pre & 1);

        // save contribution of pre-synaptic connection
        c_delay.left [post_var] += delayed_term( t, edge );
        c_delay.right[post_var] += delayed_term( t+dt, edge );
    }
}

void System_ConductanceBased_network::worker_deriv(
    time_type t, const array_type& dxdt, uidx_t start, uidx_t stop ) const
{
    uidx_t i_post, i_pre, post_var;
    for ( uidx_t i = start; i < stop; ++i )
    {
        auto& edge = param.delayed(i);

        i_post = edge.dst; // post-synaptic
        i_pre  = edge.src; // pre-synaptic

        // gE is at 3*i_post+1
        // gI is at 3*i_post+2
        //
        // (i_pre & 1) is 1 if i_pre is odd (ie inhibitory)
        post_var = 3*i_post + 1+(i_pre & 1);

        // save contribution of pre-synaptic connection
        dxdt[post_var] += delayed_term( t, edge );
    }
}

// ------------------------------------------------------------------------

void System_ConductanceBased_network::_derivative_full( time_type t, const state_type& x, const array_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = param.n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[ConductanceBased_network._derivative_full] Unexpected state size." );

    // initialize dxdt
    dr::fill( dxdt, 0.0 );

    // set delayed contributions
    if (param.option.num_threads > 1)
    {
        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i] = std::thread(
                [this,t,dxdt]( uidx_t b, uidx_t e ){ this->worker_deriv(t,dxdt,b,e); },
                t_delayed_partition[i], t_delayed_partition[i+1]
            );

        for ( uidx_t i = 0; i < param.option.num_threads; ++i )
            t_threads[i].join();
    }
    else worker_deriv( t, dxdt, 0, param.n_delayed() );

    // local variables
    uidx_t i_post, i_pre, post_var;
    double v, gE, gI;

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);

        i_post   = edge.dst;
        i_pre    = edge.src;
        post_var = 3*i_post + 1+(i_pre & 1);

        dxdt[post_var] += edge.coupling * param.node(edge.src).sigm( x[3*edge.src] );
    }

    // iterate on each node
    const auto& model = param.option.model;
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);

        v  = x[3*n + 0];
        gE = x[3*n + 1];
        gI = x[3*n + 2];

        dxdt[3*n + 0] = (
            node.g_leak * (model.R_leak - v)
            + gE        * (model.R_dep  - v)
            + gI        * (model.R_hyp  - v)
            + node.curr(t)
        ) / node.tau_v;
        dxdt[3*n + 1] = ( -gE + dxdt[3*n + 1] + node.stim(t) ) / model.tau_gE;
        dxdt[3*n + 2] = ( -gI + dxdt[3*n + 2] ) / model.tau_gI;
    }
}

// ------------------------------------------------------------------------

void System_ConductanceBased_network::_derivative_cache(
    time_type t, const state_type& x, const array_type& dxdt ) const
{
    const uidx_t nx = x.size();
    const uidx_t nn = param.n_nodes();
    DRAYN_DASSERT_R( nx == ndims(), "[ConductanceBased_network._derivative_cache] Unexpected state size." );

    // Fallback to full method if the cache isn't set (eg at the beginning)
    if ( c_delay.size() != nx )
    {
        DRAYN_INFO("[ConductanceBased_network._derivative_cache] "
            "Cache is empty, fallback to _derivative_full (this is normal for the initial point).");

        _derivative_full(t,x,dxdt); return;
    }

    // initialize dxdt
    dr::fill( dxdt, 0.0 );

    // local variables
    uidx_t i_post, i_pre, post_var;
    double v, gE, gI;

    // set instantaneous contributions
    for ( uidx_t e: param.f_instant )
    {
        auto& edge = param.edge(e);

        i_post   = edge.dst;
        i_pre    = edge.src;
        post_var = 3*i_post + 1+(i_pre & 1);

        dxdt[post_var] += edge.coupling * param.node(edge.src).sigm( x[3*edge.src] );
    }

    // iterate on each node
    const auto& model = param.option.model;
    for ( uidx_t n = 0; n < nn; ++n )
    {
        auto& node = param.node(n);
        
        v  = x[3*n + 0];
        gE = x[3*n + 1];
        gI = x[3*n + 2];

        dxdt[3*n + 0] = (
            node.g_leak * (model.R_leak - v)
            + gE        * (model.R_dep  - v)
            + gI        * (model.R_hyp  - v)
            + node.curr(t)
        ) / node.tau_v;
        dxdt[3*n + 1] = ( -gE + dxdt[3*n + 1] + c_delay(3*n + 1,t) + node.stim(t) ) / model.tau_gE;
        dxdt[3*n + 2] = ( -gI + dxdt[3*n + 2] + c_delay(3*n + 2,t) ) / model.tau_gI;
    }
}
