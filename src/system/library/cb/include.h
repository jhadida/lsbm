
//==================================================
// @title        Conductance-Based Model
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <functional>
#include <thread>
#include <vector>

// ----------  =====  ----------

LSBM_NS_START

#include "param.h"
#include "unit.h"
#include "network.h"

// TODO: fix code with piecewise stimulus
// #include "plugin_mark.h"

LSBM_NS_END
