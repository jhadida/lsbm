
//==================================================
// @title        unit.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_ConductanceBased_unit
    : public NeuronalSystem<false>
{
public:

    using self        = System_ConductanceBased_unit;
    using parent      = NeuronalSystem<false>;
    using param_type  = ConductanceBased_node;
    using model_type  = ConductanceBased_model;

    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;

    // ----------  =====  ----------


    System_ConductanceBased_unit() { clear(); }

    void clear();

    // dimensions
    inline uidx_t n_nodes() const { return 2; }
    inline uidx_t ndims() const { return 3*n_nodes(); } // override DISOL parent

    void derivative( time_type t, const state_type& x, const array_type& dxdt ) const;

    // Implement inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class H>
    void callback_after_commit( H& dat );

    // Matlab stuff
#ifdef LSBM_USE_JMX

    System_ConductanceBased_unit( const jmx::Struct& in )
        { configure(in); }

    bool configure( const jmx::Struct& in );

#endif


    // ----------  =====  ----------

    param_type  E, I;
    model_type  model;

    double wee, wei, wii, wie;
};

// ------------------------------------------------------------------------

template <class H>
void System_ConductanceBased_unit::callback_after_commit( H& dat )
{
    if ( I.is_plastic() )
    {
        const double v_pre  = dat.next.x[3];
        const double v_post = dat.next.x[0];

        const double S_pre  = I.sigm( v_pre  );
        const double S_post = E.sigm( v_post );

        // Backward Euler step
        wie += dat.cur.dt * I.eta*S_pre * ( S_post - I.rho );
        wie  = std::max( wie, 0.0 );
    }
}
