
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



class System_ConductanceBased_network
    : public NeuronalSystem<false>
{
public:

    using self        = System_ConductanceBased_network;
    using parent      = NeuronalSystem<false>;
    using param_type  = ConductanceBased_network;

    using pool_type   = traits::pool;
    using time_type   = parent::time_type;
    using state_type  = parent::state_type;
    using array_type  = parent::array_type;
    using edge_type   = param_type::edge_type;

    // ----------  =====  ----------


    System_ConductanceBased_network() { clear(); }

    void clear();
    bool reset();

    // dimensions
    inline uidx_t n_nodes() const { return param.n_nodes(); }
    inline uidx_t ndims() const { return 3*n_nodes(); } // override DISOL parent

    // used by lsbm_helper
    inline time_type min_delay () const { return param.min_delay(); }
    inline time_type max_delay () const { return param.max_delay(); }


// Mex interface
#ifdef LSBM_USE_JMX

    System_ConductanceBased_network( const jmx::Struct& in )
        { configure(in); }

    inline bool configure( const jmx::Struct& in )
        { return param.extract(in) && reset(); }

    // overload additional outputs
    inline void additional_outputs( jmx::Struct& out ) const {
        if ( param.option.enable_isp )
            disol::jmx_export( a_isp, out.mkstruct("isp") );
    }

#endif


    // Derivative
    inline void derivative( time_type t, const state_type& x, const array_type& dxdt ) const
    {
        if ( param.option.cache_delays )
            _derivative_cache(t,x,dxdt);
        else
            _derivative_full(t,x,dxdt);
    }

    void _derivative_full  ( time_type t, const state_type& x, const array_type& dxdt ) const;
    void _derivative_cache ( time_type t, const state_type& x, const array_type& dxdt ) const;


    // Initialise additional outputs
    // bind to event: after_init
    template <class H>
    void callback_after_init( H& dat );

    // Cache delayed firing-rates for faster multi-step integration
    // bind to event: before_step
    template <class H>
    void callback_before_step( H& dat );

    // Implement inhibitory synaptic plasticity
    // bind to event: after_commit
    template <class H>
    void callback_after_commit( H& dat );


    // ----------  =====  ----------

    param_type   param;

private:

    DelayCache<double>       c_delay;
    dr::shared<std::thread>  t_threads;
    dr::shared<uidx_t>       t_delayed_partition, t_local_couplings;
    pool_type                a_isp;


    // ----------  =====  ----------
    // DELAYED TERM COMPUTATION

    // form of delayed terms for the conductance-based network model
    inline double delayed_term( time_type t, const edge_type& edge ) const {
        const double fac = this->induction(t);
        return fac * edge.coupling * param.node(edge.src).sigm( // delayed pre-synaptic potential
                this->delayed_value( t-edge.delay, 3*edge.src )
            );
    }

    // multi-thread workers
    void worker_cache( uidx_t start, uidx_t stop ) const;
    void worker_deriv( time_type t, const array_type& dxdt, uidx_t start, uidx_t stop ) const;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



/*

Note on indexing:

If the network contains N neuronal masses, then the network matrix is 2N * 2N.
This is because each neuronal mass has both excitatory and inhibitory subpopulations.
Each subpopulation, whether E or I, is called a "node".

Node indexing is such that:
    - nodes { 2*k, k = 0 .. (N-1) } are excitatory, and
    - nodes { 2*k+1, k = 0 .. (N-1) } are inhibitory.

Each "node" is described by 3 state variables in the conductance-based model:
    - a potential V,
    - an excitatory conductance gE
    - an inhibitory conductance gI

Therefore, the state-variable indexing is such that for node k:
    - state-variable 3*k   corresponds to  V_k
    -       "        3*k+1         "      gE_k
    -       "        3*k+2         "      gI_k

 */

template <class H>
void System_ConductanceBased_network::callback_after_init( H& dat )
{
    if ( param.option.enable_isp )
    {
        a_isp.init( n_nodes()/2, this->records.bsize(), dat.fix );
        a_isp.time() = dat.cur.t;
        for ( uidx_t i = 1; i < n_nodes(); i += 2 ) // iterate over inhibitory nodes
            a_isp.value(i/2) = param.edge(t_local_couplings[ 2*i + 1 ]).coupling; // wie
    }
}

// ------------------------------------------------------------------------

template <class H>
void System_ConductanceBased_network::callback_before_step( H& dat )
{
    // current time and time-step
    const time_type t  = dat.cur.t;
    const time_type dt = dat.cur.dt;

    // delay caching
    if ( param.option.cache_delays )
    {
        c_delay.reset( dat.size(), t, dt );

        // iterate on compressed storage
        if (param.option.num_threads > 1)
        {
            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i] = std::thread(
                    [this]( uidx_t b, uidx_t e ){ this->worker_cache(b,e); },
                    t_delayed_partition[i], t_delayed_partition[i+1]
                );

            for ( uidx_t i = 0; i < param.option.num_threads; ++i )
                t_threads[i].join();
        }
        else worker_cache( 0, param.n_delayed() );
    }
}

// ------------------------------------------------------------------------

template <class H>
void System_ConductanceBased_network::callback_after_commit( H& dat )
{
    double v_post, v_pre;
    double S_post, S_pre;

    if ( param.option.enable_isp )
    {
        a_isp.increment();

        for ( uidx_t i = 1; i < n_nodes(); i += 2 ) // iterate over inhibitory nodes
        {
            auto& Enode = param.node(i-1); // "post-synaptic" excitatory population
            auto& Inode = param.node(i); // "pre-synaptic"  inhibitory population
            auto& ItoE  = param.edge(t_local_couplings[ 2*i + 1 ]); // inhibitory -> excitatory

            if ( Inode.is_plastic() )
            {
                // using "cur" because this is called after commit by the integrator
                v_pre  = dat.cur.x[ 3*i     ]; // inhibitory
                v_post = dat.cur.x[ 3*(i-1) ]; // excitatory

                // corresponding normalised firing rates
                S_post = Enode.sigm( v_post );
                S_pre  = Inode.sigm( v_pre  );

                // Backward Euler step
                ItoE.coupling += dat.cur.dt * Inode.eta*S_pre * ( S_post - Inode.rho );
                ItoE.coupling  = std::max( ItoE.coupling, 0.0 ); // make sure I doesn't become excitatory
            }

            // save coupling value
            a_isp.time() = dat.cur.t;
            a_isp.value(i/2) = ItoE.coupling;
        }
    }
}
