
//==================================================
// @title        unit.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

void System_ConductanceBased_unit::clear()
{
    E.clear();
    I.clear();

    wee = wei = wii = wie = 0.0; // coupling coefficients
    parent::clear();
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool System_ConductanceBased_unit::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({
        "wee", "wei", "wii", "wie", "model", "E", "I"
    }), "Missing field(s)." );

    wee = in.getnum("wee");
    wei = in.getnum("wei");
    wii = in.getnum("wii");
    wie = in.getnum("wie");

    DRAYN_ASSERT_RF( model.extract(in["model"]), "Couldn't extract model parameters." )
    DRAYN_ASSERT_RF( E.extract(in["E"]), "Couldn't extract excitatory params." )
    DRAYN_ASSERT_RF( I.extract(in["I"]), "Couldn't extract inhibitory params." )

    return true;
}
#endif

// ------------------------------------------------------------------------

void System_ConductanceBased_unit::derivative( time_type t, const state_type& x, const array_type& dxdt ) const
{
    const double ve  = x[0];
    const double gee = x[1];
    const double gie = x[2];

    const double vi  = x[3];
    const double gei = x[4];
    const double gii = x[5];

    const double Se  = E.sigm(ve);
    const double Si  = I.sigm(vi);

    const double Pe  = E.stim(t);
    const double Pi  = I.stim(t);

    const double Ce  = E.curr(t);
    const double Ci  = I.curr(t);

    // Potentials
    dxdt[0] = (
          E.g_leak * (model.R_leak - ve)
        + gee      * (model.R_dep  - ve)
        + gie      * (model.R_hyp  - ve)
        + Ce
    ) / E.tau_v;

    dxdt[3] = (
        I.g_leak   * (model.R_leak - vi)
        + gei      * (model.R_dep  - vi)
        + gii      * (model.R_hyp  - vi)
        + Ci
    ) / I.tau_v;

    // Conductances
    dxdt[1] = ( Pe + wee*Se - gee ) / model.tau_gE;
    dxdt[2] = (      wie*Si - gie ) / model.tau_gI;

    dxdt[4] = ( Pi + wei*Se - gei ) / model.tau_gE;
    dxdt[5] = (      wii*Si - gii ) / model.tau_gI;
}
