
//==================================================
// @title        param.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



struct ConductanceBased_model
{
    // Characteristic synaptic times
    double tau_gE;
    double tau_gI;

    // Reversal potentials
    double R_leak;
    double R_hyp; // hyperpolarising (inhibitory)
    double R_dep; // depolarising (excitatory)

    // ----------  =====  ----------

    ConductanceBased_model():
        tau_gE ( 5.0e-3),
        tau_gI (10.0e-3),
        R_leak (-60.0),
        R_hyp  (-80.0),
        R_dep  (  0.0)
    {}

    bool check() const;

    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct ConductanceBased_options
    : public default_options
{
    ConductanceBased_model model;

    bool enable_isp;

    // ----------  =====  ----------

    ConductanceBased_options() { clear(); }

    inline void clear() { 
        default_options::clear(); 
        enable_isp = false; 
    }

    void display() const;

    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct ConductanceBased_node
{
    sigmoid_ptr S;
    stimulus_ptr C, P;

    double tau_v, g_leak;
    double eta, rho;

    // ----------  =====  ----------

    ConductanceBased_node() { clear(); }

    void clear();
    bool check() const;

    inline bool is_plastic() const { return eta > 0.0; }

    inline double sigm( double v ) const { return S->val(v); }
    inline double stim( double t ) const { return (*P)(t); }
    inline double curr( double t ) const { return (*C)(t); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct ConductanceBased_network
    : public TemplateNetwork< ConductanceBased_node, default_edge, ConductanceBased_options >
{};
