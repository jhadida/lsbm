
//==================================================
// @title        plugin_mark.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Quick hack to reproduce Mark's experiment.
 * Bind to event: after_commit
 *
 * This will initialize an auxiliary mempool (isp_data) to save firing rates and synaptic plasticities (cf init_pool).
 * The callback function is used to save these values after each step (cf isp_save),
 * and to update the excitatory self-loop or synaptic learning rate (cf wee_vals, eta_vals).
 */

template <class H>
struct Plugin_ConductanceBased_Mark
    : public disol::plugin<H>
{
public:

    using self        = Plugin_ConductanceBased_Mark<H>;
    using parent      = disol::plugin<H>;
    using handle_type = H;
    using pool_type   = traits::pool;
    using signal_type = typename parent::signal_type;
    using system_type = typename H::system_type;
    using time_type   = typename H::time_type;
    using value_type  = typename H::value_type;


    Plugin_ConductanceBased_Mark() {}
    Plugin_ConductanceBased_Mark( signal_type& sig )
        { this->attach(sig); }


    // auxiliary pool for syn. plasticities and firing rates
    pool_type isp_data;
    bool init_pool( handle_type& dat )
    {
        DRAYN_DASSERT_RF( dat.valid(), "Invalid dat." );
        system_type& sys = *dat.sys;

        // save initial dat
        isp_data.init( 4, sys.records.bsize(), sys.records.is_fixed_step() );
        isp_save( dat );

        return true;
    }

    // vary excitatory self-loop and learning rate
    Stimulus_piecewise wee_vals, eta_vals;


protected:

    // bind to event: after_commit
    void callback( handle_type& dat )
    {
        DRAYN_DASSERT_R( dat.valid(), "Invalid dat." );
        system_type& sys = *dat.sys;

        // Run ISP
        sys.callback_after_commit(dat);

        // Set current isp data
        isp_data.increment();
        isp_save( dat );

        // Update wee/eta values
        if ( !wee_vals.empty() )
            sys.wee = wee_vals(dat.next.t);

        if ( !eta_vals.empty() )
            sys.I.eta = eta_vals(dat.next.t);
    }

    void isp_save( handle_type& dat )
    {
        DRAYN_DASSERT_R( dat.valid(), "Invalid dat." );
        system_type& sys = *dat.sys;

        // Update time
        isp_data.time() = sys.records.time();

        // Current states
        auto isp_state  = isp_data.state();
        auto sys_state  = sys.records.state();

        // Synaptic weights
        isp_state[0] = sys.wee;
        isp_state[1] = sys.wie;

        // Firing rates (Hz)
        isp_state[2] = sys.E.sigm( sys_state[0] );
        isp_state[3] = sys.I.sigm( sys_state[3] );
    }
};
