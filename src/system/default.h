
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

/**
 * Typical options used for the simulation of neuronal systems.
 */
struct default_options
{
    bool    cache_delays;
    uidx_t  num_threads;

    // ----------  =====  ----------

    default_options() { clear(); }

    inline void clear()
        { cache_delays = false; num_threads = 1; }
    inline bool check() const
        { return num_threads > 0; }

    // Print options to stdout
    void display() const;

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

/**
 * Default edges store a source, a destination, a coupling strength and a delay.
 */
struct default_edge
{
    uidx_t src, dst;
    double delay, coupling;

    // ----------  =====  ----------

    default_edge() { clear(); }

    inline void clear()
        { delay = coupling = 0.0; src = dst = 0; }
    inline bool check() const
        { return delay >= 0.0; }

    // Print to stdout
    inline void display() const
        { drayn_print( "(%u,%u) = {delay=%.2f,coupling=%.2f}", src, dst, delay, coupling ); }

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif
};

LSBM_NS_END
