
//==================================================
// @title        delay_coupling.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

void default_options::display() const
{
    const char* b2s[] = { "false", "true" };
    drayn_print( "[default_options] Selected options are:" );
    drayn_print( " - cache delays : %s", b2s[cache_delays] );
    drayn_print( " - num threads  : " uidx_f "\n", num_threads );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX

bool default_options::extract( const jmx::Struct& in )
{
    // When true, cache delayed terms during a step for faster computation of the derivative.
    // Optimized version : linear in search time (one quadratic update required per step)
    // Non-optimized     : quadratic in search time for each call
    cache_delays = in.getbool("cache_delays",false);

    // Threads are used with the option 'cache_delays'; several threads are started in order
    // to compute cached values for delayed terms at each step.
    num_threads = in.getnum<uidx_t>("num_threads",1);

    return check();
}

bool default_edge::extract( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({"src","dst","delay","coupling"}),
        "[default_edge.extract] Missing field(s)." );

    src = in.getnum<uidx_t>("src");
    dst = in.getnum<uidx_t>("dst");
    delay = in.getnum("delay");
    coupling = in.getnum("coupling");

    DRAYN_ASSERT_RF( src*dst > 0, "[default_edge.extract] Indices should be positive." );
    --src; --dst; // Matlab indexing begins at 1

    DRAYN_ASSERT( dr::op_abs(coupling) > dr::c_num<double>::eps,
        "[default_edge.extract] Very small coupling magnitude, consider neglecting weak edges for speed." );

    return check();
}

#endif

LSBM_NS_END
