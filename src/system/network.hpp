
//==================================================
// @title        network.hpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class N, class E, class O>
void TemplateNetwork<N,E,O>::clear()
{
    nodes.clear();
    edges.clear();

    option.clear();
    f_instant.clear();
    f_delayed.clear();
}

// ------------------------------------------------------------------------

template <class N, class E, class O>
double TemplateNetwork<N,E,O>::min_delay() const
{
    const double eps = dr::c_num<double>::eps;
    double md = dr::c_num<double>::max;
    for ( uidx_t i=0; i < n_delayed(); ++i )
        if ( delayed(i).delay > eps && delayed(i).delay < md )
            md = delayed(i).delay;

    return md;
}

template <class N, class E, class O>
double TemplateNetwork<N,E,O>::max_delay() const
{
    double md = 0.0;
    for ( uidx_t i=0; i < n_delayed(); ++i )
        if ( delayed(i).delay > md )
            md = delayed(i).delay;

    return md;
}

// ------------------------------------------------------------------------

template <class N, class E, class O>
bool TemplateNetwork<N,E,O>::check() const
{
    // Check each node
    for ( auto& node: nodes )
        DRAYN_ASSERT_RF( node.check(), "[TemplateNetwork.check] Invalid node." )
    
    for ( auto& edge: edges )
        DRAYN_ASSERT_RF( edge.check(), "[TemplateNetwork.check] Invalid edge." )

    return option.check();
}

// ------------------------------------------------------------------------

template <class N, class E, class O>
void TemplateNetwork<N,E,O>::display() const
{
    drayn_print("[TemplateNetwork] Network with " uidx_f " nodes, and " uidx_f " edges:", n_nodes(), n_edges() );
    for ( auto& edge : edges ) edge.display();
}

// ------------------------------------------------------------------------

template <class N, class E, class O>
void TemplateNetwork<N,E,O>::update_filters()
{
    const double eps = dr::c_num<double>::eps;
    f_instant.clear();
    f_delayed.clear();

    for ( uidx_t e = 0; e < n_edges(); ++e )
    {
        if ( edge(e).delay > eps )
            f_delayed.push_back(e);
        else
            f_instant.push_back(e);
    }
}

// ------------------------------------------------------------------------
#ifdef LSBM_USE_JMX

template <class N, class E, class O>
bool TemplateNetwork<N,E,O>::extract( const jmx::Struct& in )
{
    const double eps = dr::c_num<double>::eps;
    DRAYN_ASSERT_RF( in.has_fields({ "nodes", "edges", "options" }), 
        "[TemplateNetwork.extract] Missing field(s)." )

    clear();

    //-------------------------------------------------------------
    // 1. Extract options
    //-------------------------------------------------------------

    DRAYN_ASSERT_RF( option.extract(in.getstruct("options")), "[TemplateNetwork.extract] Couldn't extract options." )


    //-------------------------------------------------------------
    // 2. Extract nodes
    //-------------------------------------------------------------

    auto in_nodes = in.getstruct("nodes");
    const uidx_t Nnodes = in_nodes.numel();
    DRAYN_REJECT_RF( Nnodes < 2, "[TemplateNetwork.extract] Number of nodes shouldn't be less than 2." )

    nodes.resize( Nnodes );
    for ( uidx_t i=0; i < Nnodes; ++i )
        DRAYN_ASSERT_RF( nodes[i].extract(in_nodes.select(i)),
            "[TemplateNetwork.extract] Couldn't extract node " uidx_f ".", i )


    //-------------------------------------------------------------
    // 3. Extract edges
    //-------------------------------------------------------------

    auto in_edges = in.getstruct("edges");
    const uidx_t Nedges = in_edges.numel();
    
    edges.resize( Nedges );
    for ( uidx_t i=0; i < Nedges; ++i )
    {
        DRAYN_ASSERT_RF( edges[i].extract(in_edges.select(i)),
            "[TemplateNetwork.extract] Couldn't extract edge " uidx_f ".", i )

        if ( edges[i].delay > eps )
            f_delayed.push_back(i);
        else
            f_instant.push_back(i);
    }
    

    //-------------------------------------------------------------
    // 4. Show information
    //-------------------------------------------------------------

    print_info( 
        "[TemplateNetwork] Parsed " uidx_f " nodes and " uidx_f " edges (" uidx_f " with delay).", 
        n_nodes(), n_edges(), n_delayed() );

    if ( verbosity() >= verbose_level::Info ) option.display();

    return true;
}

#endif
