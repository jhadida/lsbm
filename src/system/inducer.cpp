
//==================================================
// @title        inducer.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

inducer_ptr inducer_factory( const std::string& name, double onset, double length )
{
    static const Inducer_mapping mapping;
    switch( mapping.get_id(name) )
    {
        case inducer_id::Step:
            return make_inducer<Activation_step>(onset,length);

        case inducer_id::Linear:
            return make_inducer<Activation_linear>(onset,length);

        case inducer_id::Smoothstep:
            return make_inducer<Activation_smoothstep<1>>(onset,length);
        case inducer_id::Smoothstep2:
            return make_inducer<Activation_smoothstep<2>>(onset,length);
        case inducer_id::Smoothstep3:
            return make_inducer<Activation_smoothstep<3>>(onset,length);
        case inducer_id::Smoothstep4:
            return make_inducer<Activation_smoothstep<4>>(onset,length);

        default:
            DRAYN_WARN( "Unknown inducer: %s", name.c_str() )
            return inducer_ptr();
    }
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX

inducer_ptr inducer_factory( const jmx::Struct& cfg )
{
    inducer_ptr a;

    DRAYN_ASSERT_ERR( cfg.has_field("name"), "Missing field 'name'." )
    a = inducer_factory( 
        cfg.getstr("name"), 
        cfg.getnum("onset",0.0), cfg.getnum("length",1.0) 
    );
    DRAYN_ASSERT_ERR( a, "Could not build or configure inducer." )

    return a;
}

#endif

LSBM_NS_END
