
//==================================================
// @title        network.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <vector>
#include <algorithm>
#include <functional>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

/**
 * Template class holding the information necessary for simulating any neuronal system.
 */

template <class NodeType, class EdgeType = default_edge, class Options = default_options>
struct TemplateNetwork
{
    using node_type = NodeType;
    using edge_type = EdgeType;
    using options_type = Options;

    using nodes_vec = std::vector<node_type>;  // array of nodes
    using edges_vec = std::vector<edge_type>;  // array of edges
    using filter_type = std::vector<uidx_t>;

    nodes_vec  nodes;
    edges_vec  edges;

    options_type  option;
    filter_type   f_delayed, f_instant;

    // ----------  =====  ----------

    TemplateNetwork() { clear(); }

    void clear();
    bool check() const;

    // Dimensions
    inline uidx_t n_nodes () const { return nodes.size(); }
    inline uidx_t n_edges () const { return edges.size(); }

    // Filter sizes
    inline uidx_t n_delayed () const { return f_delayed.size(); }
    inline uidx_t n_instant () const { return f_instant.size(); }

    // Access elements
    inline node_type& node ( uidx_t k ) { return nodes[k]; }
    inline edge_type& edge ( uidx_t k ) { return edges[k]; }

    inline const node_type& node ( uidx_t k ) const { return nodes[k]; }
    inline const edge_type& edge ( uidx_t k ) const { return edges[k]; }

    // Access edges through filters
    inline edge_type& delayed ( uidx_t k ) { return edges[f_delayed[k]]; }
    inline edge_type& instant ( uidx_t k ) { return edges[f_instant[k]]; }

    inline const edge_type& delayed ( uidx_t k ) const { return edges[f_delayed[k]]; }
    inline const edge_type& instant ( uidx_t k ) const { return edges[f_instant[k]]; }

    // Display network
    void display() const;

    // Delay bounds
    double min_delay() const;
    double max_delay() const;

    // Update edge filters
    void update_filters();

    // Assign from Matlab input
    #ifdef LSBM_USE_JMX
    bool extract( const jmx::Struct& in );
    #endif

};

// Include implementations
#include "network.hpp"

// ------------------------------------------------------------------------

/**
 * Sort delayed edges.
 */
template <class Network>
void sort_delayed_edges_increasing( Network& net ) {
    std::sort( net.f_delayed.begin(), net.f_delayed.end(),
        [ &net ]( uidx_t a, uidx_t b ){ return net.edge(a).delay < net.edge(b).delay; }
    );
}

template <class Network>
void sort_delayed_edges_decreasing( Network& net ) {
    std::sort( net.f_delayed.begin(), net.f_delayed.end(),
        [ &net ]( uidx_t a, uidx_t b ){ return net.edge(a).delay > net.edge(b).delay; }
    );
}

// ------------------------------------------------------------------------

/**
 * Partition delayed edges by destination.
 * This is used to get lock-free multithreaded code for the computation of delayed terms.
 */
template <class Network>
dr::svec<uidx_t> partition_delayed_edges_by_destination( Network& net, const uidx_t N )
{
    // sort delayed edges by destination
    // this is done indirectly by sorting the associated filter
    std::sort( net.f_delayed.begin(), net.f_delayed.end(),
        [ &net ]( uidx_t a, uidx_t b ){ return net.edge(a).dst < net.edge(b).dst; }
    );

    // split the delayed edges into N equal bins
    auto partition = dr::linspace<uidx_t>( 0, net.n_delayed(), N+1 );

    // adjust strides to make sure all edges with a given destination are contained within at most
    // one bin, in order to avoid concurrent writing to the corresponding delayed term
    for ( uidx_t i=1; i < N; ++i ) {
        uidx_t& p = partition[i];
        while ( (p > partition[i-1]) && (net.delayed(p).dst == net.delayed(p-1).dst) ) --p;
    }

    return partition;
}

LSBM_NS_END
