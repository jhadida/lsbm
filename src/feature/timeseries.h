
//==================================================
// @title        timeseries.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * TODO:
 * This is a quick hack and might be entirely changed in the future.
 * Currently only works for evenly-sampled solutions (ie arithmetic time-sampling).
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct TimeSeries
{
    typedef TimeSeries self;


    template <class S>
    bool parse_system( const S& sys );

    template <class T>
    bool parse_mx_output( const T& data );


    inline size_t nt() const { return m_times  .n_elem; }
    inline size_t nv() const { return m_values .n_cols; }

    inline double tspan() const { return m_times(nt()-1) - m_times(0); }
    inline double tstep() const { return m_times(1) - m_times(0); }

    // Signal
    arma::mat m_values;
    arma::vec m_times;

    // Fourier & Hilbert transforms
    arma::vec m_f_frq;
    arma::mat m_f_amp, m_f_ph;
    arma::mat m_h_frq, m_h_ph;
    arma::cx_mat m_h_sig;
};



        /********************     **********     ********************/
        /********************     **********     ********************/



template <class S>
bool TimeSeries::parse_system( const S& sys )
{
    uidx_t ncols = sys.ndims();
    uidx_t nrows = sys.n_states();

    m_times  .set_size( nrows );
    m_values .set_size( nrows, ncols );

    auto times  = dr::arma2dr<double>::reference( m_times );
    auto values = dr::arma2dr<double>::reference( m_values );
    disol::pool_export( sys.records, times, values, true );

    return true;
}

// ------------------------------------------------------------------------

template <class T>
bool TimeSeries::parse_mx_output( const T& data )
{
    m_times  = dr::dr2arma_col<double>( data.times  .get(), true );
    m_values = dr::dr2arma_mat<double>( data.states .get(), true );
}

LSBM_NS_END
