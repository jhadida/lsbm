
//==================================================
// @title        transforms.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

void transform_fourier( TimeSeries& ts )
{
    typedef uint64_t Uint;

    Uint m     = dr::bit_nextpow2( ts.nt() );
    Uint v     = ts.nv();
    Uint r     = m/2;
    double dt  = ts.tstep();
    double fs  = 1.0 / dt;

    DRAYN_ASSERT_R( r > 1, 
        "[lsbm.transform_fourier] Not enough values." );
    DRAYN_ASSERT_R( dt > dr::c_num<double>::eps, 
        "[lsbm.transform_fourier] Timestep is too small." );

    // Set frequencies (half-spectrum because input is real)
    ts.m_f_frq.set_size( r );
    for ( Uint i = 0; i < r; ++i )
        ts.m_f_frq[i] = i * (fs / m);

    // Compute Fourier transform
    arma::cx_mat spectra = arma::fft( ts.m_values, m );

    ts.m_f_amp = arma::abs( spectra.rows(0,r-1) );
    ts.m_f_ph  = transform_util_angle( spectra.rows(0,r-1) );

    // Remove DC component
    for ( Uint i = 0; i < v; ++i ) 
        ts.m_f_amp(0,i) = 0.0;
}

// ------------------------------------------------------------------------

void transform_hilbert( TimeSeries& ts )
{
    typedef uint64_t Uint;

    Uint n     = ts.nt();
    Uint v     = ts.nv();
    double dt  = ts.tstep();
    double fs  = 1.0 / dt;

    DRAYN_ASSERT_R( n > 1, 
        "[lsbm.transform_hilbert] Not enough timepoints." );
    DRAYN_ASSERT_R( dt > dr::c_num<double>::eps, 
        "[lsbm.transform_hilbert] Timestep is too small." );

    // Weight vector
    arma::vec h = arma::zeros(n);
    for ( Uint i = 1; i <= (n-1)/2; ++i ) h(i) = 2;
    h(0) = 1; if ( (n % 2) == 0 ) h(n/2) = 1;

    arma::cx_mat F = arma::fft( ts.m_values, n );

    for ( Uint ic = 0; ic < v; ++ic )
    for ( Uint ir = 0; ir < n; ++ir )
    {
        auto f_tmp = F(ir,ic);
        F(ir,ic)   = std::complex<double>( h[ir]*f_tmp.real(), h[ir]*f_tmp.imag() );
    }

    ts.m_h_sig = arma::ifft( F );
    ts.m_h_ph  = transform_util_angle( ts.m_h_sig );
    ts.m_h_frq = (fs*0.5*C_1_PI) * transform_util_deriv( ts.m_h_ph, dt );
}



        /********************     **********     ********************/
        /********************     **********     ********************/



arma::mat transform_util_angle( const arma::cx_mat& spectra )
{
    typedef uint64_t Uint;

    Uint nr = spectra.n_rows;
    Uint nc = spectra.n_cols;

    arma::mat angles( nr, nc );

    for ( Uint ic = 0; ic < nc; ++ic )
    for ( Uint ir = 0; ir < nr; ++ir )
        angles(ir,ic) = std::arg(spectra(ir,ic));

    transform_util_unwrap(angles);
    return angles;
}

// ------------------------------------------------------------------------

void transform_util_unwrap( arma::mat& angles )
{
    static dr::fp_close<double> fp_compare;
    typedef uint64_t Uint;

    DRAYN_ASSERT_R( angles.n_elem > 0, 
        "[lsbm.transform_util_unwrap] angles matrix is empty." );

    Uint nr = angles.n_rows;
    Uint nc = angles.n_cols;

    // Rescale in [-pi,pi)
    auto rescale = []( double x ) -> double 
        { return dr::op_mod( x+C_PI, 2*C_PI ) - C_PI; };

    // Phase cumulative correction
    arma::vec cor(nr-1);

    for ( Uint ic = 0; ic < nc; ++ic )
    {
        for ( Uint ir = 1; ir < nr; ++ir )
        {
            double da = angles(ir,ic) - angles(ir-1,ic);
            cor(ir-1) = 0.0;

            if ( dr::op_abs(da) >= C_PI )
            {
                double das = rescale(da); 
                if ( fp_compare(das,-C_PI) && da > 0.0 ) das = C_PI;
                cor(ir-1) = das - da;
            }
        }

        angles.col(ic).rows(1,nr-1) += arma::cumsum(cor);
    }
}

// ------------------------------------------------------------------------

arma::mat transform_util_deriv( const arma::mat& vals, double dt )
{
    typedef uint64_t Uint;

    Uint nr = vals.n_rows;
    Uint nc = vals.n_cols;

    DRAYN_ASSERT_RV( dt > dr::c_num<double>::eps, vals,
        "[lsbm.transform_util_deriv] Timestep is too small." );
    DRAYN_ASSERT_RV( nr > 2 && nc > 0, vals,
        "[lsbm.transform_util_deriv] Bad input size." );

    arma::mat deriv( nr, nc );

    // Central approx
    deriv.rows( 1, nr-2 ) = ( vals.rows(2,nr-1) - vals.rows(0,nr-3) ) / (2*dt);

    // Forward approx for first row
    deriv.row( 0 ) = ( vals.row(1) - vals.row(0) ) / dt;

    // Backward approx for last row
    deriv.row(nr-1) = ( vals.row(nr-1) - vals.row(nr-2) ) / dt;

    return deriv;
}

LSBM_NS_END
