
//==================================================
// @title        transforms.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <complex>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

// Utilities requires for the transforms
void      transform_util_unwrap ( arma::mat& angles );
arma::mat transform_util_angle  ( const arma::cx_mat& spectra );
arma::mat transform_util_deriv  ( const arma::mat& vals, double dt );


// Fourier and Hilbert transforms
void transform_fourier( TimeSeries& ts );
void transform_hilbert( TimeSeries& ts );

LSBM_NS_END
