
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>

        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param
{
    inline bool check() const { return true; }
    
    #ifdef LSBM_USE_JMX
    virtual bool configure( const jmx::Struct& ) =0;
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_abstract
{
    using time_type = traits::time;

    virtual bool check() const =0;
    virtual double operator() ( double ) const =0;
    inline double val( time_type t ) const { return operator()(t); }

    #ifdef LSBM_USE_JMX
    virtual bool configure( const jmx::Struct& ) =0;
    #endif
};

// ------------------------------------------------------------------------

template <class Parameters>
struct Stimulus: public Stimulus_abstract
{
    Parameters param;

    virtual ~Stimulus() {} // -Wdelete-non-virtual-dtor

    inline bool check() const 
        { return param.check(); }

    inline bool configure( const Parameters& p )
        { param = p; return check(); }

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& in )
        { return param.configure(in); }
    #endif
};

LSBM_NS_END
