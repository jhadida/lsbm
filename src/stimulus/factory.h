
//==================================================
// @title        factory.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>
#include <memory>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

enum class stimulus_id
{
    Constant,
    Heaviside,
    Impulse,
    Harmonic,
    Piecewise,
    TimeCourse
};

// ------------------------------------------------------------------------

struct Stimulus_mapping: public nmap<stimulus_id>
{
    Stimulus_mapping()
    {
        this->define( "constant",      stimulus_id::Constant );
        this->define( "const",         stimulus_id::Constant );
        //
        this->define( "heaviside",     stimulus_id::Heaviside );
        this->define( "step",          stimulus_id::Heaviside );
        //
        this->define( "impulse",       stimulus_id::Impulse );
        this->define( "bump",          stimulus_id::Impulse );
        //
        this->define( "harmonic",      stimulus_id::Harmonic );
        this->define( "sine",          stimulus_id::Harmonic );
        this->define( "wave",          stimulus_id::Harmonic );
        //
        this->define( "piecewise",     stimulus_id::Piecewise );
        //
        this->define( "timecourse",    stimulus_id::TimeCourse );
        this->define( "tc",            stimulus_id::TimeCourse );
    }
};

// ------------------------------------------------------------------------

// Self-managed stimulus pointer
typedef std::shared_ptr<Stimulus_abstract> stimulus_ptr;

stimulus_ptr stimulus_factory( const std::string& name );

template <class P>
stimulus_ptr make_stimulus( const std::string& name, const P& param ) {
    auto x = stimulus_factory(name);
    dynamic_cast< Stimulus<P>& >(*x).param = param;
    return x;
}

#ifdef LSBM_USE_JMX
stimulus_ptr stimulus_factory( const jmx::Struct& cfg );
#endif

LSBM_NS_END
