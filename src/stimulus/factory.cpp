
//==================================================
// @title        factory.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

stimulus_ptr stimulus_factory( const std::string& name )
{
    static const Stimulus_mapping mapping;
    switch( mapping.get_id(name) )
    {
        case stimulus_id::Constant:
            return stimulus_ptr( new Stimulus_constant() );

        case stimulus_id::Heaviside:
            return stimulus_ptr( new Stimulus_heaviside() );

        case stimulus_id::Piecewise:
            return stimulus_ptr( new Stimulus_piecewise() );

        case stimulus_id::Impulse:
            return stimulus_ptr( new Stimulus_impulse() );

        case stimulus_id::Harmonic:
            return stimulus_ptr( new Stimulus_harmonic() );

        case stimulus_id::TimeCourse:
            return stimulus_ptr( new Stimulus_timecourse() );

        default:
            DRAYN_WARN( "Unknown stimulus: %s", name.c_str() )
            return stimulus_ptr();
    }
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX

stimulus_ptr stimulus_factory( const jmx::Struct& cfg )
{
    stimulus_ptr s;

    DRAYN_ASSERT_ERR( cfg.has_field("name"), "Missing field 'name'." )
    s = stimulus_factory(cfg.getstr("name"));
    DRAYN_ASSERT_ERR( s && s->configure(cfg), "Could not build or configure stimulus." )

    return s;
}

#endif

LSBM_NS_END
