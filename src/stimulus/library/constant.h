
//==================================================
// @title        constant.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param_constant: public Stimulus_param
{
    double value;

    Stimulus_param_constant( double v=1.0 )
        : value(v) {}

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_constant: public Stimulus<Stimulus_param_constant>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_constant;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const
        { return param.value; }
};

LSBM_NS_END
