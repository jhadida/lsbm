
//==================================================
// @title        harmonic.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param_harmonic: public Stimulus_param
{
    using time_type = traits::time;
    
    time_type  freq, phi;
    double     base, amp;

    Stimulus_param_harmonic( time_type f=1.0, double a=1.0, time_type p=0.0, double b=0.0 )
        : freq(f), amp(a), phi(p), base(b) {}

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_harmonic: public Stimulus<Stimulus_param_harmonic>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_harmonic;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const { 
        t = dr::math::Tau*param.freq*t + param.phi;
        return param.base + param.amp * sin(t);
    }
};

LSBM_NS_END
