
//==================================================
// @title        heaviside.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param_heaviside: public Stimulus_param
{
    using time_type = traits::time;
    
    time_type  onset;
    double     value;

    Stimulus_param_heaviside( time_type t=0.0, double v=1.0 )
        : onset(t), value(v) {}

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_heaviside: public Stimulus<Stimulus_param_heaviside>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_heaviside;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const { 
        return t >= param.onset ? param.value : 0.0;
    }
};

LSBM_NS_END
