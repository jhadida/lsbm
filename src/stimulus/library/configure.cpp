
//==================================================
// @title        harmonic.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START
#ifdef LSBM_USE_JMX

bool Stimulus_param_constant::configure( const jmx::Struct& in )
{
    value = in.getnum("value",1.0);
    return check();
}

// ------------------------------------------------------------------------

bool Stimulus_param_heaviside::configure( const jmx::Struct& in )
{
    onset = in.getnum("onset",0.0);
    value = in.getnum("value",1.0);

    return check();
}

// ------------------------------------------------------------------------

bool Stimulus_param_impulse::configure( const jmx::Struct& in )
{
    loc = in.getnum("loc",0.0);
    width = in.getnum("width",1.0);
    amp = in.getnum("amp",1.0);

    return check();
}

// ------------------------------------------------------------------------

bool Stimulus_param_harmonic::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_field("freq"), "Missing field 'freq'." );

    freq = in.getnum("freq");
    base = in.getnum("base",0.0);
    amp = in.getnum("amp",1.0);
    phi = in.getnum("phi",0.0);

    return check();
}

// ------------------------------------------------------------------------

bool Stimulus_param_piecewise::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_ERR( in.has_field("stepmat"), "Missing field 'stepmat'." );

    init = in.getnum("init",0.0);
    auto stepmat = in.getmat("stepmat");
    DRAYN_ASSERT_RF( stepmat.ncols() == 2, "Steps matrix should be nx2." );

    // Allocate local array of steps
    const uidx_t n = stepmat.nrows();
    steps.resize(n);

    for ( uidx_t i=0; i < n; ++i ) {
        steps[i].onset = stepmat(i,0);
        steps[i].value = stepmat(i,1);
    }

    return check();
}

#endif
LSBM_NS_END
