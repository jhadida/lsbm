
//==================================================
// @title        piecewise.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <vector>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_piecewise_step: 
    public comparable<traits::time, traits::time>
{
    using time_type = traits::time;
    using value_type = traits::value;
    using self = Stimulus_piecewise_step;

    time_type onset;
    value_type value;

    Stimulus_piecewise_step( time_type t=0.0, value_type v=0.0 )
        : onset(t), value(v) {}

    virtual ~Stimulus_piecewise_step() {} // -Wdelete-non-virtual-dtor

    inline operator const time_type&() const { return onset; }
    inline time_type operator- ( const time_type& t ) const 
        { return onset-t; }
};

// ----------  =====  ----------

struct Stimulus_param_piecewise: public Stimulus_param
{
    using time_type = traits::time;
    using step_type = Stimulus_piecewise_step;

    double init;
    std::vector<step_type> steps;

    Stimulus_param_piecewise() {
        init = 0.0;
    }

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_piecewise: public Stimulus<Stimulus_param_piecewise>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_piecewise;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const { 
        if ( param.steps.empty() ) return param.init;
        const uidx_t k = dr::upper_bound(param.steps,t);
        return k==0 ? param.init : param.steps[k-1].value;
    }
};

LSBM_NS_END
