
//==================================================
// @title        impulse.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param_impulse: public Stimulus_param
{
    using time_type = traits::time;
    
    time_type  loc, width;
    double     amp;

    Stimulus_param_impulse( time_type l=0.0, time_type w=1.0, double a=1.0 )
        : loc(l), width(w), amp(a) {}

    inline bool check() const {
        return width >= dr::c_num<time_type>::eps;
    }

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_impulse: public Stimulus<Stimulus_param_impulse>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_impulse;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const { 
        if ( dr::op_abs(t-param.loc) >= param.width/2 ) return 0.0;
        t = atanh( 2*(t-param.loc) / param.width );
        return (param.amp/param.width) * exp(-t*t);
    }
};

LSBM_NS_END
