
//==================================================
// @title        timecourse.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_param_timecourse: public Stimulus_param
{
    using time_type = traits::time;
    using value_type = traits::value;
    using tc_type = disol::tc_ptr<const time_type,const value_type>;
    
    tc_type tc;

    inline bool check() const { return tc; }

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& in ) {
        return disol::jmx_import( in, tc ) && check();
    }
    #endif
};

// ------------------------------------------------------------------------

struct Stimulus_timecourse: public Stimulus<Stimulus_param_timecourse>
{
    using time_type = traits::time;
    using param_type = Stimulus_param_timecourse;
    using parent = Stimulus<param_type>;
    using parent::param;

    inline double operator() ( time_type t ) const { 
        return param.tc.pinterp(t);
    }
};

LSBM_NS_END
