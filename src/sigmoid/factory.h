
//==================================================
// @title        factory.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <string>
#include <memory>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

enum class sigmoid_id
{
    Logistic,
    Gaussian,
    Hyperbolic,
    Gumbel,
    Weibull,
    Smoothstep
};

// ------------------------------------------------------------------------

struct Sigmoid_mapping: public nmap<sigmoid_id>
{
    Sigmoid_mapping()
    {
        this->define( "logistic",    sigmoid_id::Logistic   );
        this->define( "gaussian",    sigmoid_id::Gaussian   );
        this->define( "normal",      sigmoid_id::Gaussian   );
        this->define( "hyperbolic",  sigmoid_id::Hyperbolic );
        this->define( "gumbel",      sigmoid_id::Gumbel     );
        this->define( "weibull",     sigmoid_id::Weibull    );
        this->define( "smoothstep",  sigmoid_id::Smoothstep );
    }
};

// ------------------------------------------------------------------------

// self-managed sigmoid pointer
typedef std::shared_ptr<Sigmoid_abstract> sigmoid_ptr;

template <class K>
sigmoid_ptr make_sigmoid( double mu, double sigma ) {
    auto x = new Sigmoid<K>();
    x->rsc.mu = mu;
    x->rsc.sigma = sigma;
    return sigmoid_ptr(x);
}

sigmoid_ptr sigmoid_factory( const std::string& name );

#ifdef LSBM_USE_JMX
sigmoid_ptr sigmoid_factory( const jmx::Struct& cfg );
#endif

LSBM_NS_END
