
//==================================================
// @title        gaussian.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_Gaussian: public Sigmoid_kernel
{
    const double invSqrt2Pi;
    Sigmoid_Gaussian(): invSqrt2Pi(1.0 / sqrt(2.0*dr::math::Pi)) {}

    inline double val( double x ) const { return 0.5*(1 + dr::sp_erf(x/sqrt(2.0))); }
    inline double inv( double y ) const { return sqrt(2.0)*dr::sp_erfinv(2*y-1); }
    inline double der( double x ) const { return invSqrt2Pi*exp(-x*x); }
};

LSBM_NS_END
