
//==================================================
// @title        weibull.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

double Sigmoid_Weibull::der( double x ) const {
    if ( x <= 0.0 ) return 0.0;
    const double x1 = std::pow(x,shape);
    const double x2 = std::pow(x,shape-1);
    return shape*x2*exp(-x1);
}

#ifdef LSBM_USE_JMX

bool Sigmoid_Weibull::configure( const jmx::Struct& in ) {
    shape = in.getnum("shape",1.0);
    return check();
}

#endif

LSBM_NS_END
