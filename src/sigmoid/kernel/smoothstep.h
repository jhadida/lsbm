
//==================================================
// @title        smoothstep.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_smoothstep: public Sigmoid_kernel
{
    inline double val( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 1.0;
        return x*x*(3 - 2*x);
    }
    inline double inv( double y ) const { 
        return 0.5 - sin(asin(1 - 2*y)/3); 
    }
    inline double der( double x ) const { 
        if ( x <= 0.0 ) return 0.0;
        if ( x >= 1.0 ) return 0.0;
        return 6*x*(1-x); 
    }
};

LSBM_NS_END
