
//==================================================
// @title        gumbel.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_Gumbel: public Sigmoid_kernel
{
    inline double val( double x ) const { return exp(-exp(-x)); }
    inline double inv( double y ) const { return -log(-log(y)); }
    inline double der( double x ) const { return exp(-x-exp(-x)); }
};

LSBM_NS_END
