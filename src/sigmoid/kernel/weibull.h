
//==================================================
// @title        weibull.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_Weibull: public Sigmoid_kernel
{
    double shape;

    inline void clear() { shape = 1.0; }
    inline bool check() const { return shape > 0.0; }

    double der( double x ) const;

    inline double val( double x ) const 
        { return x > 0.0 ? (1 - exp(-std::pow(x,shape))) : 0.0; }

    inline double inv( double y ) const 
        { return std::pow( -log1p(-y), 1.0/shape ); }

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif

};

LSBM_NS_END
