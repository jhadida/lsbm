
//==================================================
// @title        hyperbolic.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_hyperbolic: public Sigmoid_kernel
{
    inline double val( double x ) const { return 0.5*(1 + tanh(x)); }
    inline double inv( double y ) const { return atanh(2*y-1); }
    inline double der( double x ) const {
        x = tanh(x);
        return 0.5*(1 - x*x);
    }
};

LSBM_NS_END
