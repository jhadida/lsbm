
//==================================================
// @title        factory.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

sigmoid_ptr sigmoid_factory( const std::string& name )
{
    static const Sigmoid_mapping mapping;
    switch( mapping.get_id(name) )
    {
        case sigmoid_id::Logistic:
            return sigmoid_ptr(new Sigmoid<Sigmoid_logistic>());

        case sigmoid_id::Gaussian:
            return sigmoid_ptr(new Sigmoid<Sigmoid_Gaussian>());

        case sigmoid_id::Hyperbolic:
            return sigmoid_ptr(new Sigmoid<Sigmoid_hyperbolic>());

        case sigmoid_id::Gumbel:
            return sigmoid_ptr(new Sigmoid<Sigmoid_Gumbel>());

        case sigmoid_id::Weibull:
            return sigmoid_ptr(new Sigmoid<Sigmoid_Weibull>());

        case sigmoid_id::Smoothstep:
            return sigmoid_ptr(new Sigmoid<Sigmoid_smoothstep>());

        default:
            DRAYN_WARN( "Unknown sigmoid: %s", name.c_str() )
            return sigmoid_ptr();
    }
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX

sigmoid_ptr sigmoid_factory( const jmx::Struct& cfg )
{
    sigmoid_ptr s;

    DRAYN_ASSERT_ERR( cfg.has_field("name"), "Missing field 'name'." )
    s = sigmoid_factory(cfg.getstr("name"));
    DRAYN_ASSERT_ERR( s && s->configure(cfg), "Could not build or configure sigmoid." )

    return s;
}

#endif

LSBM_NS_END
