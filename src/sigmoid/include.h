#ifndef LSBM_SIGMOID_H_INCLUDED
#define LSBM_SIGMOID_H_INCLUDED

//==================================================
// @title        sigmoid
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "abstract.h"

// ----------  =====  ----------

#include "kernel/logistic.h"
#include "kernel/gaussian.h"
#include "kernel/hyperbolic.h"
#include "kernel/gumbel.h"
#include "kernel/weibull.h"
#include "kernel/smoothstep.h"

// ----------  =====  ----------

#include "factory.h"

#endif
