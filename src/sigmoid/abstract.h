
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Sigmoid_kernel
{
    inline void clear() {}
    inline bool check() const { return true; }

    virtual double val( double ) const =0; // sigmoid value
    virtual double der( double ) const =0; // value of the derivative
    virtual double inv( double ) const =0; // inverse of the sigmoid

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& ) { return true; }
    #endif
};

// ------------------------------------------------------------------------

struct Sigmoid_abstract
{
    virtual double val( double ) const =0; // sigmoid value
    virtual double der( double ) const =0; // value of the derivative
    virtual double inv( double ) const =0; // inverse of the sigmoid

    inline double operator() (double x) const { return val(x); }

    virtual bool check() const =0;

    #ifdef LSBM_USE_JMX
    virtual bool configure( const jmx::Struct& in ) =0;
    #endif
};

// ------------------------------------------------------------------------

template <class Kernel>
struct Sigmoid: public Sigmoid_abstract
{
    Kernel ker;
    rescaling rsc;
    
    // ----------  =====  ----------
    
    Sigmoid() { clear(); }

    virtual ~Sigmoid() {} // -Wdelete-non-virtual-dtor

    inline void clear() 
        { ker.clear(); rsc.clear(); }

    inline bool check() const 
        { return ker.check() && rsc.check(); }

    inline double val(double x) const 
        { return ker.val(rsc.rescale(x)); }

    inline double der(double x) const 
        { return ker.der(rsc.rescale(x)) / rsc.sigma; }

    inline double inv(double y) const 
        { return rsc.unscale(ker.inv(y)); }

    #ifdef LSBM_USE_JMX
    inline bool configure( const jmx::Struct& in ) 
        { return rsc.configure(in) && ker.configure(in) && check(); }
    #endif
};

LSBM_NS_END
