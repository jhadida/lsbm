#ifndef LSBM_H_INCLUDED
#define LSBM_H_INCLUDED

//==================================================
// @title        lsbm.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#define LSBM_NS_START namespace lsbm {
#define LSBM_NS_END   }

#include "drayn.h"
#include "disol.h"

// ----------  =====  ----------

// propagate flags from Drayn
#ifdef DRAYN_USE_JMX
    #ifndef LSBM_USE_JMX
    #define LSBM_USE_JMX
    #endif 
#endif

#ifdef DRAYN_USE_ARMA
    #ifndef LSBM_USE_ARMA
    #define LSBM_USE_ARMA
    #endif 
#endif

// ------------------------------------------------------------------------

#include "traits.h"

#include "util/include.h"
#include "sigmoid/include.h"
#include "activation/include.h"
#include "stimulus/include.h"
#include "system/include.h"
// #include "feature/include.h"

#endif
