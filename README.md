
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

# Large-Scale Biophysical Networks

C++ library built on top of Drayn and DiSol with a fairly modular way of defining neuronal systems.
Currently implemented models include Wilson-Cowan (node and network) and a conductance-based model (node and network).
There's more to come!
