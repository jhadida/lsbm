
//==================================================
// @title        compact_sigmoid.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <functional>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

double compact_sigmoid( double a, double b, double x );

// ------------------------------------------------------------------------

inline double smoothstep2( const double& x )
{
    return -x*x*(2*x - 3);
}
inline double smoothstep3( const double& x )
{
    return x*x*x*(x*(6*x - 15) + 10);
}
inline double smoothstep4( const double& x )
{
    double y = x*x;
    return -y*y*(x*(x*(20*x - 70) + 84) - 35);
}
inline double smoothstep5( const double& x )
{
    double y = x*x;
    return y*y*x*(x*(x*(x*(70*x - 315) + 540) - 420) + 126);
}

double smoothstep( double a, double b, double x );

// ------------------------------------------------------------------------

struct CompactSigmoid
{
    typedef lsbm_traits::time time_type;
    time_type t_start, t_end;

    CompactSigmoid()
        { set_timeframe(0.0,1.0); }
    CompactSigmoid( time_type ts, time_type te )
        { set_timeframe(ts,te); }

    bool set_timeframe( time_type ts, time_type te );

    inline double operator() ( time_type t ) const
        { return static_cast<double>(t > t_start); }
        // { return smoothstep( t_start, t_end, t ); }
        // { return compact_sigmoid( t_start, t_end, t ); }
};

LSBM_NS_END
