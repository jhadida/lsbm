
//==================================================
// @title        impulse.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <vector>
#include <functional>
#include <stdexcept>

// TODO: need to know initial time



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct stimulus_param_poisson: public stimulus_param
{
    using time_type = traits::time;
    using gen_type = std::function<double()>;

    std::vector<time_type> events;
    time_type  event_rate, decay_rate;
    gen_type   gen;
    double     amp, inc, ord;

    stimulus_poisson_param() {
        event_rate = decay_rate = 1.0;
        amp = inc = 0.0;
        ord = 1.0;

        gen = dr::exponential_gen(event_rate);
        events.push_back(gen());
    }

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& in );
    #endif
};

// ------------------------------------------------------------------------

struct stimulus_poisson: public stimulus<stimulus_param_poisson>
{
    using time_type = traits::time;
    using param_type = stimulus_param_poisson;
    using parent = stimulus<param_type>;
    using parent::param;

    double operator() ( time_type t ) const;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//




double stimulus_poisson::operator() ( time_type t ) const
{
    DRAYN_REJECT_ERR( t < param.events[0], "Poisson process requires chronological time-calls." );

    if ( t >= param.next_event )
    {
        param.amp =
            param.amp * exp( -dr::op_pow(
                param.decay_rate*(param.next_event - param.last_event),
                param.ord
            )) + param.inc;

        param.last_event  = param.next_event;
        param.next_event += param.generator();
    }

    t = param.decay_rate * (t - param.last_event);
    return param.amp * exp(- dr::op_pow(t,param.ord));
}

bool stimulus_param_poisson::check() const
{
    DRAYN_ASSERT_RF( param.next_event > param.last_event, "Event-times should be sorted." );
    DRAYN_ASSERT_RF( param.event_rate > dr::c_num<double>::eps, "Event-rate should be strictly positive." );
    DRAYN_ASSERT_RF( param.decay_rate >= 0.0, "Decay-rate should be positive or null." );
    DRAYN_ASSERT_RF( param.ord > 0.0, "Order should be strictly positive." );

    DRAYN_ASSERT_ERR( param.decay_rate > param.event_rate,
        "This combination of event/decay rates will likely cause diverging cumulative effects." );

    return static_cast<bool>(param.generator);
}

bool stimulus_param_poisson::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_fields({"event_rate","decay_rate","inc"}), "Missing required field(s)." );

    param.event_rate = in.getnum("event_rate");
    param.decay_rate = in.getnum("decay_rate");
    param.inc = in.getnum("inc");

    param.amp = in.getnum("amp",1.0);
    param.ord = in.getnum("ord",1.0);

    // Build generator
    param.generator = dr::exponential_gen( param.event_rate );

    // Choose next event time
    param.last_event = 0.0;
    param.next_event = param.generator();

    return check();
}

LSBM_NS_END
