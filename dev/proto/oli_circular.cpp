
//==================================================
// @title        oli_circular.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

#ifdef LSBM_USE_JMX
bool Stimulus_Oli_Circular::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_fields({"value","drift","width"}), "Missing required field(s)." );

    DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["value"], param.value ), "Could not extract 'value'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["drift"], param.drift ), "Could not extract 'drift'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["width"], param.width ), "Could not extract 'width'." );
    DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["start"], param.start, 0.0 ), "Could not extract 'start'." );

    return check();
}
#endif

LSBM_NS_END
