
struct AsymmetricRescaling
    : public AbstractRescaling
{
    typedef AsymmetricRescaling self;

    double mu, sigma, alpha;

    // ----------  =====  ----------

    AsymmetricRescaling() { clear(); }

    inline void clear()
        { mu=alpha=0.0; sigma=1.0; }
    inline bool check() const
        { return (sigma > dr::c_num<double>::eps) && (alpha > -1.0); }

    double rescale( double x ) const;
    double derived( double x ) const;
    double unscale( double y ) const;

    bool configure( double m, double s, double a = 0.0 );

#ifdef LSBM_USE_JMX
    bool configure( const dr::mx_struct& ms );
#endif

};

// ------------------------------------------------------------------------

double AsymmetricRescaling::rescale( double x ) const
{
    double xh = (x-mu) / sigma;
    double xa = std::abs(xh);

    xh = (1.0 + 0.5*alpha)*xh;
    xa = 0.5*xa + log( 0.5 + 0.5*exp(-xa) );

    return xh + alpha*xa;
}

double AsymmetricRescaling::derived( double x ) const
{
    x = (x-mu)/sigma;
    x = alpha / (1.0 + exp(-x));

    return (1.0 + x) / sigma;
}

/**
 * Note: this is a crude approximation.
 * The maximum error is alpha*log(5/4) < alpha*0.23 for y = (2+alpha)*log(2).
 * The error decreases exponentially from there on both sides.
 */
double AsymmetricRescaling::unscale( double y ) const
{
    static const double LOG2 = log(2.0);

    if ( y > (2.0+alpha)*LOG2 ) {
        return mu + sigma*(y + alpha*LOG2) / (1.0+alpha);
    }
    else {
        return mu + sigma*2.0*y / (2.0+alpha);
    }
}

// ------------------------------------------------------------------------

bool AsymmetricRescaling::configure( double m, double s, double a )
{
    ASSERT_RF( s > dr::c_num<double>::eps, "Bad sigma." );
    ASSERT_RF( (a > -1.0) && (a < 1.0), "Bad alpha." );

    mu    = m;
    sigma = s;
    alpha = 2.0*a / (1.0-a);

    return true;
}

#ifdef LSBM_USE_JMX

bool AsymmetricRescaling::configure( const dr::mx_struct& ms )
{
    ASSERT_RF( ms, "Invalid input." );
    ASSERT_RF( ms.has_fields({"mu","sigma"}), "Missing field(s)." );

    double m, s, a;

    ASSERT_RF( dr::mx_extract_scalar( ms["mu"], m ), "Could not extract 'mu'." );
    ASSERT_RF( dr::mx_extract_scalar( ms["sigma"], s ), "Could not extract 'sigma'." );
    ASSERT_RF( dr::mx_extract_scalar( ms["alpha"], a, 0.0 ), "Could not extract 'alpha'." );

    return configure(m,s,a);
}

#endif
