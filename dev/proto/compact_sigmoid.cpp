
//==================================================
// @title        compact_sigmoid.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

LSBM_NS_START

bool CompactSigmoid::set_timeframe( time_type ts, time_type te )
{
    t_start = ts;
    t_end   = te;
    return t_end-t_start > dr::c_num<time_type>::eps;
}

// ------------------------------------------------------------------------

struct CompactSigmoid_data
{
    dr::vector<double> x, y, dy;

    CompactSigmoid_data( usize_t n )
    {
        static const double eps = dr::c_num<double>::eps;

        // the sigmoid is defined on [-1,1]
        x = dr::linspace<double>( -1.0, 1.0, n );

        // compute compact sigmoid
        y.create( n, 0.0 );
        for ( usize_t i = 1; i < n; ++i )
        {
            y[i]  = 1.0 - x[i]*x[i];
            y[i]  = ( y[i] < eps ) ? 0.0 : exp( -1.0/ y[i] );
            y[i] += y[i-1];
        }
        for ( usize_t i = 1; i < n; ++i ) y[i] /= y[n-1]; // normalise

        // remap the sigmoid to [0,1]
        x = dr::linspace<double>( 0.0, 1.0, n );

        // compute the slope at each point
        dy.create( n, 0.0 );
        for ( usize_t i = 0; i < n; ++i )
            disol::set_pchip_slopes_tc( x, y, n, dy, i );

        // dr::numeric_ostream_precision::set( 7 );
        // std::cout << x;
        // std::cout << y;
        // std::cout << dy;
    }
};

// ------------------------------------------------------------------------

double compact_sigmoid( double a, double b, double x )
{
    static const usize_t n = 4001;
    static const CompactSigmoid_data data(n);

    ASSERT_THROW( a < b, "[lsbm.smoothstep] Bad input." );
    disol::LinearInterpolant lin;

    x = (x-a)/(b-a);
    if ( x <= 0.0 ) return 0.0;
    if ( x >= 1.0 ) return 1.0;
    x = x*(n-1);

    lin.left   = dr::op_ufloor(x);
    lin.right  = std::min( lin.left+1, n-1 );
    lin.wright = x - lin.left;
    lin.wleft  = 1.0 - lin.wright;

    return disol::interp_pchip_tc( data.x, data.y, data.dy, n, lin );
}

// ------------------------------------------------------------------------

double smoothstep( double a, double b, double x )
{
    ASSERT_THROW( a < b, "[lsbm.smoothstep] Bad input." );
    x = (x-a)/(b-a);

    if ( x >= 1.0 ) return 1.0;
    if ( x <= 0.0 ) return 0.0;
    return smoothstep3(x);
}

LSBM_NS_END
