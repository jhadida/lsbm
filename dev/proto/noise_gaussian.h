
//==================================================
// @title        noise_gaussian.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <functional>



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

class Stimulus_Noise_Gaussian
    : public Stimulus
{
public:

    typedef std::function<double()>  gen_type;
    typedef lsbm_traits::time        time_type;

    // ----------  =====  ----------


    Stimulus_Noise_Gaussian() {}

    inline void clear () { gen_type().swap(m_gen); }
    inline bool check () const { return static_cast<bool>(m_gen); }

    // Value at time t
    inline double operator() ( time_type t = 0.0 ) const
        { return m_gen(); }

    bool configure( double sigma );

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& ms );
    #endif

private:

    gen_type m_gen;
};

LSBM_NS_END
