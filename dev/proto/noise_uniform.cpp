
//==================================================
// @title        noise_uniform.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

bool Stimulus_Noise_Uniform::configure( double lower, double upper )
{
    DRAYN_ASSERT_RF( lower >= upper, "Incompatible inputs." );
    m_gen = dr::uniform_gen( lower, upper );

    return check();
}

#ifdef LSBM_USE_JMX
bool Stimulus_Noise_Uniform::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );

    double lower = 0.0, upper = 1.0;
    if ( ms.has_fields({"lower","upper"}) )
    {
        DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["lower"], lower ), "Could not extract 'lower'." );
        DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["upper"], upper ), "Could not extract 'upper'." );
    }

    return configure(lower,upper);
}
#endif

LSBM_NS_END
