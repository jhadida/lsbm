
//==================================================
// @title        multiplicative.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_Compound_Multiplicative
    : public Stimulus
{
    typedef Stimulus_Compound_Multiplicative  self;
    typedef Stimulus                          parent;


    Stimulus_ptr base, mul;

    // ----------  =====  ----------

    Stimulus_Compound_Multiplicative()
        { clear(); }

    void clear();
    bool check() const;

    // Values of base and mul
    inline double base_value  ( time_type t ) const
        { return (*base)(t); }
    inline double mul_value ( time_type t ) const
        { return mul? (*mul)(t) : 1.0; }

    // Value at time t
    inline double operator() ( time_type t ) const
        { return base_value(t) + mul_value(t); }


    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& ms );
    #endif

};

LSBM_NS_END
