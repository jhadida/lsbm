
//==================================================
// @title        additive.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_Compound_Additive
    : public Stimulus
{
    typedef Stimulus_Compound_Additive  self;
    typedef Stimulus                    parent;


    Stimulus_ptr base, add;

    // ----------  =====  ----------

    Stimulus_Compound_Additive()
        { clear(); }

    void clear();
    bool check() const;

    // Values of base and add
    inline double base_value  ( time_type t ) const
        { return (*base)(t); }
    inline double add_value ( time_type t ) const
        { return add? (*add)(t) : 0.0; }

    // Value at time t
    inline double operator() ( time_type t ) const
        { return base_value(t) + add_value(t); }


    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& ms );
    #endif

};

LSBM_NS_END
