
//==================================================
// @title        additive.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

void Stimulus_Compound_Additive::clear()
{
    base .reset();
    add  .reset();
}

// ------------------------------------------------------------------------

bool Stimulus_Compound_Additive::check() const
{
    return (base && base->check()) && ( !add || add->check() );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool Stimulus_Compound_Additive::configure( const jmx::Struct& in )
{
    DRAYN_ASSERT_RF( in.has_field("base"), "Missing field 'base'." );

    clear();
    DRAYN_ASSERT_RF( base = Stimulus_Factory(in.getstr("base")), "Couldn't extract 'base'." );

    if ( in.has_field("add") ) {
        DRAYN_ASSERT_RF( add = Stimulus_Factory(in.getstr("add")), "Couldn't extract 'add'." );
    }
    else // Compatibility with previous versions
    if ( in.has_field("noise") ) {
        DRAYN_ASSERT_RF( add = Stimulus_Factory(in["noise"]), "Couldn't extract 'noise'." );
    }

    return check();
}
#endif

LSBM_NS_END
