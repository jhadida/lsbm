
//==================================================
// @title        multiplicative.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

void Stimulus_Compound_Multiplicative::clear()
{
    base .reset();
    mul  .reset();
}

// ------------------------------------------------------------------------

bool Stimulus_Compound_Multiplicative::check() const
{
    return (base && base->check()) && ( !mul || mul->check() );
}

// ------------------------------------------------------------------------

#ifdef LSBM_USE_JMX
bool Stimulus_Compound_Multiplicative::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_field("base"), "Missing field 'base'." );

    clear();
    DRAYN_ASSERT_RF( base = Stimulus_Factory(ms["base"]), "Couldn't extract 'base'." );

    if ( ms.has_field("mul") ) {
        DRAYN_ASSERT_RF( mul = Stimulus_Factory(ms["mul"]), "Couldn't extract 'mul'." );
    }

    return check();
}
#endif

LSBM_NS_END
