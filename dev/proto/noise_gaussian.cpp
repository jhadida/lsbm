
//==================================================
// @title        noise_gaussian.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

bool Stimulus_Noise_Gaussian::configure( double sigma )
{
    DRAYN_ASSERT_RF( sigma >= 0.0, "Bad sigma." );
    m_gen = dr::normal_gen( 0.0, sigma );

    return check();
}

#ifdef LSBM_USE_JMX
bool Stimulus_Noise_Gaussian::configure( const jmx::Struct& ms )
{
    DRAYN_ASSERT_RF( ms, "Invalid input." );
    DRAYN_ASSERT_RF( ms.has_field("sigma"), "Missing field 'sigma'." );

    double sigma = 0.0;
    DRAYN_ASSERT_RF( dr::mx_extract_scalar( ms["sigma"], sigma ), "Could not extract 'sigma'." );

    return configure(sigma);
}
#endif

LSBM_NS_END
