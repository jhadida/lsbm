
//==================================================
// @title        oli_circular.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



LSBM_NS_START

struct Stimulus_Oli_Circular_param
{
    double value;
    double start, drift, width;

    inline void clear()
    {
        value = start = 0.0;
        drift = width = 1.0;
    }
};

// ------------------------------------------------------------------------

class Stimulus_Oli_Circular
    : public Stimulus
{
public:

    typedef lsbm_traits::time            time_type;
    typedef Stimulus_Oli_Circular_param  param_type;

    // ----------  =====  ----------

    param_type param;

    inline void clear()
        { param.clear(); }
    inline bool configure( const param_type& p )
        { param = p; return check(); }

    // ----------  =====  ----------


    Stimulus_Oli_Circular() { clear(); }


    inline bool check() const
        { return param.width > dr::c_num<double>::eps; }

    // Value at time t
    inline double operator() ( time_type t ) const
    {
        t = dr::op_rem(
            param.start + t * param.drift,
            2.0 * param.width
        );
        return param.value * (t < param.width);
    }

    #ifdef LSBM_USE_JMX
    bool configure( const jmx::Struct& ms );
    #endif

};

LSBM_NS_END
