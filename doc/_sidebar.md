
* Library

 * Architecture
 * [Activation](lib/activation)
 * [Sigmoid](lib/sigmoid)
 * [Stimulus](lib/stimulus)
 * [Utilities](lib/utilities)

* Model

 * [Kuramoto](model/kura)
 * [Hopf](model/hopf)
 * [Wilson-Cowan](model/wc)
 * [Conductance-based](model/cb)

* Interface

 * [Matlab](matlab/index)
 * [Python](python/index)
