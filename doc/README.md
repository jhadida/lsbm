
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

# Large-scale biophysical models

This is mainly a C++/Mex library to simulate (possibly stochastic) networks of neuronal-mass models with delays.
Various functions (activation, sigmoid and stimulus) are defined to simplify the implementation of models, and their simulation.
Several models are currently implemented, including:

 - Kuramoto model ([more](model/kura));
 - Hopf model ([more](model/hopf));
 - Wilson-Cowan with inhibitory-synaptic plasticity (ISP), and stochatic variants ([more](model/wc));
 - Conductance-based model ([more](model/cb))

This repository also contains a [Matlab toolbox](doc/matlab/index), and soon a [Python module](doc/python/index), to interface with the simulation library.
