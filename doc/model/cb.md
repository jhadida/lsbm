

# Conductance-based model

The temporal variations of membrane potentials are modelled as a function of ionic currents. 
Contrary to the Wilson-Cowan model, the mean-field ionic currents relate _directly_ to the signal measured with MEG.

## Theory

### Local equations

Two subpopulations per brain regions (E/I), and three state variables per subpopulation (membrane potential + E/I conductances).
The intensity of ionic currents is controlled by synapses, which act as gates allowing ionic flows proportional to their conductance.
$$
\begin{aligned}
    \tau_e^\mathrm{pot} \partial_t V_e
        &=  g_e^\mathrm{leak} \Delta_e^\mathrm{leak}
        +   g_{ee} \Delta_e^\mathrm{dep}
        +   g_{ie} \Delta_e^\mathrm{hyp}
        +   C_e \\[2mm]
    \tau_e^\mathrm{syn} \partial_t g_{ee}
        &=  -g_{ee} + w_{ee} U_e + P_e \\[2mm]
    \tau_i^\mathrm{syn} \partial_t g_{ie}
        &=  -g_{ie} + w_{ie} U_i \\[8mm]
    \tau_i^\mathrm{pot} \partial_t V_i
        &=  g_i^\mathrm{leak} \Delta_i^\mathrm{leak}
        +   g_{ei} \Delta_i^\mathrm{dep}
        +   g_{ii} \Delta_i^\mathrm{hyp}
        +   C_i \\[2mm]
    \tau_e^\mathrm{syn} \partial_t g_{ei}
        &=  -g_{ei} + w_{ei} U_e + P_i \\[2mm]
    \tau_i^\mathrm{syn} \partial_t g_{ii}
        &=  -g_{ii} + w_{ii} U_i 
\end{aligned}
$$
where $\Delta_y^X = R^X - V_y$ is a relative potential and $U = \mathcal{S}(V; \mu, \sigma)$ is a synaptic response function. The quantity relating to MEG signals is the current $g_{ee}\Delta_e^\mathrm{dep}$.

![Conductance-based model](./cb-model.png)

### Nullclines and normalisation

In the limit of $\tau^\mathrm{syn}\to 0$, or in the steady-state regime $\partial_t V_e = 0$, we have:
$$
\begin{aligned}
    g_{ee} &= w_{ee} U_e + P_e \\
    g_{ie} &= w_{ie} U_i \\
    g_{ei} &= w_{ei} U_e + P_i \\
    g_{ii} &= w_{ii} U_i
\end{aligned}
$$
which leads to two state equations per region instead of six.

Hence, the nullclines of membrane potential variables are embedded in a two-dimensional space:
$$
\begin{aligned}
    \partial_t V_e = 0
        &\iff   -w_{ie} \mathcal{S}_i(V_i) \Delta_e^\mathrm{hyp} = g_e^\mathrm{leak} \Delta_e^\mathrm{leak} + (P_e + w_{ee}U_e) \Delta_e^\mathrm{dep} + C_e \\[2mm]
        &\iff   V_i = \mathcal{S}_i^{-1}\left(
            \frac{ 
                g_e^\mathrm{leak} \Delta_e^\mathrm{leak} + (P_e + w_{ee}U_e) \Delta_e^\mathrm{dep} + C_e 
            }{ 
                -w_{ie} \Delta_e^\mathrm{hyp} 
            }
        \right)\\[8mm]
    \partial_t V_i = 0
        &\iff   -(P_i + w_{ei}\mathcal{S}_e(V_e)) \Delta_i^\mathrm{dep} = g_i^\mathrm{leak} \Delta_i^\mathrm{leak} + w_{ii}U_i \Delta_i^\mathrm{hyp} + C_i \\[2mm]
        &\iff   V_e = \mathcal{S}_e^{-1}\left(
            \frac{ 
                g_i^\mathrm{leak} \Delta_i^\mathrm{leak} + w_{ii}U_i \Delta_i^\mathrm{hyp} + P_i \Delta_i^\mathrm{dep} + C_i
            }{ 
                -w_{ei}\Delta_i^\mathrm{dep}
            }
        \right)
\end{aligned}
$$

From these equations, we see that any simultaneous scaling of $\big\{ g_x^\mathrm{leak}, C_x, P_x, w_{xx}, w_{yx} \big\}$ leaves the argument of the inverse sigmoid unchanged, so there is a degree a freedom in the parameterisation. We normalise this by imposing $g_e^\mathrm{leak} = g_i^\mathrm{leak} = 1\mathrm{nS}$.

### Oscillatory regime and excitability

Both the synaptic input $P_{e,i}$ and the input currents $C_{e,i}$ lead to an oscillatory regime beyond a certain value; however they do so in slightly different ways.

On the first hand, synaptic inputs leads to a non-zero steady-state of the conductances, which effectively "forces the gates" to always be open, regardless of the response function. This is useful to study the effects of remote regions onto the local neuronal populations, i.e. from a network perspective.

On the other hand, potential inputs bring the membrane potentials into the dynamic range of the synaptic response function, which of course depends on its parameters. This is useful to study the effects of stimuli which affect the excitability of local neuronal populations.

### Network equations

The network equations take the form:
$$
\begin{aligned}
    \tau_k^\mathrm{pot} \partial_t V_k &=  \Delta_k^\mathrm{leak} + g_k^E \Delta_k^\mathrm{dep} + g_k^I \Delta_k^\mathrm{hyp} + C_k \\[2mm]
    \tau_E^\mathrm{syn} \partial_t g_k^E &=  -g_k^E + \sum_{j\in\Omega_k^E} w_{jk}U_j(t-\lambda_{jk}) \\[2mm]
    \tau_I^\mathrm{syn} \partial_t g_k^I &=  -g_k^I + \sum_{j\in\Omega_k^I} w_{jk}U_j(t-\lambda_{jk})
\end{aligned}
$$
where $g_k^{E,I}$ are macro-conductances (see thesis), and the synaptic inputs do not appear because they are fully specified from network interactions.

Note that contrary to the WC model, a scaling of the synaptic strengths $w_{jk}$ is insufficient to cause the network to oscillate, because these weights are _outside_ the non-linearity. Instead, it is necessary to scale the synaptic strengths in conjunction with an increase in potential inputs $C_k$. This has the effect of bringing the potentials into the dynamic range of the non-linearities, which then combines with the effect of scaling the synaptic strengths.

### Instantaneous synaptic gates

In that case, the system drops to only two equations per unit:
$$
\begin{aligned}
    \tau_e \partial_t V_e
        &=  g_e^\mathrm{leak} \Delta_e^\mathrm{leak}
        +   (w_{ee} U_e + P_e) \Delta_e^\mathrm{dep}
        +   w_{ie} U_i \Delta_e^\mathrm{hyp}
        +   C_e \\[2mm]
    \tau_i \partial_t V_i
        &=  g_i^\mathrm{leak} \Delta_i^\mathrm{leak}
        +   (w_{ei} U_e + P_i) \Delta_i^\mathrm{dep}
        +   w_{ii} U_i \Delta_i^\mathrm{hyp}
        +   C_i 
\end{aligned}
$$

The nullclines remain unchanged, and therefore the same normalisation can be applied to parameters $\big\{ g_x^\mathrm{leak}, C_x, P_x, w_{xx}, w_{yx} \big\}$.
The network equations become:
$$
\begin{aligned}
    \tau_k^E \partial_t V_k^E
        &=  \Delta_k^\mathrm{E,leak}
        +   \left( P_k^E + w_{ee} U_k^E + \sum_{j\neq k} w_{jk} V_j^E(t-\lambda_{jk}) \right) \Delta_k^\mathrm{E,dep}
        +   w_{ie} U_k^I \Delta_k^\mathrm{E,hyp}
        +   C_k^E \\[2mm]
    \tau_k^I \partial_t V_k^I
        &=  \Delta_k^\mathrm{I,leak}
        +   (w_{ei} U_k^E + P_k^I) \Delta_k^\mathrm{I,dep}
        +   w_{ii} U_k^I \Delta_k^\mathrm{I,hyp}
        +   C_k^I 
\end{aligned}
$$

### Inversion

Assuming $P_k^I = 0$, and $C_k^E$ and $C_k^I$ constant (to be estimated), we have:
$$
    P_k^E = \frac{ 
        \tau_k^E \partial_t V_k^E
        - \Delta_k^\mathrm{E,leak}
        - w_{ie} U_k^I \Delta_k^\mathrm{E,hyp}
        - C_k^E
    }{ \Delta_k^\mathrm{E,dep} }
    - w_{ee} U_k^E 
    - \sum_{j\neq k} w_{jk} V_j^E(t-\lambda_{jk})
$$
