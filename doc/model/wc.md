
# Wilson-Cowan model

The oscillatory nature of electro-physiological signals is modelled as resulting from cycles of excitation and inhibition.

## Theory

### Local equations

Two subpopulations per brain region (E/I), one state equation for each:
$$
\begin{aligned}
    \tau_e \partial_t E     &=   -E + (1 - r_e E)\mathcal{S}_e\big( c_{ee}E + c_{ie}I + P_e \big)\\
    \tau_i \partial_t I     &=   -I + (1 - r_i I)\mathcal{S}_i\big( c_{ei}E + c_{ii}I \big)\\
\end{aligned}
$$
where $\mathcal{S}(x; \mu,\sigma)$ is a population response function.

Inhibitory couplings $c_{ie}$ and $c_{ii}$ must be non-positive, while $c_{ee}$ and $c_{ei}$ must be non-negative.
Parameter $r$ is a dimensionless amortisation factor in $[0,1]$ affecting the amplitude of the response function.

### Nullclines and normalisation

The nullclines are obtained simply by setting $\partial_t E = \partial_t I = 0$:
$$
\begin{aligned}
    c_{ie} I &= -c_{ee}E - P_e + \mathcal{S}_e^{-1} \left(\frac{E}{1-r_e E}\right) \\
    c_{ei} E &= -c_{ii}I - P_i + \mathcal{S}_i^{-1} \left(\frac{I}{1-r_i I}\right) 
\end{aligned}
$$

Note that both nullclines are linear in $\mu$ and $\sigma$, hence any simultaneous rescaling of $\big\{ c_{xx}, c_{yx}, P_x, \mu_x, \sigma_x \big\}$ leaves the equations unchanged, so there is a degree of freedom in the parameterisation for _each_ subpopulation. We normalise this by imposing:
 - $P_e = 1$ is the oscillatory input threshold (rescaling by a factor $1/P_e^*$);
 - $\mu_i = \mu_e$ (rescaling each parameter set as needed).

### Network equations

The network equations take the form
$$
    \tau_k \partial_t X_k = -X_k + (1-r_k X_k)\mathcal{S}_k\left(
        \sum_{j=1}^{2N} c_{jk}X_j(t-\lambda_{jk}) + P_k
    \right)
$$
where $N$ is the number of regions and $\lambda_{jk} = \lambda_{j\to k}$ are delays.

### Stochastic formulation

Reminder than if $W$ is a Wiener process, then:
$$
    \forall\ t > s,\quad W(t) - W(s) \sim \mathcal{N}( 0, |t-s| )
$$

The derivative of white noise is therefore:
$$
    \eta\sim\mathcal{N}(0,\sigma^2) 
        \quad\Rightarrow\quad 
    d\eta \sim \mathcal{N}(0,2\sigma^2) \sim \sigma\sqrt{\frac{2}{dt}} dW
$$

To formulate the stochastic network equations, we first define:
$$
\begin{aligned}
    Z_k &= \sum_{j=1}^{2N} c_{jk}X_j(t-\lambda_{jk}) + \eta_k \\
    F_k &= -X_k + (1-r_k X_k)\mathcal{S}_k( Z_k + P_k )
\end{aligned}
$$

Then the stochastic system is given by:
$$
\begin{aligned}
    \tau_k dX_k &= F_k dt \\ 
    dZ_k &= \left[ \sum_{j=1}^{2N} c_{jk} F_j(t-\lambda_{jk}) \right]dt + \sigma_k\sqrt{\frac{2}{dt}} dW_k
\end{aligned}
$$

This implies keeping a separate record for $F_k$, and computing the initial conditions for $Z_k$.

### Inversion

Network equations are:
$$
\begin{aligned}
    \tau^E_k \partial_t E_k &= - E_k + (1 - r^E_k E_k) \mathcal{S}^E_k\left( c_{ee} E_k - c_{ie} I_k + \sum_{j\neq k} w_{jk} E_j(t-\lambda_{jk}) + P_k \right) \\
    \tau^I_k \partial_t I_k &= - I_k + (1 - r^I_k I_k) \mathcal{S}^I_k\left( c_{ei} E_k - c_{ii} I_k \right) \\
\end{aligned}
$$
with parameters:
- $\{ \mu, \tau^E, \tau^I, r^E, r^I, \sigma^E, \sigma^I, c_{ee}, c_{ei}, c_{ie}, c_{ii} \}$ are local parameters;
- $[ w_{ij} ]$ and $[ \lambda_{ij} ]$ are matrices derived from structural connectomes;
- $\gamma$ and $\delta$ are global parameters.

We can rewrite the first equation as:
$$
    P_k = c_{ie}I_k - c_{ee}E_k - \sum_{j\neq k} w_{jk} E_j(t-\lambda_{jk}) + \mathcal{Q}^E_k \left(\frac{\tau^E_k\partial_t E_k + E_k}{1 - r^E_k E_k} \right)
$$
which is completely determined by the knowledge of $\partial_t E_k$, $E_k$, $I_k$ and all parameters.
