
# Stimulus functions

## List

### Constant

Single parameter `value`.

### Heaviside

Step function with parameters `onset` and `value`.

### Impulse

Function:
$$
    f(t) = \begin{cases}
        \frac{A}{w} \mathrm{exp}\left( - \mathrm{atanh}\left(\frac{2(x-L)}{w}\right)^2  \right) &  \text{if } L-\frac{w}{2} < x < L+\frac{w}{2} \\
        0 & \text{otherwise}
    \end{cases}
$$

| Parameter | Name |
|---|---|
| $A$ | amp |
| $L$ | loc |
| $w$ | width |

### Harmonic

Function:
$$
    f(t) = A \sin( 2\pi\omega t + \varphi ) + B
$$

| Parameter | Name |
|---|---|
| $A$ | amp |
| $B$ | base |
| $\omega$ | freq |
| $\varphi$ | phi |

### Piecewise

Piecewise-constant function defined by onsets and values.
Using upper-bound algorithm to find active segment for each time-query.

### Timecourse

Using PCHIP interpolation for given `time` and `vals` vectors.

## Examples

```cpp
auto P = lsbm::Stimulus_param_piecewise();

auto S = lsbm::make_stimulus();
```
