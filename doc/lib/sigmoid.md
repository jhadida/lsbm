
# Sigmoid functions

Within LSBM, sigmoid function are more than just bounded [activation functions](lib/activation): they also define methods to compute the derivative and inverse sigmoid.

## Available functions

<div align="center" style="width:100%">
<table>
    <tr>
        <th style="width:22%"></th>
        <th style="width:22%; text-align:center;">$f(x)$</th>
        <th style="width:22%; text-align:center;">$f^\prime(x)$</th>
        <th style="width:22%; text-align:center;">$f^{-1}(x)$</th>
    </tr>
    <tr>
        <td>Logistic</td>
        <td>$\displaystyle{\frac{1}{1+e^{-x}}}$</td>
        <td>$f(x) - f(x)^2$</td>
        <td>$\displaystyle{\log\left(\frac{y}{1-y}\right)}$</td>
    </tr>
    <tr>
        <td>Gumbel</td>
        <td>$\displaystyle{e^{-e^{-x}}}$</td>
        <td>$e^{-x}f(x)$</td>
        <td>$\displaystyle{-\log\log\frac{1}{y}}$</td>
    </tr>
    <tr>
        <td>Gaussian</td>
        <td>$\displaystyle{\frac{1 + \mathrm{erf}(x/\sqrt{2})}{2}}$</td>
        <td>$\displaystyle{\frac{e^{-x^2}}{\sqrt{2\pi}}}$</td>
        <td>$\sqrt{2}\ \mathrm{erf}^{-1}(2y-1)$</td>
    </tr>
    <tr>
        <td>Hyperbolic</td>
        <td>$\displaystyle{\frac{1 + \mathrm{tanh}(x)}{2}}$</td>
        <td>$\displaystyle{\frac{1 - \mathrm{tanh}(x)^2}{2}}$</td>
        <td>$\mathrm{tanh}^{-1}(2y-1)$</td>
    </tr>
    <tr>
        <td>Weibull</td>
        <td>$\displaystyle{1 - e^{-x^\alpha}}$</td>
        <td>$\displaystyle{\alpha x^{\alpha-1} e^{-x^\alpha}}$</td>
        <td>$\displaystyle{\big(-\log(1-y)\big)^{1/\alpha}}$</td>
    </tr>
    <tr>
        <td>Smoothstep</td>
        <td>$x^2 (3-2x)$</td>
        <td>$6x(1-x)$</td>
        <td>$\displaystyle{\frac{1}{2} - \sin\left( \frac{\mathrm{asin}(1-2y)}{3} \right)}$</td>
    </tr>
</table>
</div>

## Example code

Method 1: using the factory helper.
```cpp
// smart-pointer to abstract sigmoid
auto S = lsbm::make_sigmoid< lsbm::Sigmoid_logistic >( mu, sigma );

S->val(x); // value
S->der(x); // derivative
S->inv(x); // inverse function
```

The existing kernels (to be used as template types) are:
```cpp
lsbm::Sigmoid_logistic
lsbm::Sigmoid_Gaussian
lsbm::Sigmoid_hyperbolic
lsbm::Sigmoid_Gumbel
lsbm::Sigmoid_Weibull
lsbm::Sigmoid_smoothstep
```

Method 2: manually.
```cpp
// concrete definition
auto S = lsbm::Sigmoid< lsbm::Sigmoid_Weibull >();

S.rsc.configure(mu,sigma); // rescaling parameters
S.ker.shape = 2.0; // set kernel parameter (optional)

S.val(x); // value
S.der(x); // derivative
S.inv(x); // inverse function
```
