
# Activation functions

Activation functions are commonly used in artificial neural networks. 
In biophysical network models, they serve a similar purpose: that of associating a "response" to a given set of "inputs" (which are typically summed or averaged before being fed to the function).
This is why they are also often called "_response functions_" as well.

We distinguish two classes of activations, depending on whether the response is bounded (typically in $(0,1)$) or not.

## Bounded activations

Step, linear and [smoothstep](lib/smoothstep).

## Unbounded activations

Rectifier, softplus, rational, mixed.
