
# Smoothstep functions

Sometimes, it is desirable to define a sigmoid function over a bounded domain: e.g. transitioning smoothly from exactly y=0 to y=1 between x=23 and x=42 (and constant otherwise).
Unfortunately, due to the smoothness requirement at the bounds, it is easy to show that no such function exists (consider the derivatives of all order being 0 at the bounds).

Smoothstep functions are the next best thing (see the [Wikipedia article](https://en.wikipedia.org/wiki/Smoothstep)): they are a class of polynomial approximations which are smooth up to a finite order at the bounds.
