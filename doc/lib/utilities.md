
# Utilities

There are a few utilities that you might be interested to use if you write your own C++ or Mex files (advanced usage / contributors).

## Verbosity

The verbosity affects calls to `lsbm::print_info` and `lsbm::print_debug`, which you might want to use in your code.

```cpp
lsbm::verbosity("quiet");  // only warnings
lsbm::verbosity("info");   // no debug message
lsbm::verbosity("debug");  // all messages
```

Note that this is a runtime switch, which is different from the Drayn compiler flags `DRAYN_SHOW_DEBUG` and `DRAYN_SHOW_INFO`.
A runtime switch means that the function call still occurs when the switch is off, which might cause a minor overhead.

## Rescaling

A rescaling is a simple mathematical operation $x \to (x-\mu)/\sigma$. 
We use rescalings in several places, e.g. to shift the location and sharpness of sigmoid functions.

You can also use rescalings in your code if needed:
```cpp
auto r = lsbm::rescaling( mu, sigma );

double y = r.rescale(x);
drayn::is_close( x, r.unscale(y) ); 
```

## Name-mapping

Name mapping consists in associating an ID (typically `int` or `enum class`) with a string.
We use this for factory helpers, and it is useful when interfacing with user inputs.

Here is an example:
```cpp
// use an enum class for example
enum class nonsense
{
    Foo,
    Bar,
    Baz
};

// define the name-mapping
struct nonsense_map: public lsbm::nmap<nonsense>
{
    nonsense_map() {
        this->define( "foo", nonsense::Foo );
        this->define( "bar", nonsense::Bar );
        this->define( "baz", nonsense::Baz );

        // you can also define aliases
        this->define( "fu", nonsense::Foo );
    }
};

// example usage
void some_function( std::string name ) {
    // interesting things...

    // declare and use map
    const nonsense_map nm;
    nm.get_id(name); // throws if name is undefined
}
```
