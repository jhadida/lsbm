
CC = clang++
# CC = g++

DRAYN_SRC = ../jcl/src
DISOL_SRC = ../disol/src

WARNING = -Wall
DEFINE = -DDRAYN_SHOW_INFO
INCLUDE = -I"$(DRAYN_SRC)" -I"$(DISOL_SRC)"
CXXFLAGS = $(DEFINE) $(WARNING) $(INCLUDE) -std=c++11

clean:
	rm -f bin/*.o 

drayn:
	$(CC) -c -o bin/drayn.o $(CXXFLAGS) $(DRAYN_SRC)/drayn.cpp

disol: drayn
	$(CC) -c -o bin/disol.o $(CXXFLAGS) $(DISOL_SRC)/disol.cpp

lsbm:
	$(CC) -c -o bin/lsbm.o $(CXXFLAGS) src/lsbm.cpp
